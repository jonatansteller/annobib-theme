<?php
// Annotated Bibliography Theme
// Reusable Component: Card

$annobib_section	= get_query_var( 'annobib_section' );
$annobib_language	= get_query_var( 'annobib_language' );
$annobib_current	= get_query_var( 'annobib_current' );
$annobib_author		= get_the_term_list( get_the_ID(), $annobib_section . '-writer', '', ' ' . __( 'and', 'annobib-theme' ) . ' ', '' );
$annobib_genre		= get_the_term_list( get_the_ID(), $annobib_section . '-genre', '', '/', '' );
$annobib_country	= get_the_term_list( get_the_ID(), $annobib_section . '-country', '', '/', '' );
$annobib_year			= annobib_retrieve( get_post(), $annobib_section, 'year' );
$annobib_slug			= annobib_retrieve( get_post(), $annobib_section, 'slug' );
?>


				<!-- PAGE MAIN: CARDS: CARD -->
				<article class="pf-c-card pf-m-hoverable pf-m-selectable" role="button" tabindex="0" id="card-<?php echo $annobib_slug ?>" data-target="<?php echo get_permalink(); ?>" aria-labelledby="card-<?php echo $annobib_slug ?>-title">
					<div class="pf-c-card__header">
						<div class="pf-c-card__actions">
							<?php get_template_part( 'reusable', 'share' ); ?>
						</div>
						<div class="pf-c-card__title pf-c-content">
							<?php the_title( '<h2 id="card-' . $annobib_slug . '-title">', '</h2>', true ); ?>
							<?php if ( $annobib_author && ! is_wp_error( $annobib_author ) ) : ?><small><?php _e( 'By', 'annobib-theme' ); echo ' ' . strip_tags( $annobib_author ); ?></small><?php endif; ?>
						</div>
					</div>
					<div class="pf-c-card__body pf-c-content">
						<?php the_excerpt(); ?>
					</div>
					<?php if ( ( $annobib_genre && ! is_wp_error( $annobib_genre ) ) || ( $annobib_country && ! is_wp_error( $annobib_country ) ) || $annobib_year ) : ?><div class="pf-c-card__footer pf-c-content">
						<small>
							<?php $meta = '';
							if ( $annobib_genre && ! is_wp_error( $annobib_genre ) ) {
								$meta .= strip_tags( $annobib_genre );
							}
							if ( $meta != '' && $annobib_country && ! is_wp_error( $annobib_country ) ) {
								$meta .= ' · ';
							}
							if ( $annobib_country && ! is_wp_error( $annobib_country ) ) {
								$meta .= strip_tags( $annobib_country );
							}
							if ( $meta != '' && $annobib_year ) {
								$meta .= ' · ';
							}
							if ( $annobib_year ) {
								$meta .= $annobib_year;
							}
							echo $meta; ?>
						</small>
					</div><?php endif; ?>
				</article>
