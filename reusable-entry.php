<?php
// Annotated Bibliography Theme
// Reusable Component: Entry

$annobib_section			= get_query_var( 'annobib_section' );
$annobib_language			= get_query_var( 'annobib_language' );
$annobib_current			= get_query_var( 'annobib_current' );
$annobib_author				= get_the_term_list( get_the_ID(), $annobib_section . '-writer', '', ' ' . __( 'and', 'annobib-theme' ) . ' ', '' );
$annobib_genre				= get_the_term_list( get_the_ID(), $annobib_section . '-genre', '', '/', '' );
$annobib_country			= get_the_term_list( get_the_ID(), $annobib_section . '-country', '', '/', '' );
$annobib_year					= annobib_retrieve( get_post(), $annobib_section, 'year' );
$annobib_slug					= annobib_retrieve( get_post(), $annobib_section, 'slug' );
$annobib_citation			= annobib_retrieve( get_post(), $annobib_section, 'citation' );
$annobib_addenda			= annobib_retrieve( get_post(), $annobib_section, 'addenda' );
$annobib_entryicon		= annobib_retrieve( get_post(), $annobib_section, 'entryicon' );
$annobib_links				= annobib_retrieve( get_post(), $annobib_section, 'usefullinks' );
$annobib_criteria			= annobib_iconlist( get_the_ID(), $annobib_section . '-criterion', 'navigation-success' );
$annobib_adaptations	= annobib_iconlist( get_the_ID(), $annobib_section . '-adaptation', false );
$annobib_curricula		= annobib_labelgroup( get_the_ID(), $annobib_section . '-curriculum' );
$annobib_topics				= annobib_labelgroup( get_the_ID(), $annobib_section . '-topic' );
$annobib_contexts			= annobib_labelgroup( get_the_ID(), $annobib_section . '-context' );
?>


	<!-- PAGE MAIN -->
	<main role="main" class="pf-c-page__main" tabindex="-1" id="content" aria-labelledby="content-title">
		<article>

			<!-- PAGE MAIN: SECTION -->
			<div class="pf-c-page__main-section pf-m-light">

				<!-- PAGE MAIN: SECTION: SPLIT -->
				<div class="pf-l-split pf-m-gutter pf-u-mt-sm">

					<!-- PAGE MAIN: SECTION: SPLIT: ITEM -->
					<div class="pf-l-split__item pf-m-visible-on-xl">
						<?php set_query_var( 'annobib_modifier', 'norm' );
						get_template_part( 'reusable', 'back' ); ?>
					</div>

					<!-- PAGE MAIN: SECTION: SPLIT: FILL ITEM -->
					<div class="pf-l-split__item pf-m-fill pf-c-content">
						<?php the_title( '<h1 class="pf-c-title pf-m-3xl annobib-m-block-title" id="content-title">', '</h1>', true ); ?>
						<?php if ( $annobib_author && ! is_wp_error( $annobib_author ) ) : ?><p class="annobib-m-block-meta"><?php _e( 'By', 'annobib-theme' ); echo ' ' . $annobib_author; ?></p><?php endif; ?>
					</div>

					<!-- PAGE MAIN: SECTION: SPLIT: ITEM -->
					<div class="pf-l-split__item pf-m-visible-on-xl">
						<?php get_template_part( 'reusable', 'share' ); ?>
					</div>

				</div>

				<!-- PAGE MAIN: SECTION: BULLSEYE -->
				<div class="pf-l-bullseye annobib-m-stereo">

					<!-- PAGE MAIN: SECTION: BULLSEYE: SPLIT -->
					<div class="pf-l-bullseye__item pf-l-split pf-m-gutter">
						<h2 class="annobib-m-hidden"><?php _e( 'Summary', 'annobib-theme' ); ?></h2>

						<!-- PAGE MAIN: SECTION: BULLSEYE: SPLIT: ITEM -->
						<div class="pf-l-split__item annobib-m-stereo__speaker">
							<div class="annobib-c-figure">
								<?php if ( has_post_thumbnail() ) : ?><figure class="annobib-c-figure__frame">
									<?php the_post_thumbnail( 'post-thumbnail', ['class' => 'annobib-c-figure__frame-picture']); ?>
									<figcaption class="annobib-c-figure__frame-caption"><?php _e( 'Cover of the critical edition', 'annobib-theme' ); ?></figcaption>
								</figure><?php else : ?><figure class="annobib-c-figure__frame">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/images/cover.svg" class="annobib-c-figure__frame-picture">
									<figcaption class="annobib-c-figure__frame-caption"><?php _e( 'Generic cover of this title', 'annobib-theme' ); ?></figcaption>
								</figure>
								<div class="annobib-c-figure__cover">
									<?php the_title( '<div class="annobib-c-figure__cover-title">', '</div>', true ); ?>
									<?php if ( $annobib_author && ! is_wp_error( $annobib_author ) ) : ?><div class="annobib-c-figure__cover-author"><?php echo strip_tags( $annobib_author ); ?></div><?php endif; ?>
								</div><?php endif; ?>
							</div>
						</div>

						<!-- PAGE MAIN: SECTION: BULLSEYE: SPLIT: ITEM -->
						<div class="pf-l-split__item pf-m-fill">
							<div class="pf-c-content annobib-m-block-paragraph">
								<?php the_content(); ?>
								<?php if ( ( $annobib_genre && ! is_wp_error( $annobib_genre ) ) || ( $annobib_country && ! is_wp_error( $annobib_country ) ) || $annobib_year ) : ?><p class="annobib-m-block-meta">
									<?php $meta = '';
									if ( $annobib_genre && ! is_wp_error( $annobib_genre ) ) {
										$meta .= $annobib_genre;
									}
									if ( $meta != '' && $annobib_country && ! is_wp_error( $annobib_country ) ) {
										$meta .= ' · ';
									}
									if ( $annobib_country && ! is_wp_error( $annobib_country ) ) {
										$meta .= $annobib_country;
									}
									if ( $meta != '' && $annobib_year ) {
										$meta .= ' · ';
									}
									if ( $annobib_year ) {
										$meta .= $annobib_year;
									}
									echo $meta; ?>
								</p><?php endif; ?>
							</div>
							<h2 class="annobib-m-hidden"><?php _e( 'Critical edition', 'annobib-theme' ); ?></h2>
							<div class="annobib-c-reference">
								<span class="annobib-c-reference__icon">
									<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__<?php echo $annobib_entryicon; ?>" /></svg>
								</span>
								<p class="annobib-c-reference__text"><?php echo $annobib_citation; ?> <span class="annobib-c-reference__text-extra"><?php echo $annobib_addenda; ?></span></p>
							</div>
							<h2 class="annobib-m-hidden"><?php _e( 'Jump to further sections', 'annobib-theme' ); ?></h2>
							<div class="pf-c-toolbar pf-m-inset-none annobib-m-inline">
								<div class="pf-c-toolbar__content">
									<div class="pf-c-toolbar__content-section">
										<div class="pf-c-toolbar__group pf-m-button-group"><?php
											$toolbar_icon = '<span class="pf-c-button__icon pf-m-start"><svg class="annobib-c-icon" aria-hidden="true"><use href="' . get_template_directory_uri() . '/assets/images/annobib-c-icon.svg#annobib-c-icon__navigation-down" /></svg></span>';
											$toolbar_counter = 0;
											if ( $annobib_curricula ) : ?><div class="pf-c-toolbar__item">
												<a href="#classrooms" class="pf-c-button pf-m-link"><?php
													$toolbar_counter++;
													if ( $toolbar_counter == 1 ) {
														echo $toolbar_icon;
													}
													_e( 'Classrooms', 'annobib-theme' ); ?></a>
											</div><?php endif; ?>
											<?php if ( $annobib_adaptations ) : ?><div class="pf-c-toolbar__item">
												<a href="#adaptations" class="pf-c-button pf-m-link"><?php
													$toolbar_counter++;
													if ( $toolbar_counter == 1 ) {
														echo $toolbar_icon;
													}
												_e( 'Adaptations', 'annobib-theme' ); ?></a>
											</div><?php endif; ?>
											<?php if ( $annobib_links ) : ?><div class="pf-c-toolbar__item">
												<a href="#resources" class="pf-c-button pf-m-link"><?php
													$toolbar_counter++;
													if ( $toolbar_counter == 1 ) {
														echo $toolbar_icon;
													}
													_e( 'Resources', 'annobib-theme' ); ?></a>
											</div><?php endif; ?>
											<?php if ( $annobib_topics || $annobib_contexts ) : ?><div class="pf-c-toolbar__item">
												<a href="#explore" class="pf-c-button pf-m-link"><?php
													$toolbar_counter++;
													if ( $toolbar_counter == 1 ) {
														echo $toolbar_icon;
													}
													if ( $annobib_topics && $annobib_contexts ) {
														_e( 'Topics/contexts', 'annobib-theme' );
													}
													elseif ( $annobib_topics ) {
														_e( 'Topics', 'annobib-theme' );
													}
													elseif ( $annobib_contexts ) {
														_e( 'Contexts', 'annobib-theme' );
													} ?></a>
											</div><?php endif; ?>
										</div>
									</div>
								</div>
							</div>
						</div>

						<!-- PAGE MAIN: SECTION: BULLSEYE: SPLIT: ITEM -->
						<div class="pf-l-split__item annobib-m-stereo__speaker" id="criteria">
							<h2 class="pf-c-title pf-m-xl pf-u-mt-sm pf-u-mb-md annobib-m-block-title annobib-m-hidden"><?php _e( 'In favour of this entry', 'annobib-theme' ); ?></h2>
							<?php if ( $annobib_criteria ) : ?><ul class="annobib-c-iconlist">
								<?php echo $annobib_criteria ?>
							</ul><?php endif; ?>
						</div>

					</div>

				</div>

			</div>

			<?php if ( $annobib_curricula ) : ?><!-- PAGE MAIN: SECTION -->
			<div class="pf-c-page__main-section" id="classrooms">
				<h2 class="pf-c-title pf-m-xl pf-u-mt-sm pf-u-mb-md annobib-m-block-title"><?php _e( 'Recommended for these classrooms', 'annobib-theme' ); ?></h2>

				<!-- PAGE MAIN: SECTION: COLUMNS -->
				<div class="annobib-l-columns">

					<?php echo $annobib_curricula; ?>

				</div>

			</div><?php endif; ?>

			<!-- PAGE MAIN: SECTION -->
			<div class="pf-c-page__main-section pf-m-light">

				<!-- PAGE MAIN: SECTION: COLUMNS -->
				<div class="annobib-l-columns">

					<?php if ( $annobib_adaptations ) : ?><!-- PAGE MAIN: SECTION: COLUMNS: ITEM -->
					<div class="annobib-l-columns__item" id="adaptations">
						<h2 class="pf-c-title pf-m-xl pf-u-mt-sm pf-u-mb-md annobib-m-block-title"><?php _e( 'Adapted as', 'annobib-theme' ); ?></h2>
						<ul class="annobib-c-iconlist annobib-m-columns">
							<?php echo $annobib_adaptations; ?>
						</ul>
					</div><?php endif; ?>

					<?php if ( $annobib_links ) : ?><!-- PAGE MAIN: SECTION: COLUMNS: ITEM -->
					<div class="annobib-l-columns__item" id="resources">
						<h2 class="pf-c-title pf-m-xl pf-u-mt-sm pf-u-mb-md annobib-m-block-title"><?php _e( 'Online resources', 'annobib-theme' ); ?></h2>
						<ul class="annobib-c-linklist annobib-m-columns">
							<?php echo $annobib_links; ?>
						</ul>
					</div><?php endif; ?>

				</div>

			</div>


			<?php if ( $annobib_topics && $annobib_contexts ) : ?><!-- PAGE MAIN: SECTION -->
			<div class="pf-c-page__main-section" id="explore">

				<!-- PAGE MAIN: SECTION: TABS -->
				<nav class="pf-c-tabs" aria-label="Select tab">
					<ul class="pf-c-tabs__list">
						<li class="pf-c-tabs__item pf-m-current">
							<button class="pf-c-tabs__link annobib-h-tab" id="explore-tabs-topics" aria-controls="explore-topics" data-target="#explore-topics">
								<span class="pf-c-tabs__item-text"><?php _e( 'Topics', 'annobib-theme' ); ?></span>
							</button>
						</li>
						<li class="pf-c-tabs__item">
							<button class="pf-c-tabs__link annobib-h-tab" id="explore-tabs-contexts" aria-controls="explore-contexts" data-target="#explore-contexts">
								<span class="pf-c-tabs__item-text"><?php _e( 'Contexts', 'annobib-theme' ); ?></span>
							</button>
						</li>
					</ul>
				</nav>

				<!-- PAGE MAIN: SECTION: TAB PANEL -->
				<section class="pf-c-tab-content" id="explore-topics" role="tabpanel" tabindex="0" aria-labelledby="explore-tabs-topics">
					<h2 class="pf-c-title pf-m-xl pf-u-mb-md annobib-m-block-title annobib-m-hidden"><?php _e( 'Suitable for discussing these topics', 'annobib-theme' ); ?></h2>

					<!-- PAGE MAIN: SECTION: TAB PANEL: COLUMNS -->
					<div class="annobib-l-columns">

						<?php echo $annobib_topics; ?>

					</div>
				</section>

				<!-- PAGE MAIN: SECTION: TAB PANEL -->
				<section class="pf-c-tab-content" id="explore-contexts" role="tabpanel" tabindex="0" aria-labelledby="explore-tabs-contexts" hidden>
					<h2 class="pf-c-title pf-m-xl pf-u-mb-md annobib-m-block-title annobib-m-hidden"><?php _e( 'Suitable for discussing these contexts', 'annobib-theme' ); ?></h2>

					<!-- PAGE MAIN: SECTION: TAB PANEL: COLUMNS -->
					<div class="annobib-l-columns">

						<?php echo $annobib_contexts; ?>

					</div>

				</section>

			</div>


			<?php elseif ( $annobib_topics ) : ?><!-- PAGE MAIN: SECTION -->
			<div class="pf-c-page__main-section" id="explore">
				<h2 class="pf-c-title pf-m-xl pf-u-mb-md annobib-m-block-title"><?php _e( 'Suitable for discussing these topics', 'annobib-theme' ); ?></h2>

				<!-- PAGE MAIN: SECTION: COLUMNS -->
				<div class="annobib-l-columns">

					<?php echo $annobib_topics; ?>

				</div>

			</div>


			<?php elseif ( $annobib_contexts ) : ?><!-- PAGE MAIN: SECTION -->
			<div class="pf-c-page__main-section">
				<h2 class="pf-c-title pf-m-xl pf-u-mb-md annobib-m-block-title"><?php _e( 'Suitable for discussing these contexts', 'annobib-theme' ); ?></h2>

				<!-- PAGE MAIN: SECTION: COLUMNS -->
				<div class="annobib-l-columns">

					<?php echo $annobib_contexts; ?>

				</div>

			</div><?php endif; ?>


		</article>
	</main>
