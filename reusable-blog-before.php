<?php
// Annotated Bibliography Theme
// Reusable Component: Blog Before the Loop

$annobib_section			= get_query_var( 'annobib_section' );
$annobib_language			= get_query_var( 'annobib_language' );
$annobib_current			= get_query_var( 'annobib_current' );
?>


	<!-- PAGE MAIN -->
	<main role="main" class="pf-c-page__main" tabindex="-1" id="content" aria-labelledby="content-title">
		<?php the_title( '<h1 class="annobib-m-hidden" id="content-title">', '</h1>', true );
