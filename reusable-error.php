<?php
// Annotated Bibliography Theme
// Reusable Component: Error Page

$annobib_section	= get_query_var( 'annobib_section' );
$annobib_language	= get_query_var( 'annobib_language' );
$annobib_current	= get_query_var( 'annobib_current' );
$annobib_modifier	= get_query_var( '$annobib_modifier' );
if ( ! $annobib_modifier ) {
	$annobib_modifier	= 'article';
}
?>


			<?php if ( $annobib_modifier == 'main' ) : ?><main role="main" class="pf-c-page__main" tabindex="-1" id="content" aria-labelledby="content-title"><?php endif; ?>

				<!-- PAGE MAIN: CARDS: EMPTY -->
				<article class="pf-c-empty-state">
					<div class="pf-c-empty-state__content">
						<svg class="annobib-c-icon pf-c-empty-state__icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__navigation-empty" /></svg>

						<!-- PAGE MAIN: CARDS: EMPTY: TEXT -->
						<h1 class="pf-c-title pf-m-lg" id="content-title"><?php _e( 'Something went wrong', 'annobib-theme' ); ?></h1>
						<div class="pf-c-empty-state__body"><?php _e( 'This page might have moved or it may be an old link.', 'annobib-theme' ); ?></div>

						<!-- PAGE MAIN: CARDS: EMPTY: PRIMARY -->
						<div class="pf-c-empty-state__primary">
							<button type="button" class="pf-c-button pf-m-primary annobib-h-back"><?php _e( 'Return to previous page', 'annobib-theme' ); ?></button>
						</div>

						<!-- PAGE MAIN: CARDS: EMPTY: SECONDARY -->
						<div class="pf-c-empty-state__secondary">
							<a href="<?php echo get_home_url(); ?>" class="pf-c-button pf-m-plain"><?php _e( 'Try the front page', 'annobib-theme' ); ?></a>
							<a href="mailto:lit4school@uni-leipzig.de" class="pf-c-button pf-m-plain"><?php _e( 'Send us an email', 'annobib-theme' ); ?></a>
						</div>

					</div>
				</article>

			<?php if ( $annobib_modifier == 'main' ) : ?></main><?php endif; ?>
