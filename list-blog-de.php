<?php /* Template Name: Blog (German) */

// Annotated Bibliography Theme
// Blog (German)

annobib_localise( 'de_DE' );
set_query_var( 'annobib_section', 'blog' );
set_query_var( 'annobib_language', 'de' );
set_query_var( 'annobib_current', 'blog-de' );


// Header
get_header();

// Sidebar
get_sidebar( 'blog' );

// Blog before the loop
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
    get_template_part( 'reusable', 'blog-before' );
	}
}

// Set up the specialised loop
wp_reset_postdata();
$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
$loop = new WP_Query( array(
  'post_type'			=> 'post',
  'category_name'	=> 'german',
  'paged'					=> $paged
));

// Set up pagination
$tempQuery = $wp_query;
$wp_query  = NULL;
$wp_query  = $loop;

// Main
if ( $loop->have_posts() ) {
	while ( $loop->have_posts() ) {
		$loop->the_post();
		get_template_part( 'reusable', 'blog-post' );
	}
}
else {
	set_query_var( 'annobib_modifier', 'main' );
	get_template_part( 'reusable', 'error' );
}

// List after the loop
get_template_part( 'reusable', 'blog-after1' );
set_query_var( 'annobib_modifier', 'small-openup' );
get_template_part( 'reusable', 'pagination' );
get_template_part( 'reusable', 'blog-after2' );

// Reset loop and pagination
$wp_query = NULL;
$wp_query = $tempQuery;
wp_reset_postdata();

// Footer
get_footer();

?>
