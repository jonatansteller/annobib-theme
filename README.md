# Annotated Bibliography Theme

Theme to enable a WordPress frontend that displays an annotated bibliography if accompanied by the plugin of the same name. Both are being developed by staff of the Zentrum für Lehrerbildung und Schulforschung at Leipzig University. The theme powers a project called [Lit4School](https://home.uni-leipzig.de/lit4school/).

- *Current version:* 0.97
- *Release date:* 19 May 2021
- *Dependency:* PatternFly 2021.06 (4.103.6)

## Features

- View: general front page
- View: subject overview
- View: bibliographic entry (single)
- View: bibliographic entries and search results (cards with pagination)
- View: blog post (single)
- View: blog posts (overview)
- Design: PatternFly-based layout
- Design: colour and icon theme
- Feature: web app
- Feature: share an entry
- Feature: make a suggestion
- Feature: video tutorial
- Feature: multiple entry points on subject overviews
- Feature: filter archive pages
- Feature: enhanced search results
- Accessibility: responsive layout
- Accessibility: basic ARIA notations
- Internationalisation: English and German-language versions

## Roadmap

- P1 (feature): consider implementing a topic explorer for users looking for inspiration
- P2 (feature): implement a more sophisticated suggestion form in addition to the existing one
- P2 (feature): add meta genre icons to cards and consider an occasional animation to briefly reveal covers
- P3 (bug): consider using a session token to intelligently decide what language to use, but avoid setting a cookie
- P4 (feature): implement sorting if possible
- P4 (feature): consider implementing a five-star review feature
- P5 (accessibility): when the dust has settled, audit ARIA and HTML

## Contributing

If you encounter a bug in the frontend this theme produces, feel free to use the bug tracker on the [theme's GitLab page](https://gitlab.com/jonatansteller/annobib-theme/issues).

For backend and database bugs please refer to the [plugin's GitLab page](https://gitlab.com/jonatansteller/annobib-plugin/issues) instead.

## Build instructions

These instructions use terminal commands for Linux, but they should translate well to other platforms.

- First-time set-up: this repository should live in your development folder as "annobib-theme". Add a parallel folder "patternfly", open a terminal, switch to the "patternfly" folder using the "cd" command, set up an npm environment by typing "npm", and then install Patternfly with the command "npm i @patternfly/patternfly".
- Update to the latest PatternFly: if your build environment is set up and you want to update to the latest PatternFly release, open a terminal, switch to the "patternfly" folder using the "cd" command, type "npm update" and you are good to go. Remember to "cd .." back to your development folder for the next steps.
- Get the necessary PatternFly assets, step 1: in your development folder, copy the following two folders to "annobib-theme/assets/fonts": "patternfly/node_modules/@patternfly/patternfly/assets/fonts/RedHatText" and "patternfly/node_modules/@patternfly/patternfly/assets/fonts/RedHatDisplay".
- Get the necessary PatternFly assets, step 2: compile the necessary css files for all the components and layouts used in this theme into a single "patternfly.css" file. Use the following command: "cat patternfly/node_modules/@patternfly/patternfly/base/patternfly-globals.css patternfly/node_modules/@patternfly/patternfly/base/patternfly-common.css patternfly/node_modules/@patternfly/patternfly/base/patternfly-variables.css patternfly/node_modules/@patternfly/patternfly/layouts/Bullseye/bullseye.css patternfly/node_modules/@patternfly/patternfly/layouts/Flex/flex.css patternfly/node_modules/@patternfly/patternfly/layouts/Gallery/gallery.css patternfly/node_modules/@patternfly/patternfly/layouts/Grid/grid.css patternfly/node_modules/@patternfly/patternfly/layouts/Split/split.css patternfly/node_modules/@patternfly/patternfly/components/AboutModalBox/about-modal-box.css patternfly/node_modules/@patternfly/patternfly/components/Alert/alert.css patternfly/node_modules/@patternfly/patternfly/components/AlertGroup/alert-group.css patternfly/node_modules/@patternfly/patternfly/components/Backdrop/backdrop.css patternfly/node_modules/@patternfly/patternfly/components/Brand/brand.css patternfly/node_modules/@patternfly/patternfly/components/Button/button.css patternfly/node_modules/@patternfly/patternfly/components/Card/card.css patternfly/node_modules/@patternfly/patternfly/components/Chip/chip.css patternfly/node_modules/@patternfly/patternfly/components/ChipGroup/chip-group.css patternfly/node_modules/@patternfly/patternfly/components/Content/content.css patternfly/node_modules/@patternfly/patternfly/components/Divider/divider.css patternfly/node_modules/@patternfly/patternfly/components/Dropdown/dropdown.css patternfly/node_modules/@patternfly/patternfly/components/EmptyState/empty-state.css patternfly/node_modules/@patternfly/patternfly/components/Form/form.css patternfly/node_modules/@patternfly/patternfly/components/FormControl/form-control.css patternfly/node_modules/@patternfly/patternfly/components/InputGroup/input-group.css patternfly/node_modules/@patternfly/patternfly/components/Label/label.css patternfly/node_modules/@patternfly/patternfly/components/LabelGroup/label-group.css patternfly/node_modules/@patternfly/patternfly/components/ModalBox/modal-box.css patternfly/node_modules/@patternfly/patternfly/components/Nav/nav.css patternfly/node_modules/@patternfly/patternfly/components/OptionsMenu/options-menu.css patternfly/node_modules/@patternfly/patternfly/components/Page/page.css patternfly/node_modules/@patternfly/patternfly/components/Pagination/pagination.css patternfly/node_modules/@patternfly/patternfly/components/Select/select.css patternfly/node_modules/@patternfly/patternfly/components/SkipToContent/skip-to-content.css patternfly/node_modules/@patternfly/patternfly/components/TabContent/tab-content.css patternfly/node_modules/@patternfly/patternfly/components/Tabs/tabs.css patternfly/node_modules/@patternfly/patternfly/components/Title/title.css patternfly/node_modules/@patternfly/patternfly/components/Toolbar/toolbar.css > annobib-theme/assets/patternfly.css".
- Update version numbers: a new PatternFly or theme version number should be indicated in the following files: README.md (top section), style.css (header section), functions.php (in the "annobib_files" function).
