<?php
// Annotated Bibliography Theme
// Sidebar: Blog

$annobib_section	= get_query_var( 'annobib_section' );
$annobib_language	= get_query_var( 'annobib_language' );
$annobib_current	= get_query_var( 'annobib_current' );
$annobib_list			= wp_get_archives( array(
	'type'		=> 'yearly',
	'format'	=> 'html',
	'echo'		=> false ));
$annobib_list			= str_replace( '<li>', '<li class="pf-c-nav__item">', $annobib_list );
$annobib_list			= str_replace( '<a href', '<a class="pf-c-nav__link" href', $annobib_list );
?>


	<!-- PAGE SIDEBAR -->
	<nav class="pf-c-page__sidebar pf-m-light" id="sidebar" aria-labelledby="sidebar-title">
		<div class="pf-c-page__sidebar-body">
			<div class="pf-c-nav pf-m-light">
				<h2 class="annobib-m-hidden" id="sidebar-title" aria-hidden="true"><?php _e( 'Find entries', 'annobib-theme' ); ?></h2>

				<!-- PAGE SIDEBAR: SECTION -->
				<section class="pf-c-nav__section" aria-labelledby="sidebar-category">
					<h3 class="pf-c-nav__section-title annobib-m-hidden" id="sidebar-category"><?php _e( 'By category', 'annobib-theme' ); ?></h3>
					<ul class="pf-c-nav__list">
						<li class="pf-c-nav__item">
							<a href="<?php echo home_url( '/blog' ); ?>" class="pf-c-nav__link<?php if ( $annobib_current == 'blog' ) { echo ' pf-m-current'; } ?>" id="sidebar-category-english"<?php if ( $annobib_current == 'blog' ) { echo ' aria-current="page"'; } ?>><?php _e( 'Overview', 'annobib-theme' ); ?>
								<span class="pf-c-nav__toggle">
									<span class="pf-c-nav__toggle-icon annobib-m-steady">
										<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__entry-home" /></svg>
									</span>
								</span>
							</a>
						</li>
						<li class="pf-c-nav__item">
							<a href="<?php echo home_url( '/en-blog' ); ?>" class="pf-c-nav__link<?php if ( $annobib_current == 'blog-en' ) { echo ' pf-m-current'; } ?>" id="sidebar-category-english"<?php if ( $annobib_current == 'blog-en' ) { echo ' aria-current="page"'; } ?>><?php _e( 'English', 'annobib-theme' ); ?>
								<span class="pf-c-nav__toggle">
									<span class="pf-c-nav__toggle-icon annobib-m-steady">
										<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__entry-blog" /></svg>
									</span>
								</span>
							</a>
						</li>
						<li class="pf-c-nav__item">
							<a href="<?php echo home_url( '/de-blog' ); ?>" class="pf-c-nav__link<?php if ( $annobib_current == 'blog-de' ) { echo ' pf-m-current'; } ?>" id="sidebar-category-german"<?php if ( $annobib_current == 'blog-de' ) { echo ' aria-current="page"'; } ?>><?php _e( 'German', 'annobib-theme' ); ?>
								<span class="pf-c-nav__toggle">
									<span class="pf-c-nav__toggle-icon annobib-m-steady">
										<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__entry-blog" /></svg>
									</span>
								</span>
							</a>
						</li>
					</ul>
				</section>

				<!-- PAGE SIDEBAR: SECTION -->
				<section class="pf-c-nav__section">
					<h3 class="pf-c-nav__section-title" id="sidebar-metadata"><?php _e( 'Find posts', 'annobib-theme' ); ?></h3>
					<ul class="pf-c-nav__list">
						<li class="pf-c-nav__item pf-m-expandable">
							<button type="button" class="pf-c-nav__link annobib-h-menu" id="sidebar-metadata-label" aria-expanded="false"><?php _e( 'Content label', 'annobib-theme' ); ?>
								<span class="pf-c-nav__toggle">
									<span class="pf-c-nav__toggle-icon annobib-m-steady">
										<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__entry-label" /></svg>
									</span>
								</span>
							</button>
							<section class="pf-c-nav__subnav" aria-labelledby="sidebar-metadata-label" hidden>
								<ul class="pf-c-nav__list">
									<?php annobib_navigation( 'post_tag', 'sub', false ); ?>
								</ul>
							</section>
						</li>
						<li class="pf-c-nav__item pf-m-expandable">
							<button type="button" class="pf-c-nav__link annobib-h-menu" id="sidebar-metadata-date" aria-expanded="false"><?php _e( 'Publication date', 'annobib-theme' ); ?>
								<span class="pf-c-nav__toggle">
									<span class="pf-c-nav__toggle-icon annobib-m-steady">
										<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__entry-date" /></svg>
									</span>
								</span>
							</button>
							<section class="pf-c-nav__subnav" aria-labelledby="sidebar-metadata-date" hidden>
								<ul class="pf-c-nav__list">
									<?php echo $annobib_list ?>
								</ul>
							</section>
						</li>
					</ul>
				</section>

			</div>
		</div>
	</nav>
