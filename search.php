<?php
// Annotated Bibliography Theme
// Entry lists

// Set language and section according to URL (or according to browser setting)
if ( strpos( $_SERVER['REQUEST_URI'], '/de' ) != false ) {
	annobib_localise( 'de_DE' );
	set_query_var( 'annobib_language', 'de' );
	set_query_var( 'annobib_section', 'de' );
}
elseif ( strpos( $_SERVER['REQUEST_URI'], '/en' ) != false ) {
	//annobib_localise( 'en_GB' );
	set_query_var( 'annobib_language', 'en' );
	set_query_var( 'annobib_section', 'en' );
}
else {
	if ( substr( $_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2 ) == 'de' ) {
	  annobib_localise( 'de_DE' );
	  set_query_var( 'annobib_language', 'de' );
	}
	else {
	  //annobib_localise( 'en_GB' );
	  set_query_var( 'annobib_language', 'en' );
	}
	set_query_var( 'annobib_section', 'general' );
}
set_query_var( 'annobib_current', 'search' );


// Header
get_header();

// Sidebar
//get_sidebar( 'bibliography' );

// Assemble necessary terms for the term search
$search_taxonomy_de = array(
	'de-curriculum',
	'de-topic',
	'de-context',
	'de-genre',
	'de-writer' );
$search_taxonomy_en = array(
	'en-curriculum',
	'en-topic',
	'en-genre',
	'en-writer',
	'en-country' );
if ( get_query_var( 'annobib_section' ) == 'de' ) {
	$search_taxonomy = $search_taxonomy_de;
}
elseif ( get_query_var( 'annobib_section' ) == 'en' ) {
	$search_taxonomy = $search_taxonomy_en;
}
else {
	$search_taxonomy = array_merge( $search_taxonomy_de, $search_taxonomy_en );
}

// Term search
$search_query = get_search_query();
$search_terms = get_terms( array(
	'search'		=> $search_query,
  'taxonomy'	=> $search_taxonomy ) );
$search_count = count( $search_terms );
set_query_var( 'annobib_searchcount', $search_count );

// List before the loop
get_template_part( 'reusable', 'list-before1' );
get_template_part( 'reusable', 'pagination' );
get_template_part( 'reusable', 'list-before2' );

// Display results of term search
if( $search_count > 0 && $paged == 1 ) {
	foreach ($search_terms as $search_term) {
		echo '<!-- PAGE MAIN: CARDS: CARD -->';
		echo '<article class="pf-c-card pf-m-hoverable pf-m-selectable annobib-m-card-term" role="button" tabindex="0" id="card-' . $search_term->slug . '" data-target="' . get_term_link( $search_term ) . '" aria-labelledby="card-' . $search_term->slug . '-title">';
			echo '<div class="pf-c-card__body pf-c-content">';
				echo '<h2 id="card-' . $search_term->slug . '-title">' . $search_term->name . '</h2>';
			echo '</div>';
		echo '</article>';
	}
	echo '</div>';
}

// Main (filtered in functions.php)
if ( have_posts() ) {
	if ( $search_count > 0 && $paged == 1 ) {
		echo '<hr class="pf-c-divider pf-u-mt-lg pf-u-mb-lg" />';
		echo '<div class="pf-l-gallery pf-m-gutter">';
	}
	while ( have_posts() ) {
		the_post();
		get_template_part( 'reusable', 'card' );
	}
}
elseif ( $search_count == 0 ) {
	get_template_part( 'reusable', 'empty' );
}

// List after the loop
get_template_part( 'reusable', 'list-after1' );
set_query_var( 'annobib_modifier', 'small-openup' );
get_template_part( 'reusable', 'pagination' );
get_template_part( 'reusable', 'list-after2' );

// Footer
get_footer();

?>
