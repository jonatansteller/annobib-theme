<?php
// Annotated Bibliography Theme
// Reusable Component: Back

$annobib_section  = get_query_var( 'annobib_section' );
$annobib_language = get_query_var( 'annobib_language' );
$annobib_current	= get_query_var( 'annobib_current' );
$annobib_modifier = get_query_var( 'annobib_modifier' );
if ( ! $annobib_modifier ) {
  $annobib_modifier = 'free';
}
?>


<button class="pf-c-button pf-m-plain annobib-h-back<?php if ( $annobib_modifier == 'norm' ) { echo ' annobib-m-norm'; } ?>" type="button" aria-label="<?php _e( 'Back to previous view', 'annobib-theme' ); ?>">
  <svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__navigation-back" /></svg>
</button>
