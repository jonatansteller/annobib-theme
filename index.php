<?php
// Annotated Bibliography Theme
// Entry lists

// Set language and section according to URL
if ( strpos( $_SERVER['REQUEST_URI'], '/de-' ) != false ) {
	annobib_localise( 'de_DE' );
	set_query_var( 'annobib_section', 'de' );
	set_query_var( 'annobib_language', 'de' );
}
else {
	//annobib_localise( 'en_GB' );
	set_query_var( 'annobib_section', 'en' );
	set_query_var( 'annobib_language', 'en' );
}
set_query_var( 'annobib_current', 'unknown' );

// Header
get_header();

// Sidebar
get_sidebar( 'bibliography' );

// List before the loop
get_template_part( 'reusable', 'list-before1' );
get_template_part( 'reusable', 'pagination' );
get_template_part( 'reusable', 'list-before2' );

// Main
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		get_template_part( 'reusable', 'card' );
	}
}
else {
	get_template_part( 'reusable', 'empty' );
}

// List after the loop
get_template_part( 'reusable', 'list-after1' );
set_query_var( 'annobib_modifier', 'small-openup' );
get_template_part( 'reusable', 'pagination' );
get_template_part( 'reusable', 'list-after2' );

// Footer
get_footer();

?>
