<?php
// Annotated Bibliography Theme
// Homepage: bibliography (English)

//annobib_localise( 'en_GB' );
set_query_var( 'annobib_section', 'en' );
set_query_var( 'annobib_language', 'en' );
set_query_var( 'annobib_current', 'home-en' );


// Header
get_header();

// Sidebar
get_sidebar( 'bibliography' );

// Main
get_template_part( 'reusable', 'home' );

// Footer
get_footer();

?>
