<?php
// Annotated Bibliography Theme
// Reusable Component: List Before the Loop 1

$annobib_section	= get_query_var( 'annobib_section' );
$annobib_language	= get_query_var( 'annobib_language' );
$annobib_current	= get_query_var( 'annobib_current' );
?>


	<!-- PAGE MAIN -->
	<main role="main" class="pf-c-page__main" tabindex="-1" id="content" aria-labelledby="content-title">

		<!-- PAGE MAIN: CONTENT -->
		<div class="pf-c-page__main-section pf-m-light pf-m-no-fill">
			<div class="pf-c-content">
				<?php if ( is_page() ) {
					the_title( '<h1 id="content-title">', '</h1>', true );
					the_content();
				}
				else {
					the_archive_title( '<h1 id="content-title">', '</h1>' );
					if ( ! is_search() ) {
						the_archive_description( '<p>', '</p>' );
					}
				} ?>
			</div>
		</div>

		<!-- PAGE MAIN: CONTENT -->
		<section class="pf-c-page__main-section pf-m-light pf-m-no-padding" aria-labelledby="content-toolbar">
			<h2 class="annobib-m-hidden" id="content-toolbar" aria-hidden="true"><?php _e( 'Toolbar', 'annobib-theme' ); ?></h2>

			<!-- TOOLBAR -->
			<div class="pf-c-toolbar" id="toolbar">

				<!-- TOOLBAR (First row) -->
				<div class="pf-c-toolbar__content">
					<div class="pf-c-toolbar__content-section">

						<?php if ( is_search() ) :
							get_template_part( 'reusable', 'search' );

						else : ?><!-- TOOLBAR: FILTER GROUP -->
						<div class="pf-c-toolbar__group pf-m-filter-group pf-m-hidden pf-m-visible-on-lg" id="toolbar-filters">
							<h3 class="pf-c-toolbar__item pf-m-label"><?php _e( 'Filters', 'annobib-theme' ); ?></h3>

							<!-- TOOLBAR: FILTER GROUP: SELECT -->
							<div class="pf-c-toolbar__item">
								<div class="pf-c-select">
									<button class="pf-c-select__toggle" type="button" id="toolbar-filters-<?php echo $annobib_section ?>-curriculum" aria-haspopup="listbox" aria-expanded="false" aria-labelledby="toolbar-filters-<?php echo $annobib_section ?>-curriculum" data-target="<?php echo $annobib_section ?>-curriculum">
										<div class="pf-c-select__toggle-wrapper">
											<span class="pf-c-select__toggle-text"><?php _e( 'School', 'annobib-theme' ); ?></span>
										</div>
										<span class="pf-c-select__toggle-arrow">
											<svg class="annobib-c-icon"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__small-opendown" /></svg>
										</span>
									</button>
									<ul class="pf-c-select__menu" aria-labelledby="toolbar-filters-<?php echo $annobib_section ?>-curriculum" hidden>
										<?php annobib_navigation( $annobib_section . '-curriculum', 'filter', false ); ?>
									</ul>
								</div>
							</div>

							<!-- TOOLBAR: FILTER GROUP: SELECT -->
							<div class="pf-c-toolbar__item">
								<div class="pf-c-select">
									<button class="pf-c-select__toggle" type="button" id="toolbar-filters-<?php echo $annobib_section ?>-topic" aria-haspopup="listbox" aria-expanded="false" aria-labelledby="toolbar-filters-<?php echo $annobib_section ?>-topic" data-target="<?php echo $annobib_section ?>-topic">
										<div class="pf-c-select__toggle-wrapper">
											<span class="pf-c-select__toggle-text"><?php _e( 'Topic', 'annobib-theme' ); ?></span>
										</div>
										<span class="pf-c-select__toggle-arrow">
											<svg class="annobib-c-icon"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__small-opendown" /></svg>
										</span>
									</button>
									<ul class="pf-c-select__menu" aria-labelledby="toolbar-filters-<?php echo $annobib_section ?>-topic" hidden>
										<?php annobib_navigation( $annobib_section . '-topic', 'filter', false ); ?>
									</ul>
								</div>
							</div>

							<?php if ( $annobib_section == 'de' ) : ?><!-- TOOLBAR: FILTER GROUP: SELECT -->
							<div class="pf-c-toolbar__item">
								<div class="pf-c-select">
									<button class="pf-c-select__toggle" type="button" id="toolbar-filters-<?php echo $annobib_section ?>-context" aria-haspopup="listbox" aria-expanded="false" aria-labelledby="toolbar-filters-<?php echo $annobib_section ?>-context" data-target="<?php echo $annobib_section ?>-context">
										<div class="pf-c-select__toggle-wrapper">
											<span class="pf-c-select__toggle-text"><?php _e( 'Context', 'annobib-theme' ); ?></span>
										</div>
										<span class="pf-c-select__toggle-arrow">
											<svg class="annobib-c-icon"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__small-opendown" /></svg>
										</span>
									</button>
									<ul class="pf-c-select__menu" aria-labelledby="toolbar-filters-<?php echo $annobib_section ?>-context" hidden>
										<?php annobib_navigation( $annobib_section . '-context', 'filter', false ); ?>
									</ul>
								</div>
							</div><?php endif; ?>

							<!-- TOOLBAR: FILTER GROUP: SELECT -->
							<div class="pf-c-toolbar__item">
								<div class="pf-c-select">
									<button class="pf-c-select__toggle" type="button" id="toolbar-filters-<?php echo $annobib_section ?>-genre" aria-haspopup="listbox" aria-expanded="false" aria-labelledby="toolbar-filters-<?php echo $annobib_section ?>-genre" data-target="<?php echo $annobib_section ?>-genre">
										<div class="pf-c-select__toggle-wrapper">
											<span class="pf-c-select__toggle-text"><?php _e( 'Genre', 'annobib-theme' ); ?></span>
										</div>
										<span class="pf-c-select__toggle-arrow">
											<svg class="annobib-c-icon"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__small-opendown" /></svg>
										</span>
									</button>
									<ul class="pf-c-select__menu" aria-labelledby="toolbar-filters-<?php echo $annobib_section ?>-genre" hidden>
										<?php annobib_navigation( $annobib_section . '-genre', 'filter', false ); ?>
									</ul>
								</div>
							</div>

							<!-- TOOLBAR: FILTER GROUP: SELECT -->
							<div class="pf-c-toolbar__item">
								<div class="pf-c-select">
									<button class="pf-c-select__toggle" type="button" id="toolbar-filters-<?php echo $annobib_section ?>-writer" aria-haspopup="listbox" aria-expanded="false" aria-labelledby="toolbar-filters-<?php echo $annobib_section ?>-writer" data-target="<?php echo $annobib_section ?>-writer">
										<div class="pf-c-select__toggle-wrapper">
											<span class="pf-c-select__toggle-text"><?php _e( 'Author', 'annobib-theme' ); ?></span>
										</div>
										<span class="pf-c-select__toggle-arrow">
											<svg class="annobib-c-icon"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__small-opendown" /></svg>
										</span>
									</button>
									<ul class="pf-c-select__menu" aria-labelledby="toolbar-filters-<?php echo $annobib_section ?>-author" hidden>
										<?php annobib_navigation( $annobib_section . '-writer', 'filter', false ); ?>
									</ul>
								</div>
							</div>

							<?php if ( $annobib_section == 'en' ) : ?><!-- TOOLBAR: FILTER GROUP: SELECT -->
							<div class="pf-c-toolbar__item">
								<div class="pf-c-select">
									<button class="pf-c-select__toggle" type="button" id="toolbar-filters-<?php echo $annobib_section ?>-country" aria-haspopup="listbox" aria-expanded="false" aria-labelledby="toolbar-filters-<?php echo $annobib_section ?>-country" data-target="<?php echo $annobib_section ?>-country">
										<div class="pf-c-select__toggle-wrapper">
											<span class="pf-c-select__toggle-text"><?php _e( 'Country', 'annobib-theme' ); ?></span>
										</div>
										<span class="pf-c-select__toggle-arrow">
											<svg class="annobib-c-icon"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__small-opendown" /></svg>
										</span>
									</button>
									<ul class="pf-c-select__menu" aria-labelledby="toolbar-filters-<?php echo $annobib_section ?>-country" hidden>
										<?php annobib_navigation( $annobib_section . '-country', 'filter', false ); ?>
									</ul>
								</div>
							</div><?php endif; ?>

							<!-- TOOLBAR: FILTER GROUP: SELECT -->
							<div class="pf-c-toolbar__item">
								<div class="pf-c-select">
									<button class="pf-c-select__toggle" type="button" id="toolbar-filters-<?php echo $annobib_section ?>-date" aria-haspopup="listbox" aria-expanded="false" aria-labelledby="toolbar-filters-<?php echo $annobib_section ?>-date" data-target="<?php echo $annobib_section ?>-date">
										<div class="pf-c-select__toggle-wrapper">
											<span class="pf-c-select__toggle-text"><?php _e( 'Date', 'annobib-theme' ); ?></span>
										</div>
										<span class="pf-c-select__toggle-arrow">
											<svg class="annobib-c-icon"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__small-opendown" /></svg>
										</span>
									</button>
									<ul class="pf-c-select__menu" aria-labelledby="toolbar-filters-<?php echo $annobib_section ?>-date" hidden>
										<li>
											<button type="button" class="pf-c-select__menu-item annobib-h-filter" aria-pressed="false" data-target="date-older" id="toolbar-filters-<?php echo $annobib_section ?>-date-date-older"><?php _e( 'Before 1945', 'annobib-theme' ); ?></button>
										</li>
										<li>
											<button type="button" class="pf-c-select__menu-item annobib-h-filter" aria-pressed="false" data-target="date-1945" id="toolbar-filters-<?php echo $annobib_section ?>-date-date-1945"><?php _e( '1945 to 1988', 'annobib-theme' ); ?></button>
										</li>
										<li>
											<button type="button" class="pf-c-select__menu-item annobib-h-filter" aria-pressed="false" data-target="date-1989" id="toolbar-filters-<?php echo $annobib_section ?>-date-date-1989"><?php _e( '1989 to 1999', 'annobib-theme' ); ?></button>
										</li>
										<li>
											<button type="button" class="pf-c-select__menu-item annobib-h-filter" aria-pressed="false" data-target="date-2000" id="toolbar-filters-<?php echo $annobib_section ?>-date-date-2000"><?php _e( '2000 to 2009', 'annobib-theme' ); ?></button>
										</li>
										<li>
											<button type="button" class="pf-c-select__menu-item annobib-h-filter" aria-pressed="false" data-target="date-2010" id="toolbar-filters-<?php echo $annobib_section ?>-date-date-2010"><?php _e( 'Since 2010', 'annobib-theme' ); ?></button>
										</li>
									</ul>
								</div>
							</div>

						</div>

						<!-- TOOLBAR: BUTTON GROUP -->
						<div class="pf-c-toolbar__group pf-m-icon-button-group">

							<!-- TOOLBAR: BUTTON GROUP: BUTTON -->
							<div class="pf-c-toolbar__toggle pf-m-hidden-on-lg">
								<button class="pf-c-button pf-m-plain annobib-h-nav" type="button" aria-label="<?php _e( 'Filters toggle', 'annobib-theme' ); ?>" aria-expanded="false" aria-controls="toolbar-filters" data-target="#toolbar-filters">
									<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__menu-filter" /></svg>
								</button>
							</div>

							<!-- TOOLBAR: BUTTON GROUP: OPTIONS MENU -->
							<div class="pf-c-toolbar__item">
								<div class="pf-c-options-menu">
									<button class="pf-c-options-menu__toggle pf-m-plain" type="button" id="toolbar-sort" aria-haspopup="listbox" aria-expanded="false" aria-label="<?php _e( 'Sort by', 'annobib-theme' ); ?>" disabled>
										<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__menu-sort" /></svg>
									</button>
									<ul class="pf-c-options-menu__menu" aria-labelledby="toolbar-sort" hidden>
										<li>
											<button class="pf-c-options-menu__menu-item annobib-h-selection" type="button" data-target="sort-added" aria-disabled="true" aria-pressed="true" disabled><?php _e( 'Sort by date added', 'annobib-theme' ); ?>
												<span class="pf-c-options-menu__menu-item-icon annobib-h-selection-checkmark">
													<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__small-checked" /></svg>
												</span>
											</button>
										</li>
										<li>
											<button class="pf-c-options-menu__menu-item annobib-h-selection" type="button" data-target="sort-publication"><?php _e( 'Sort by publication date', 'annobib-theme' ); ?></button>
										</li>
										<li>
											<button class="pf-c-options-menu__menu-item annobib-h-selection" type="button" data-target="sort-author"><?php _e( 'Sort by author', 'annobib-theme' ); ?></button>
										</li>
									</ul>
								</div>
							</div>

						</div><?php endif; ?>

						<!-- TOOLBAR: PAGINATION -->
						<nav class="pf-c-toolbar__item pf-m-pagination" aria-labelledby="toolbar-pagination">
							<div class="pf-c-pagination">
								<h3 class="annobib-m-hidden" id="toolbar-pagination"><?php _e( 'Pagination', 'annobib-theme' ); ?></h3>
								<div class="pf-c-pagination__nav" aria-label="<?php _e( 'Pagination', 'annobib-theme' ); ?>">
