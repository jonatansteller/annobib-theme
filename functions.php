<?php
// Annotated Bibliography Theme
// Functions to use throughout the theme


// SET UP THEME WHEN ACTIVATED
function annobib_setup() {

	// Folder containing translation strings
	load_theme_textdomain( 'annobib-theme', get_template_directory() . '/languages' );

	// Content width for embeds and images
	if ( ! isset( $content_width ) ) {
		$content_width = 735;
	}

	// Add thumbnail support for cover images
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 600, 600 );

	// Activate other standard features
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'responsive-embeds' );
	add_theme_support( 'customize-selective-refresh-widgets' );
	add_theme_support( 'disable-custom-font-sizes' );
	add_theme_support( 'disable-custom-colors' );
	add_theme_support( 'html5', array(
		'comment-list',
		'comment-form',
		'search-form',
		'gallery',
		'caption',
		'script',
		'style'
	) );

	// Do not activate remaining standard features
	// add_theme_support( 'post-formats' );
	// add_theme_support( 'custom-background' );
	// add_theme_support( 'custom-header' );
	// add_theme_support( 'custom-logo' );
	// add_theme_support( 'starter-content' );
	// add_theme_support( 'wp-block-styles' );
	// add_theme_support( 'editor-styles' );
	// add_theme_support( 'editor-font-sizes' );
	// add_theme_support( 'editor-color-pallete' );
	// add_theme_support( 'dark-editor-style' );
	// add_theme_support( 'align-wide' );
	// add_editor_style()
}
add_action( 'after_setup_theme', 'annobib_setup' );


// ADD SCRIPTS AND STYLES
function annobib_files() {
	wp_enqueue_style( 'patternfly', get_template_directory_uri() . '/assets/patternfly.css', array(), '4.103.6', 'all' );
	wp_enqueue_style( 'annobib', get_stylesheet_uri(), array(), '0.972', 'all' );
	wp_enqueue_script( 'functions', get_template_directory_uri() . '/functions.js', array(), '0.97', false );
}
add_action( 'wp_enqueue_scripts', 'annobib_files' );


// ADD AND REMOVE FEATURES
function annobib_features() {

	// Add customisable menus
  register_nav_menus( array(
		'shortcuts-en' => __( 'Shortcuts (English)', 'annobib-theme' ),
		'shortcuts-de' => __( 'Shortcuts (German)', 'annobib-theme' )
  ));

	// Remove emojis
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );

	// Remove DNS prefetch
	remove_action( 'wp_head', 'wp_resource_hints', 2 );
}
add_action( 'init', 'annobib_features' );


// SHOW STATISTICS PLUGIN FOR ALL USERS
add_filter( 'statify__user_can_see_stats', '__return_true' );


// MODIFY THUMBNAIL HTML
function annobib_thumbnail( $html ) {
	return preg_replace( '/(width|height)="\d*"/', '', $html );
}
add_filter( 'post_thumbnail_html', 'annobib_thumbnail' );


// MODIFY ARCHIVE TITLE
function annobib_archive( $title ) {
	if ( is_search() ) {
		$title = sprintf( __( 'Search term: %s', 'annobib-theme' ), get_search_query() );
	}
	elseif ( is_tax( [ 'en-genre', 'de-genre' ] ) ) {
		$title = sprintf( __( 'Genre: %s', 'annobib-theme' ), single_term_title( '', false ) );
	}
	elseif ( is_tax( [ 'en-writer', 'de-writer' ] ) ) {
		$title = sprintf( __( 'Author: %s', 'annobib-theme' ), single_term_title( '', false ) );
	}
	elseif ( is_tax( [ 'en-country', 'de-country' ] ) ) {
		$title = sprintf( __( 'Country: %s', 'annobib-theme' ), single_term_title( '', false ) );
	}
	elseif ( is_tax( [ 'en-adaptation', 'de-adaptation' ] ) ) {
		$title = sprintf( __( 'Adaptation genre: %s', 'annobib-theme' ), single_term_title( '', false ) );
	}
	elseif ( is_tax( [ 'en-curriculum', 'de-curriculum' ] ) ) {
		$title = sprintf( __( 'School: %s', 'annobib-theme' ), single_term_title( '', false ) );
	}
	elseif ( is_tax( [ 'en-topic', 'de-topic' ] ) ) {
		$title = sprintf( __( 'Topic: %s', 'annobib-theme' ), single_term_title( '', false ) );
	}
	elseif ( is_tax( [ 'en-context', 'de-context' ] ) ) {
		$title = sprintf( __( 'Context: %s', 'annobib-theme' ), single_term_title( '', false ) );
	}
	elseif ( is_tax( [ 'en-criterion', 'de-criterion' ] ) ) {
		$title = sprintf( __( 'Selection criterion: %s', 'annobib-theme' ), single_term_title( '', false ) );
	}
	elseif ( is_post_type_archive() ) {
		$title = sprintf( __( '%s', 'annobib-theme' ), post_type_archive_title( '', false ) );
	}
	return $title;
}
add_filter( 'get_the_archive_title', 'annobib_archive' );


// REMOVE BLOG POSTS AND PAGES FROM USER-FACING SEARCH
function annobib_search( $query ) {
	if ( ! $query->is_admin && $query->is_search ) {
		if ( strpos( $_SERVER['REQUEST_URI'], '/de' ) != false ) {
			$query->set( 'post_type', array( 'bibliography-de' ));
		}
		elseif ( strpos( $_SERVER['REQUEST_URI'], '/en' ) != false ) {
			$query->set( 'post_type', array( 'bibliography-en' ));
		}
		else {
			$query->set( 'post_type', array( 'bibliography-de', 'bibliography-en' ));
		}
	}
	return $query;
}
add_filter( 'pre_get_posts', 'annobib_search' );


// LOCALE SWITCH FOR USE IN NON-ENGLISH, LANGUAGE-SPECIFIC THEME FILES
function annobib_localise( $locale ) {
	switch_to_locale( $locale );
	load_theme_textdomain( 'annobib-theme', get_template_directory() . '/languages' );
}


// APPLY FILTERS TO THE NEXT LOOP
function annobib_filter( $query ) {

	// Only execute this when there is a filter string
	if ( isset( $_POST['toolbar-filterform-filters'] ) ) {

		// Sanitise the filter string and turn it into an array
		$filtersActive = $_POST['toolbar-filterform-filters'];
		$filtersActive = sanitize_text_field( trim( $filtersActive ) );
		$filtersActive = str_replace( '\\', '', $filtersActive );
		$filtersArray = json_decode( $filtersActive, true );

		// For security, only allow a given set of taxonomies ('-date' has a separate logic)
		$filtersKeys = [
			'en-curriculum',
			'de-curriculum',
			'en-topic',
			'de-topic',
			'en-context',
			'de-context',
			'en-genre',
			'de-genre',
			'en-writer',
			'de-writer',
			'en-country',
			'de-country'
		];

		// Start compiling the query array
		$filtersQuery = [];
		$filtersQuery[ 'relation' ] = 'AND';

		// Set filter check and loop through keys
		$filtersCheckRegular = false;
		foreach ( $filtersKeys as $key ) {
			$filtersCheckRegularCounter = 0;
			foreach ( $filtersArray as $term ) {
				if ( $term[2] == $key ) { // See function.js for documentation of the array
					$filtersCheckRegularCounter++;

					// Add this key to the query array
					if ( $filtersCheckRegularCounter == 1 ) {
						$filtersQueryTaxonomy = [];
						$filtersQueryTaxonomy[ 'taxonomy' ] = $key;
						$filtersQueryTaxonomy[ 'field' ] = 'slug';
						$filtersQueryTaxonomyTerms = [];
					}

					// Add terms for this key to the query array
					$filtersCheckRegular = true;
					$filtersQueryTaxonomyTerms[] = $term[0];
				}
			}

			// Round out the query array for this key
			if ( $filtersCheckRegularCounter >= 1 ) {
				$filtersQueryTaxonomy[ 'terms' ] = $filtersQueryTaxonomyTerms;
				$filtersQuery[] = $filtersQueryTaxonomy;
			}
		}

		// Start compiling the specialised date query array
		$filtersDate = [];
		$filtersDateMetaTerms = [];
		foreach ( $filtersArray as $term ) {
			if ( $term[2] == 'en-date' || $term[2] == 'de-date' ) {
				$filtersDateMetaTerms[] = $term[0];
			}
		}

		// Set filter check and loop through items
		$filtersCheckDate = false;
		foreach ( $filtersDateMetaTerms as $term ) {

			// Add filters for this date to the date query array
			$filtersDateMeta = [];
			$filtersDateMeta[ 'key' ] = 'annobib-meta-year';
			if ( $term == 'date-older' ) {
				$filtersCheckDate = true;
				$filtersDateMeta[ 'value' ] = array( -1000, 1944 );
			}
			if ( $term == 'date-1945' ) {
				$filtersCheckDate = true;
				$filtersDateMeta[ 'value' ] = array( 1945, 1988 );
			}
			if ( $term == 'date-1989' ) {
				$filtersCheckDate = true;
				$filtersDateMeta[ 'value' ] = array( 1989, 1999 );
			}
			if ( $term == 'date-2000' ) {
				$filtersCheckDate = true;
				$filtersDateMeta[ 'value' ] = array( 2000, 2009 );
			}
			if ( $term == 'date-2010' ) {
				$filtersCheckDate = true;
				$filtersDateMeta[ 'value' ] = array( 2010, 2050 );
			}
			$filtersDateMeta[ 'compare' ] = 'BETWEEN';
			$filtersDateMeta[ 'type' ] = 'NUMERIC';
			$filtersDate[] = $filtersDateMeta;
		}

		// Show all entries without pagination
		if ( $filtersCheckRegular || $filtersCheckDate ) {
			$query->set( 'posts_per_page', -1 );
		}

		// Apply regular and date filters
		if ( $filtersCheckRegular ) {
			$query->set( 'tax_query', $filtersQuery );
		}
		if ( $filtersCheckDate ) {
			$query->set( 'meta_query', $filtersDate );
		}

		// Remove the function from successive loops on the same page
		remove_action( 'pre_get_posts', 'annobib_filter' );
		remove_action( 'pre_get_posts', 'annobib_filter_index' );
	}
}


// WRAPPER TO APPLY THE FILTER FUNCTION ABOVE TO INDEX.PHP
function annobib_filter_index( $query ) {
	if ( is_archive() && $query->is_main_query() ) {
		annobib_filter( $query );
	}
}
add_action( 'pre_get_posts', 'annobib_filter_index' );


// RETRIEVE SIMPLE INFORMATION FOR USE IN CARDS AND ENTRY PAGES
function annobib_retrieve( $post, $annobib_section, $information ) {

	// Slug
	if ( $information == 'slug' ) {
		$slug = $post->post_name;
		return $slug;
	}

	// Year
	elseif ( $information == 'year' ) {
		$year				= get_post_meta( $post->ID, 'annobib-meta-year', true );
		$yearGuess	= get_post_meta( $post->ID, 'annobib-meta-year-guess', true );
		if ( $yearGuess ) {
			$year = __( 'c.', 'annobib-theme' ) . ' ' . $year;
		}
		return $year;
	}

	// Entry icon
	elseif ( $information == 'entryicon' ) {
		$uniqueidType = get_post_meta( $post->ID, 'annobib-meta-uniqueid-type', true );
		if ( $uniqueidType == 'DOI' ) {
			$entryicon = 'small-article';
		}
		elseif ( $uniqueidType == 'ISSN' ) {
			$entryicon = 'small-journal';
		}
		elseif ( $uniqueidType == 'URL' ) {
			$entryicon = 'small-website';
		}
		else { // 'ISBN'
			$entryicon = 'small-book';
		}
		return $entryicon;
	}

	// Citation
	elseif ( $information == 'citation' ) {
		$citation = get_post_meta( $post->ID, 'annobib-meta-citation', true );
		return $citation;
	}

	// Addenda
	elseif ( $information == 'addenda' ) {
		$uniqueid				= get_post_meta( $post->ID, 'annobib-meta-uniqueid-id', true );
		$uniqueidType		= get_post_meta( $post->ID, 'annobib-meta-uniqueid-type', true );
		$contentrating	= get_post_meta( $post->ID, 'annobib-meta-contentrating', true );
		$lengthAmount		= get_post_meta( $post->ID, 'annobib-meta-length-amount', true );
		$lengthUnit			= get_post_meta( $post->ID, 'annobib-meta-length-unit', true );

		// Define units for length data
		if ( $lengthUnit == 'pages' ) {
			$lengthUnit = __( 'pp.', 'annobib-theme' );
		}
		elseif ( $lengthUnit == 'lines' ) {
			$lengthUnit = __( 'l.', 'annobib-theme' );
		}
		elseif ( $lengthUnit == 'minutes' ) {
			$lengthUnit = __( 'min.', 'annobib-theme' );
		}

		// Add length data
		$addenda = '';
		if ( $lengthAmount ) {
			$addenda .= $lengthAmount . ' ' . $lengthUnit;
		}

		// Add content rating
		if ( $contentrating && $contentrating != 'false' ) {
			if ( $addenda != '' ) {
				$addenda .= ', ';
			}
			$addenda .= $contentrating;
		}

		// Add unique ID
		if ( $uniqueid ) {
			if ( $uniqueidType == 'ISBN' ) {
				$uniqueidURL = 'https://' . $annobib_section . '.wikipedia.org/wiki/Special:BookSources/' . preg_replace( '/\D/', '', $uniqueid );
				$uniqueidLead = $uniqueidType . ' ';
				$uniqueidText = $uniqueid;
			}
			elseif ( $uniqueidType == 'DOI' ) {
				$uniqueidURL = 'https://doi.org/' . $uniqueid;
				$uniqueidLead = $uniqueidType . ' ';
				$uniqueidText = $uniqueid;
			}
			elseif ( $uniqueidType == 'ISSN' ) {
				$uniqueidURL = 'https://portal.issn.org/resource/ISSN/' . $uniqueid;
				$uniqueidLead = $uniqueidType . ' ';
				$uniqueidText = $uniqueid;
			}
			else { // URL
				$uniqueidURL = $uniqueid;
				$uniqueidLead = '';
				$uniqueidText = __( 'Website', 'annobib-theme' );
			}
			if ( $addenda != '' ) {
				$addenda .= ', ';
			}
			$addenda .= $uniqueidLead . '<a href="' . $uniqueidURL . '" target="_blank" rel="noreferrer noopener">' . $uniqueidText . '</a>';
		}

		// Return collated string
		return $addenda;
	}

	// Useful links
	elseif ( $information == 'usefullinks' ) {
		$usefullinks = get_post_meta( $post->ID, 'annobib-meta-usefullinks', true );
		if ( $usefullinks == false || $usefullinks == '<li></li>' ) {
			$usefullinks = '';
		}
		return $usefullinks;
	}

	// Error
	else {
		return '';
	}
}

// IDENTIFY THE ICON TO USE
function annobib_icon( $iconUniform, $iconAlternative ) {
	$iconUse = $iconUniform;

	// Match the alternative icon if no uniform icon is specified
	if ( $iconUniform == false ) {
		if ( $iconAlternative == 'audiobook' || $iconAlternative == 'audio-drama' || $iconAlternative == 'recording' || $iconAlternative == 'podcast' ) {
			$iconUse = 'type-audio';
		}
		elseif ( $iconAlternative == 'graphic-novel' || $iconAlternative == 'comic' || $iconAlternative == 'comic-book' ) {
			$iconUse = 'type-comic';
		}
		elseif ( $iconAlternative == 'easy-reading-edition' || $iconAlternative == 'easy-reading' || $iconAlternative == 'simple' || $iconAlternative == 'simple-language' ) {
			$iconUse = 'type-easy';
		}
		elseif ( $iconAlternative == 'film' || $iconAlternative == 'video' || $iconAlternative == 'movie' || $iconAlternative == 'tv-series' || $iconAlternative == 'television' || $iconAlternative == 'live-recording' ) {
			$iconUse = 'type-film';
		}
		elseif ( $iconAlternative == 'musical' || $iconAlternative == 'opera' || $iconAlternative == 'audio' || $iconAlternative == 'music' ) {
			$iconUse = 'type-music';
		}
		elseif ( $iconAlternative == 'play' || $iconAlternative == 'theatre' || $iconAlternative == 'stage' ) {
			$iconUse = 'type-play';
		}
		elseif ( $iconAlternative == 'video-game' || $iconAlternative == 'computer-game' || $iconAlternative == 'console-game' || $iconAlternative == 'game' ) {
			$iconUse = 'type-videogame';
		}
		else { // novel, book, monograph, collection, collected-volume, volume, original
			$iconUse = 'type-book';
		}
	}

	// Return
	return $iconUse;
}


// PRODUCE THE HTML CODE OF ICON LISTS FOR USE IN CARDS AND ENTRY PAGES
function annobib_iconlist( $postid, $taxonomy, $icon ) {

	// Compile terms
	$output = '';

	// Get an array of terms
	$terms = get_the_terms( $postid, $taxonomy );
	if ( $terms && ! is_wp_error( $terms ) ) {

		// Routine for each term that is not a child of a parent
		foreach ( $terms as $term ) {
			if ( $term->parent == 0 ) {

				// Identify the icon if necessary
				$iconUse = annobib_icon( $icon, $term->slug );

				// Add to the output string
				$output .= '<li><span class="annobib-c-iconlist__icon"><svg class="annobib-c-icon" aria-hidden="true"><use href="' . get_template_directory_uri() . '/assets/images/annobib-c-icon.svg#annobib-c-icon__' . $iconUse . '" /></svg></span><span class="annobib-c-iconlist__label">' . $term->name . '</span></li>';
			}
		}

		// Collect the IDs of all parents to display because they are a special case
		$parents = array();
		foreach ( $terms as $term ) {
			if ( $term->parent != 0 && ! in_array( $term->parent, $parents, true ) ) {
				$parents[] = $term->parent;
			}
		}

		// Assemble an entry for each parent
		foreach ( $parents as $parent ) {
			$parentTerm = get_term_by( 'id', $parent, $taxonomy );

			// Identify the icon if necessary
			$iconUse = annobib_icon( $icon, $parentTerm->slug );

			// Collate a name
			$childcounter = 0;
			$parentName = $parentTerm->name;
			foreach ( $terms as $term ) {
				if ( $term->parent == $parent ) {
					$childcounter++;

					// Add to the parent's name
					if ( $childcounter == 1 ) {
						$parentName .= ': ';
					}
					else {
						$parentName .= ', ';
					}
					$parentName .= $term->name;
				}
			}

			// Add to the output string
			$output .= '<li><span class="annobib-c-iconlist__icon"><svg class="annobib-c-icon" aria-hidden="true"><use href="' . get_template_directory_uri() . '/assets/images/annobib-c-icon.svg#annobib-c-icon__' . $iconUse . '" /></svg></span><span class="annobib-c-iconlist__label">' . $parentName . '</span></li>';
		}
	}

	// Print the icon list
	return $output;
}


// PRODUCE THE HTML CODE OF LABEL GROUPS FOR USE IN CARDS AND ENTRY PAGES
function annobib_labelgroup( $postid, $taxonomy ) {

	// Compile terms
	$output = '';

	// Get an array of terms
	$terms = wp_get_post_terms( $postid, $taxonomy, array(
		'orderby'	=> 'slug' // Helps with manually sorted taxonomies
	));
	if ( $terms && ! is_wp_error( $terms ) ) {

		// Identify top-level terms
		$parents = get_terms( array(
			'taxonomy' => $taxonomy,
			'orderby'	=> 'slug', // Helps with manually sorted taxonomies
			'parent' => 0,
			'hide_empty' => false // useful to preview unpublished posts which are the first ones in their taxonomy
		) );

		// Routine for each top-level term
		foreach ( $parents as $parent ) {
			$childcounter = 0;
			$childcounterClose = '';

			// Routine for each lower-level term with this top-level parent
			foreach( $terms as $term ) {
				if ( $parent->term_id == $term->parent ) {

					// Only write the top-level list item once
					$childcounter++;
					if ( $childcounter == 1 ) {

						// Add parent item to compilation
						$output .= '<div class="annobib-l-columns__item"><h3 class="pf-c-title pf-m-md pf-u-mt-sm pf-u-mb-md annobib-m-block-title"><a href="' . get_term_link( $parent->slug, $taxonomy ) . '">' . $parent->name . '</a></h3><div class="pf-c-label-group"><ul class="pf-c-label-group__list" role="list">';
						$childcounterClose = '</ul></div></div>';
					}

					// Add regular item to compilation
					$output .= '<li class="pf-c-label-group__list-item"><span class="pf-c-label pf-m-outline"><a class="pf-c-label__content" href="' . get_term_link( $term->slug, $taxonomy ) . '"><span class="pf-c-label__text">' . $term->name . '</span></a></span></li>';
				}
			}

			// Close the parent list item
			$output .= $childcounterClose;
		}
	}

	// Print the label group
	return $output;
}


// LIST NAVIGATION ENTRIES BASED ON TAXONOMIES
function annobib_navigation( $taxonomy, $type, $superordinateLabel ) {

	// Compile terms
	$output = '';

	// Get an array of terms
	$terms = get_terms( array (
		'taxonomy'			=> $taxonomy,
		'orderby'				=> 'slug', // Helps with manually sorted taxonomies
		'order'					=> 'ASC',
		'hide_empty'		=> true,
		'count'					=> false,
		'hierarchical'	=> true
	));
	if ( ! empty( $terms ) ) {

		// Routine for each top-level term
		foreach ( $terms as $term ) {
			if ( $term->parent == 0 ) {
				$childcounter = 0;
				$childcounterClose = '';

				// Route A: if the term has subterms
        foreach ( $terms as $subterm ) {
					if ( $subterm->parent == $term->term_id ) {

						// Along with the first subterm, add top-level entry and sub-entry
						$childcounter++;
						if ( $childcounter == 1 ) {

							// Variant 1: Superordinate
							if ( $type == 'super' ) {
								$output .= '<li class="pf-c-nav__item pf-m-expandable"><button type="button" class="pf-c-nav__link annobib-h-menu" id="sidebar-' . $superordinateLabel . '-' . $term->slug . '" aria-expanded="false">' . $term->name . '<span class="pf-c-nav__toggle"><span class="pf-c-nav__toggle-icon"><svg class="annobib-c-icon" aria-hidden="true"><use href="' . get_template_directory_uri() . '/assets/images/annobib-c-icon.svg#annobib-c-icon__small-openright" /></svg></span></span></button><section class="pf-c-nav__subnav" aria-labelledby="sidebar-' . $superordinateLabel . '-' . $term->slug . '" hidden><ul class="pf-c-nav__list"><li class="pf-c-nav__item"><a href="' . get_term_link( $term->slug, $taxonomy ) . '" class="pf-c-nav__link">' . __( 'All entries', 'annobib-theme' ) . '</a></li>';
								$childcounterClose = '</ul></section></li>';
							}

							// Variant 2: Subordinate
							elseif ( $type == 'sub' ) {
								$output .= '<li class="pf-c-nav__item"><a href="' . get_term_link( $term->slug, $taxonomy ) . '" class="pf-c-nav__link">' . $term->name . '</a><ul class="pf-c-nav__list">';
								$childcounterClose = '</ul></li>';
							}

							// Variant 3: Filter
							elseif ( $type == 'filter' ) {
								$output .= '<li><button type="button" class="pf-c-select__menu-item annobib-h-filter" aria-selected="false" data-target="' . $term->slug . '" id="toolbar-filters-' . $term->taxonomy . '-' . $term->slug . '">' . $term->name . '</button><ul>';
								$childcounterClose = '</ul></li>';
							}

							// Variant 4: List
							elseif ( $type == 'list' ) {
								$output .= '<li>' . $term->name . '<ul>';
								$childcounterClose = '</ul></li>';
							}
						}

						// Add subterms
						// Variant 1: superordinate
						if ( $type == 'super' ) {
							$output .= '<li class="pf-c-nav__item"><a href="' . get_term_link( $subterm->slug, $taxonomy ) . '" class="pf-c-nav__link">' . $subterm->name . '</a></li>';
						}

						// Variant 2: Subordinate
						elseif ( $type == 'sub' ) {
							$output .= '<li class="pf-c-nav__item"><a href="' . get_term_link( $subterm->slug, $taxonomy ) . '" class="pf-c-nav__link">' . $subterm->name . '</a></li>';
						}

						// Variant 3: Filter
						elseif ( $type == 'filter' ) {
							$output .= '<li><button type="button" class="pf-c-select__menu-item annobib-h-filter" aria-selected="false" data-target="' . $subterm->slug . '" id="toolbar-filters-' . $subterm->taxonomy . '-' . $subterm->slug . '">' . $subterm->name . '</button></li>';
						}

						// Variant 4: List
						elseif ( $type == 'list' ) {
							$output .= '<li>' . $subterm->name . '</li>';
						}
					}
        }

				// Route B: if the term has no children
				if ( $childcounter == 0 ) {

					// Variant 1: superordinate
					if ( $type == 'super' ) {
						$output .= '<li class="pf-c-nav__item"><a href="' . get_term_link( $term->slug, $taxonomy ) . '" class="pf-c-nav__link" id="sidebar-' . $superordinateLabel . '-' . $term->slug . '">' . $term->name . '</a></li>';
					}

					// Variant 2: Subordinate
					elseif ( $type == 'sub' ) {
						$output .= '<li class="pf-c-nav__item"><a href="' . get_term_link( $term->slug, $taxonomy ) . '" class="pf-c-nav__link">' . $term->name . '</a></li>';
					}

					// Variant 3: Filter
					elseif ( $type == 'filter' ) {
						$output .= '<li><button type="button" class="pf-c-select__menu-item annobib-h-filter" aria-selected="false" data-target="' . $term->slug . '" id="toolbar-filters-' . $term->taxonomy . '-' . $term->slug . '">' . $term->name . '</button></li>';
					}

					// Variant 4: List
					elseif ( $type == 'list' ) {
						$output .= '<li>' . $term->name . '</li>';
					}
				}

				// Finish top-level entry
				$output .= $childcounterClose;
			}
		}
	}

	// Output the result
	echo $output;
}


// LIST TAXONOMIES AS SELECT OPTIONS
function annobib_select( $taxonomy ) {

	// Compile terms
	$output = '';

	// Get an array of terms
	$terms = get_terms( array (
		'taxonomy'			=> $taxonomy,
		'orderby'				=> 'slug', // Helps with manually sorted taxonomies
		'order'					=> 'ASC',
		'hide_empty'		=> false,
		'count'					=> false,
		'hierarchical'	=> true
	));
	if ( ! empty( $terms ) ) {

		// Routine for each top-level term
		foreach ( $terms as $term ) {
			if ( $term->parent == 0 ) {
				$childcounter = 0;

				// Route A: if the term has subterms
        foreach ( $terms as $subterm ) {
					if ( $subterm->parent == $term->term_id ) {

						// Add top-level entry
						$childcounter++;
						if ( $childcounter == 1 ) {
							$output .= '<option value="' . $term->name . '">' . $term->name . '</option>';
						}

						// Add subterms
						$output .= '<option value="' . $term->name . ': ' . $subterm->name . '">- ' . $subterm->name . '</option>';
					}
        }

				// Route B: if the term has no children
				if ( $childcounter == 0 ) {
					$output .= '<option value="' . $term->name . '">' . $term->name . '</option>';
				}
			}
		}
	}

	// Output the result
	echo $output;
}

?>
