<?php
// Annotated Bibliography Theme
// Blog: by date

// Set language according to browser setting
if ( substr( $_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2 ) == 'de' ) {
  annobib_localise( 'de_DE' );
  set_query_var( 'annobib_language', 'de' );
}
else {
  //annobib_localise( 'en_GB' );
  set_query_var( 'annobib_language', 'en' );
}
set_query_var( 'annobib_section', 'blog' );
set_query_var( 'annobib_current', 'blog-date' );


// Header
get_header();

// Sidebar
get_sidebar( 'blog' );

// Blog before the loop
get_template_part( 'reusable', 'blog-before' );

// Main
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		get_template_part( 'reusable', 'blog-post' );
	}
}
else {
	get_template_part( 'reusable', 'empty' );
}

// List after the loop
get_template_part( 'reusable', 'blog-after1' );
set_query_var( 'annobib_modifier', 'small-openup' );
get_template_part( 'reusable', 'pagination' );
get_template_part( 'reusable', 'blog-after2' );

// Footer
get_footer();

?>
