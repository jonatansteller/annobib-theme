<?php /* Template Name: List Criteria (German) */

// Annotated Bibliography Theme
// Post or Page: crtieria (German)

annobib_localise( 'de_DE' );
set_query_var( 'annobib_section', 'de' );
set_query_var( 'annobib_language', 'de' );
set_query_var( 'annobib_current', 'post' );
$annobib_section	= get_query_var( 'annobib_section' );


// Header
get_header(); ?>

	<!-- PAGE MAIN -->
	<main role="main" class="pf-c-page__main" tabindex="-1" id="content" aria-labelledby="content-title">

<?php // Main
if ( have_posts() ) :
	while ( have_posts() ) :
		the_post();
		get_template_part( 'reusable', 'blog-post' ); ?>

		<div class="pf-c-page__main-section pf-m-light pf-m-no-fill annobib-m-nowhitespace">
			<div class="pf-l-bullseye annobib-m-mono">
				<div class="pf-l-bullseye__item pf-c-content">
					<h2><?php _e( 'Selection criteria', 'annobib-theme' ); ?></h2>
					<ul>
						<?php annobib_navigation( $annobib_section . '-criterion', 'list', false ); ?>
					</ul>
				</div>
			</div>
		</div>

	<?php endwhile;
else :
	get_template_part( 'reusable', 'error' );
endif; ?>

	</main>

<?php // Footer
get_footer();

?>
