<?php
// Annotated Bibliography Theme
// Header

$annobib_section	= get_query_var( 'annobib_section' );
$annobib_language	= get_query_var( 'annobib_language' );
$annobib_current	= get_query_var( 'annobib_current' );
$annobib_modifier	= get_query_var( 'annobib_modifier' );
if ( ! $annobib_modifier ) {
	$annobib_modifier = 'full';
}
?>


<!DOCTYPE html>
<html <?php language_attributes(); ?><?php if ( is_admin_bar_showing() ) { echo ' class="annobib-m-adminbar"'; } ?>>


<head>

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- GOT A NAME FOR THIS RAPTOR?
	     LET ME KNOW!
	     JONATAN.STELLER@UNI-LEIPZIG.DE

                                                     ___._
                                                   .'  <0>'-.._
                                                  /  /.··.____")
                                                 |   \   __.-'~
                                                 |  :  -'/
                                                /:.  :.-'
__________                                     | : '. |
'··.____  '········.______       _.····.····-./      :/
        '··.__            `'····/       '-.      __ :/
              '-.___           :           \   .'  )/
                    '··-._           _.-'   ] /  _/
                         '-._      _/     _/ / _/
                             \_ .-'____.-'__< |  \___
                               <_______.\    \_\_···.7
                              |   /'=r_.-'     _\\ =/
                          .··'   /            ._/'>
                        .'   _.-'
                       / .··'
                      /,/
                      |/`)
                      'c=, -->

	<!-- PWA: CURRENT AND LEGACY RESOURCES -->
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/manifest.json">
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/icon-shortcut-512.png">
	<meta name="theme-color" content="#9a1b60">

	<!-- PWA: MICROSOFT LEGACY -->
	<meta name='application-name' content='<?php bloginfo( 'name' ); ?>'>
	<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/assets/images/icon-tile-512.png">
	<meta name="msapplication-TileColor" content="#9a1b60">
	<meta name='msapplication-starturl' content='<?php echo get_home_url(); ?>'>

	<!-- PWA: APPLE LEGACY -->
	<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/assets/images/icon-rendered-512.png">
	<link rel="mask-icon" sizes="any" href="" color="#9a1b60">
	<meta name="apple-mobile-web-app-status-bar-style" content="#9a1b60">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?>">
	<meta name="apple-mobile-web-app-capable" content="yes">

	<!-- WORDPRESS: HEAD -->
	<?php wp_head(); ?>

</head>


<body <?php body_class(); ?>><div class="pf-c-page">


	<!-- WORDPRESS: BODY -->
	<?php wp_body_open(); ?>


	<!-- SKIP -->
  <a class="pf-c-skip-to-content pf-c-button pf-m-primary" href="#content"><?php _e( 'Skip to content', 'annobib-theme' ); ?></a>


	<?php if ( $annobib_modifier == 'full' ) : ?><!-- PAGE HEADER -->
  <header role="banner" class="pf-c-page__header" id="header" aria-labelledby="header-title">
		<h1 class="annobib-m-hidden" id="header-title" aria-hidden="true"><?php bloginfo( 'name' ); ?></h1>

		<!-- PAGE HEADER: BRAND -->
    <div class="pf-c-page__header-brand">
      <a class="pf-c-page__header-brand-link" href="<?php echo get_home_url(); ?>">
        <img class="pf-c-brand" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-wordmark.svg" alt="<?php _e( 'Lit4School logo', 'annobib-theme' ); ?>" />
      </a>
    </div>

		<!-- PAGE HEADER: NAVIGATION -->
		<div class="pf-c-page__header-nav">
	    <nav class="pf-c-nav pf-m-horizontal" aria-labelledby="header-navigation">
				<h2 class="annobib-m-hidden" id="header-navigation" aria-hidden="true"><?php _e( 'Main navigation', 'annobib-theme' ); ?></h2>
	      <ul class="pf-c-nav__list">
					<li class="pf-c-nav__item pf-m-hidden-on-xl">
						<?php if( $annobib_current == 'entry' || $annobib_current == 'post' || $annobib_current == 'search' ) : ?>
							<?php get_template_part( 'reusable', 'back' ); ?>
						<?php else : ?><button class="pf-c-button pf-m-plain annobib-h-nav" type="button" aria-label="<?php _e( 'Sub-navigation toggle', 'annobib-theme' ); ?>" aria-expanded="false" aria-controls="sidebar" data-target="#sidebar">
							<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__menu-open" /></svg>
						</button><?php endif; $annobib_blog = ''; ?>
					</li>
	        <li class="pf-c-nav__item">
	          <a href="<?php echo home_url( '/en' ); ?>" class="pf-c-nav__link<?php if( $annobib_section == 'en' ) { echo ' pf-m-current'; $annobib_blog = 'en-'; } ?>" aria-current="page"><?php _e( 'English', 'annobib-theme' ); ?></a>
	        </li>
	        <li class="pf-c-nav__item">
	          <a href="<?php echo home_url( '/de' ); ?>" class="pf-c-nav__link<?php if( $annobib_section == 'de' ) { echo ' pf-m-current'; $annobib_blog = 'de-'; } ?>"><?php _e( 'German', 'annobib-theme' ); ?></a>
	        </li>
	        <li class="pf-c-nav__item">
	          <a href="<?php echo home_url( '/' . $annobib_blog . 'blog' ); ?>" class="pf-c-nav__link<?php if( $annobib_section == 'blog' ) { echo ' pf-m-current'; } ?>"><?php _e( 'Blog', 'annobib-theme' ); ?></a>
	        </li>
	      </ul>
	    </nav>
			<?php if( ( $annobib_current == 'entry' || $annobib_current == 'post' ) && ! is_404() ) : ?><div class="annobib-c-page__header-nav-extra">
				<?php get_template_part( 'reusable', 'share' ); ?>
			</div><?php endif; ?>
	  </div>

		<!-- PAGE HEADER: TOOLS -->
    <section class="pf-c-page__header-tools" aria-labelledby="header-tools">
			<h2 class="annobib-m-hidden" id="header-tools" aria-hidden="true"><?php _e( 'Site tools', 'annobib-theme' ); ?></h2>

			<!-- PAGE HEADER: TOOLS: GROUP -->
			<div class="pf-c-page__header-tools-group">
				<div class="pf-c-page__header-tools-item">
					<div class="pf-c-dropdown">
						<button class="pf-c-dropdown__toggle pf-m-plain" type="button" id="header-about" aria-expanded="false">
							<span class="pf-c-dropdown__toggle-text"><?php _e( 'About', 'annobib-theme' ); ?></span>
							<span class="pf-c-dropdown__toggle-icon">
								<svg class="annobib-c-icon"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__small-opendown" /></svg>
							</span>
						</button>
						<ul class="pf-c-dropdown__menu pf-m-align-right" aria-labelledby="header-about" hidden>
							<li>
								<button class="pf-c-dropdown__menu-item pf-m-icon annobib-h-open pf-m-hidden-on-xl" type="button" data-target="#suggestion">
									<span class="pf-c-dropdown__menu-item-icon">
										<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__menu-suggestion" /></svg>
									</span><?php _e( 'Suggest an entry', 'annobib-theme' ); ?></button>
							</li>
							<li>
								<button class="pf-c-dropdown__menu-item pf-m-icon annobib-h-open pf-m-hidden-on-xl" type="button" data-target="#tutorial">
									<span class="pf-c-dropdown__menu-item-icon">
										<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__menu-tutorial" /></svg>
									</span><?php _e( 'Tutorial', 'annobib-theme' ); ?></button>
							</li>
							<li class="pf-c-divider pf-m-hidden-on-xl" role="separator"></li>
							<li>
								<a class="pf-c-dropdown__menu-item pf-m-icon" href="<?php echo home_url( '/' . $annobib_language . '-criteria' ); ?>">
									<span class="pf-c-dropdown__menu-item-icon">
										<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__menu-criteria" /></svg>
									</span><?php _e( 'Selection criteria', 'annobib-theme' ); ?></a>
							</li>
							<li>
								<a class="pf-c-dropdown__menu-item pf-m-icon" href="<?php echo home_url( '/' . $annobib_language . '-privacy' ); ?>">
									<span class="pf-c-dropdown__menu-item-icon">
										<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__menu-privacy" /></svg>
									</span><?php _e( 'Privacy policy', 'annobib-theme' ); ?></a>
							</li>
							<li>
								<a class="pf-c-dropdown__menu-item pf-m-icon" href="<?php echo home_url( '/' . $annobib_language . '-legal' ); ?>">
									<span class="pf-c-dropdown__menu-item-icon">
										<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__menu-legal" /></svg>
									</span><?php _e( 'Legal notice', 'annobib-theme' ); ?></a>
							</li>
							<li class="pf-c-divider" role="separator"></li>
							<li>
								<a class="pf-c-dropdown__menu-item pf-m-icon" href="https://twitter.com/lit4school/">
									<span class="pf-c-dropdown__menu-item-icon">
										<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__menu-twitter" /></svg>
									</span><?php _e( 'Lit4School on Twitter', 'annobib-theme' ); ?></a>
							</li>
							<li>
								<a class="pf-c-dropdown__menu-item pf-m-icon" href="https://www.instagram.com/lit4school/">
									<span class="pf-c-dropdown__menu-item-icon">
										<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__menu-instagram" /></svg>
									</span><?php _e( 'Lit4School on Instagram', 'annobib-theme' ); ?></a>
							</li>
							<li class="pf-c-divider" role="separator"></li>
							<li>
								<button class="pf-c-dropdown__menu-item pf-m-icon annobib-h-open" type="button" data-target="#about">
									<span class="pf-c-dropdown__menu-item-icon">
										<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__menu-about" /></svg>
									</span><?php _e( 'About Lit4School', 'annobib-theme' ); ?></button>
							</li>
						</ul>
					</div>
				</div>
				<div class="pf-c-page__header-tools-item pf-m-hidden pf-m-visible-on-xl">
					<button class="pf-c-button pf-m-plain annobib-h-open" type="button" data-target="#tutorial"><?php _e( 'Tutorial', 'annobib-theme' ); ?></button>
				</div>
			</div>

			<!-- PAGE HEADER: TOOLS: GROUP -->
      <div class="pf-c-page__header-tools-group pf-m-hidden pf-m-visible-on-xl">
				<div class="pf-c-page__header-tools-item">
					<button class="pf-c-button pf-m-tertiary annobib-h-open" type="button" data-target="#suggestion"><?php _e( 'Suggest an entry', 'annobib-theme' ); ?></button>
				</div>
      </div>

    </section>
  </header><?php endif; ?>
