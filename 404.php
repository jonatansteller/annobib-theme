<?php
// Annotated Bibliography Theme
// Error Page

// Set language according to browser setting
if ( substr( $_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2 ) == 'de' ) {
  annobib_localise( 'de_DE' );
  set_query_var( 'annobib_language', 'de' );
}
else {
  //annobib_localise( 'en_GB' );
  set_query_var( 'annobib_language', 'en' );
}
set_query_var( 'annobib_section', 'general' );
set_query_var( 'annobib_current', 'post' );


// Header
get_header();

// Main
set_query_var( 'annobib_modifier', 'main' );
get_template_part( 'reusable', 'error' );

// Footer
get_footer();

?>
