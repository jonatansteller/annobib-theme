<?php
// Annotated Bibliography Theme
// Bibliographic Entry: English

//annobib_localise( 'en_GB' );
set_query_var( 'annobib_section', 'en' );
set_query_var( 'annobib_language', 'en' );
set_query_var( 'annobib_current', 'entry' );


// Header
get_header();

// Main
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		get_template_part( 'reusable', 'entry' );
	}
}
else {
	set_query_var( 'annobib_modifier', 'main' );
	get_template_part( 'reusable', 'error' );
}

// Footer
get_footer();

?>
