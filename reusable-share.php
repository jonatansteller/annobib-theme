<?php
// Annotated Bibliography Theme
// Reusable Component: Share

$annobib_section  = get_query_var( 'annobib_section' );
$annobib_language = get_query_var( 'annobib_language' );
$annobib_current  = get_query_var( 'annobib_current' );
$annobib_modifier = get_query_var( 'annobib_modifier' );
if ( $annobib_modifier == 'blog' ) {
  $annobib_place1  = ' pf-m-top';
  $annobib_place2  = '';
}
else {
  $annobib_place1  = '';
  $annobib_place2  = ' pf-m-align-right';
}
?>

<div class="pf-c-dropdown<?php echo $annobib_place1; ?>">
  <button class="pf-c-dropdown__toggle pf-m-plain" type="button" id="content-share" aria-expanded="false" aria-label="Share">
    <svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__menu-share" /></svg>
  </button>
  <ul class="pf-c-dropdown__menu<?php echo $annobib_place2; ?>" aria-labelledby="content-share" hidden>
    <li>
      <button class="pf-c-dropdown__menu-item pf-m-icon annobib-h-copy" type="button" data-target="<?php echo get_permalink(); ?>">
        <span class="pf-c-dropdown__menu-item-icon">
          <svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__menu-copy" /></svg>
        </span><?php _e( 'Copy link address', 'annobib-theme' ); ?></button>
    </li>
    <li class="pf-c-divider" role="separator"></li>
    <li>
      <a class="pf-c-dropdown__menu-item pf-m-icon" href="mailto:?&subject=&body=<?php echo get_permalink(); ?>" target="_blank" rel="noreferrer noopener">
        <span class="pf-c-dropdown__menu-item-icon">
          <svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__menu-email" /></svg>
        </span><?php _e( 'Share by email', 'annobib-theme' ); ?></a>
    </li>
    <li>
      <a class="pf-c-dropdown__menu-item pf-m-icon" href="https://twitter.com/home?status=<?php echo get_permalink(); ?>" target="_blank" rel="noreferrer noopener">
        <span class="pf-c-dropdown__menu-item-icon">
          <svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__menu-twitter" /></svg>
        </span><?php _e( 'Share on Twitter', 'annobib-theme' ); ?></a>
    </li>
    <li>
      <a class="pf-c-dropdown__menu-item pf-m-icon" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>" target="_blank" rel="noreferrer noopener">
        <span class="pf-c-dropdown__menu-item-icon">
          <svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__menu-facebook" /></svg>
        </span><?php _e( 'Share on Facebook', 'annobib-theme' ); ?></a>
    </li>
    <?php if( ! is_singular() ) : ?><li class="pf-c-divider" role="separator"></li>
    <li>
      <a class="pf-c-dropdown__menu-item pf-m-icon" href="<?php echo get_permalink(); ?>">
        <span class="pf-c-dropdown__menu-item-icon">
          <svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__menu-read" /></svg>
        </span><?php _e( 'Read this entry', 'annobib-theme' ); ?></a>
    </li><?php endif; ?>
  </ul>
</div>
