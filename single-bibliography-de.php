<?php
// Annotated Bibliography Theme
// Bibliographic Entry: German

annobib_localise( 'de_DE' );
set_query_var( 'annobib_section', 'de' );
set_query_var( 'annobib_language', 'de' );
set_query_var( 'annobib_current', 'entry' );


// Header
get_header();

// Main
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		get_template_part( 'reusable', 'entry' );
	}
}
else {
	set_query_var( 'annobib_modifier', 'main' );
	get_template_part( 'reusable', 'error' );
}

// Footer
get_footer();

?>
