<?php
// Annotated Bibliography Theme
// Reusable Component: Home

$annobib_section		= get_query_var( 'annobib_section' );
$annobib_language		= get_query_var( 'annobib_language' );
$annobib_current		= get_query_var( 'annobib_current' );
$annobib_shortcuts	= wp_nav_menu( array(
  'container'				=> '',
  'echo'						=> false,
  'depth'						=> 1,
  'theme_location'	=> 'shortcuts-' . $annobib_section,
  'fallback_cb'     => false,
  'item_spacing'		=> 'discard' ));
$annobib_shortcuts	= strip_tags( $annobib_shortcuts, '<a>' );
$annobib_shortcuts	= str_replace( '<a href', '<a class="pf-c-button pf-m-plain" href', $annobib_shortcuts );
?>

	<!-- PAGE MAIN -->
  <main role="main" class="pf-c-page__main" tabindex="-1" id="content" aria-labelledby="content-title">

		<!-- PAGE MAIN: CONTENT -->
    <div class="pf-c-page__main-section pf-m-light">
      <div class="pf-c-content">
        <h1 id="content-title"><?php _e( 'Authentic literature & media for the EFL curriculum', 'annobib-theme' ); ?></h1>
      </div>
    </div>

		<!-- PAGE MAIN: CONTENT -->
		<section class="pf-c-page__main-section pf-m-light pf-m-no-padding" aria-labelledby="content-toolbar">
			<h2 class="annobib-m-hidden" id="content-toolbar" aria-hidden="true"><?php _e( 'Toolbar', 'annobib-theme' ); ?></h2>

			<!-- TOOLBAR -->
			<div class="pf-c-toolbar" id="toolbar">

				<!-- TOOLBAR (First row) -->
				<div class="pf-c-toolbar__content">
					<div class="pf-c-toolbar__content-section">

						<?php get_template_part( 'reusable', 'search' ); ?>

						<?php if ( $annobib_shortcuts ) : ?><!-- TOOLBAR: GROUP -->
						<div class="pf-c-toolbar__group annobib-m-shortcuts" id="toolbar-shortcuts">

							<!-- TOOLBAR: GROUP: SHORTCUTS -->
							<div class="pf-c-toolbar__item">
								<?php echo $annobib_shortcuts; ?>
							</div>

						</div><?php endif; ?>

					</div>
				</div>

			</div>

		</section>

		<!-- PAGE MAIN: CARDS -->
    <section class="pf-c-page__main-section" aria-labelledby="card-highlights-title">
      <div class="pf-l-gallery pf-m-gutter">

				<?php // Set up loop
        wp_reset_postdata();
        $loop = new WP_Query( array(
        	'post_type'				=> 'bibliography-' . $annobib_section,
        	'meta_key'				=> 'annobib-meta-featured',
        	'meta_value'			=> true,
        	'orderby'					=> 'rand',
        	'posts_per_page'	=> 5
        ));

        // Loop
        if ( $loop->have_posts() ) : ?>

          <!-- PAGE MAIN: CARDS: CARD -->
          <div class="pf-c-card pf-m-hoverable pf-m-selectable annobib-m-card-highlight" role="button" tabindex="0" id="card-highlights" data-target="<?php echo home_url( '/' . $annobib_section . '-highlights' ); ?>" aria-labelledby="card-highlights-title">
            <div class="pf-c-card__body">
              <h2 class="pf-c-title pf-m-xl" id="card-highlights-title"><?php _e( 'Some of our highlights', 'annobib-theme' ); ?></h2>
            </div>
          </div>

        	<?php while ( $loop->have_posts() ) {
        		$loop->the_post();
        		get_template_part( 'reusable', 'card' );
        	}
        else :
          get_template_part( 'reusable', 'soon' );
        endif;

        // Prepare next loop
        wp_reset_postdata(); ?>

			</div>

		</section>

		<!-- PAGE MAIN: CONTENT -->
		<div class="pf-c-page__main-section pf-m-light pf-m-no-fill annobib-l-columns annobib-m-wide">

			<!-- PAGE MAIN: SECTION: COLUMNS: ITEM -->
			<div class="annobib-l-columns__item">
				<h2 class="pf-c-title pf-m-lg pf-u-mb-md annobib-m-block-title"><?php _e( 'From our blog', 'annobib-theme' ); ?></h2>

        <?php // Set up loop
        if ( $annobib_section == 'de' ) {
          $loopCategory = 'german';
        }
        else {
          $loopCategory = 'english';
        }
        $loop = new WP_Query( array(
          'post_type'				=> 'post',
          'category_name'		=> $loopCategory,
          'posts_per_page'	=> 3
        ));

        // Loop
        if ( $loop->have_posts() ) {
          echo '<ul class="annobib-c-iconlist annobib-m-columns annobib-m-short">';
          while ( $loop->have_posts() ) {
            $loop->the_post();
            echo '<li><span class="annobib-c-iconlist__icon"><svg class="annobib-c-icon" aria-hidden="true"><use href="' . get_template_directory_uri() . '/assets/images/annobib-c-icon.svg#annobib-c-icon__entry-blog" /></svg></span><span class="annobib-c-iconlist__label"><a href="' . get_permalink() . '">' . the_title( '', '', false ) . '</a></span></li>';
          }
          echo '</ul>';
        }
        else {
          echo '<div class="pf-c-empty-state"><div class="pf-c-empty-state__content"><svg class="annobib-c-icon pf-c-empty-state__icon" aria-hidden="true"><use href="' . get_template_directory_uri() . '/assets/images/annobib-c-icon.svg#annobib-c-icon__navigation-empty" /></svg></div></div>';
        }

        // Prepare next loop
        wp_reset_postdata(); ?>

			</div>

			<!-- PAGE MAIN: SECTION: COLUMNS: ITEM -->
			<div class="annobib-l-columns__item">
				<h2 class="pf-c-title pf-m-lg pf-u-mb-md annobib-m-block-title"><?php _e( 'About this project', 'annobib-theme' ); ?></h2>
				<p class="annobib-m-block-title"><?php _e( 'Lit4School is a non-profit database of literature and media for the classroom. This OER is developed by the English group and edited by the ZLS\'s English and German groups at Leipzig University.', 'annobib-theme' ); ?></p>
			</div>

			<!-- PAGE MAIN: SECTION: COLUMNS: ITEM -->
			<div class="annobib-l-columns__item">
				<h2 class="pf-c-title pf-m-lg pf-u-mb-md annobib-m-block-title"><?php _e( 'With the support of', 'annobib-theme' ); ?></h2>
				<div class="annobib-m-block-image">
					<a href="https://www.zls.uni-leipzig.de" target="_blank" rel="noreferrer noopener"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-zls.svg" alt="<?php _e( 'Zentrum für Lehrerbildung und Schulforschung at Leipzig University', 'annobib-theme' ); ?>"></a>
				</div>
			</div>

		</div>

		<!-- PAGE MAIN: CARDS -->
    <section class="pf-c-page__main-section pf-m-fill" aria-labelledby="card-recent-title">
      <div class="pf-l-gallery pf-m-gutter">

        <?php // Set up loop
        $loop = new WP_Query( array(
          'post_type'				=> 'bibliography-' . $annobib_section,
          'posts_per_page'	=> 5
        ));

        // Loop
        if ( $loop->have_posts() ) : ?>

          <!-- PAGE MAIN: CARDS: CARD -->
  				<div class="pf-c-card pf-m-hoverable pf-m-selectable annobib-m-card-recent" role="button" tabindex="0" id="card-recent" data-target="<?php echo home_url( '/' . $annobib_section . '-recent' ); ?>" aria-labelledby="card-recent-title">
  					<div class="pf-c-card__body">
  						<h2 class="pf-c-title pf-m-xl" id="card-recent-title"><?php _e( 'Recently added', 'annobib-theme' ); ?></h2>
  					</div>
  				</div>

        	<?php while ( $loop->have_posts() ) {
        		$loop->the_post();
        		get_template_part( 'reusable', 'card' );
        	}
        else :
          get_template_part( 'reusable', 'soon' );
        endif;

        // Prepare next loop
        wp_reset_postdata(); ?>

			</div>

		</section>

	</main>
