/*
STRINGS

Place to translate strings of features which dynamically load elements.
*/

// Translatable strings
var stringClearAll = 'Clear all filters';
var stringClear = 'Remove';

// German translations
if ( document.documentElement.lang == 'de-DE' ) {
  stringClearAll = 'Alle Filter entfernen';
  stringClear = 'Entferne';
}


/*
FUNCTIONS AND LISTENERS FOR VARIOUS FEATURES

These features live inside a wrapper because they rely on the DOM being fully loaded.
*/

document.addEventListener( 'DOMContentLoaded', function() {


  /*
  DROPDOWNS

  Dropdowns open an element when they are clicked and close it when they are clicked again. Closing is implemented as a separate function to be able to trigger it elsewhere.
  */

  // Variables
  const dropdownSelectors = '.pf-c-dropdown__toggle, .pf-c-select__toggle, .pf-c-options-menu__toggle';
  const dropdownButtons = Array.prototype.slice.call( document.querySelectorAll( dropdownSelectors ), 0);

  // Toggle a dropdown
  function dropdownToggle( e ) {

    // If this dropdown is closed
    if( e.nextElementSibling.hidden === true ) {

      // Close all other dropdowns
      dropdownClose();

      // Open element
      e.setAttribute( 'aria-expanded', 'true' );
      e.nextElementSibling.hidden = false;

      // Set a listener to close the dropdown on any subsequent click
      setTimeout( function () {
        document.addEventListener( 'click', function () {
          if( ! event.target.closest( dropdownSelectors ) ) {
            dropdownClose();
          }
        }, { once: true } );
      }, 300);
    }

    // If the dropdown is open
    else {
      dropdownClose();
    }
  }

  // Close dropdowns
  function dropdownClose() {
    dropdownButtons.forEach( function ( e ) {
      e.setAttribute( 'aria-expanded', 'false' );
      e.nextElementSibling.hidden = true;
    });
  }

  // Listener
  dropdownButtons.forEach( function( e ) {
    e.addEventListener( 'click', function() {
      dropdownToggle( e );
    }, false );
  });


  /*
  MODALS

  Modal overlays can be triggered or closed. Some contents of the modal need to be reset when the overlay is closed.
  */

  // Variables
  const modalOverlays = Array.prototype.slice.call( document.querySelectorAll( '.pf-c-backdrop' ), 0);
  const modalTriggers = Array.prototype.slice.call( document.querySelectorAll( '.annobib-h-open' ), 0);
  const modalCloses = Array.prototype.slice.call( document.querySelectorAll( '.annobib-h-close' ), 0);
  const modalForms = Array.prototype.slice.call( document.querySelectorAll( '.annobib-h-form' ), 0);
  const modalVideos = Array.prototype.slice.call( document.querySelectorAll( '.annobib-h-video' ), 0);

  // Open modal
  function modalTrigger( e ) {

    // Add class to body element to prevent scrolling
    document.documentElement.classList.add( 'pf-c-backdrop__open' );

    // Identify and open target
    const target = e.dataset.target;
    const targets = document.querySelectorAll( target );
    targets.forEach( function( e ) {
      e.hidden = false;
      e.setAttribute( 'aria-hidden', 'false' );
    });
  }

  // Close modals
  function modalClose() {

    // Remove class from body element
    document.documentElement.classList.remove( 'pf-c-backdrop__open' );

    // Close all modal elements
    modalOverlays.forEach( function ( e ) {
      e.hidden = true;
      e.setAttribute( 'aria-hidden', 'true' );
    });

    // Reset froms and videos
    modalForms.forEach( function ( e ) {
      e.reset();
    });
    modalVideos.forEach( function ( e ) {
      e.pause();
      e.currentTime = 0;
    });
  }

  // Listeners
  modalTriggers.forEach( function( e ) {
    e.addEventListener( 'click', function() {
      modalTrigger( e );
    }, false );
  });
  modalCloses.forEach( function( e ) {
    e.addEventListener( 'click', function() {
      modalClose( e );
    }, false );
  });
  modalOverlays.forEach( function( e ) {
    e.addEventListener( 'click', function() {
      if( ! event.target.closest( '.pf-c-modal-box, .pf-c-about-modal-box' ) ) {
        modalClose( e );
      }
    }, false);
  });


  /*
  CARDS

  Cards are simple container elements that trigger a specified URL when triggered
  */

  // Variables
  const cardElements = Array.prototype.slice.call( document.querySelectorAll( '.pf-m-selectable' ), 0);

  // Make clicking a card work like a link
  function cardTrigger( e ) {

    // In case someone returned to a cards page: de-select all cards
    cardDeselect();

    // Select the card that is clicked
    e.classList.add( 'pf-m-selected' );
    const target = e.dataset.target;
    window.location.href = target;

    // In case someone returned to a cards page: de-select all cards in case someone clicks outside a card
    setTimeout( function () {
      document.addEventListener( 'click', function () {
        if( ! event.target.closest( '.pf-m-selected' ) ) {
          cardDeselect();
        }
      }, { once: true } );
    }, 300);
  }

  // De-select card
  function cardDeselect() {
    cardElements.forEach( function ( e ) {
      e.classList.remove( 'pf-m-selected' );
    });
  }

  // Listener
  cardElements.forEach( function( e ) {
    e.addEventListener( 'click', function() {
      if( ! event.target.closest( '.pf-c-dropdown') ) {
        cardTrigger( e );
      }
    }, false);
  });


  /*
  COPY BUTTONS

  Copy buttons copy text to the clipboard and show a notification.
  */

  // Variables
  const copyButtons = Array.prototype.slice.call( document.querySelectorAll( '.annobib-h-copy' ), 0);

  // Copy to clipboard when clicked
  function copyText( e ) {

    // Identify text to copy to the clipboard
    const target = e.dataset.target;

    // Copy and notify the user
    if ( 'clipboard' in navigator ) {
      navigator.clipboard.writeText( target ).then( function() {
        notificationTrigger( 'alert-clipboard-success' );
      }, function() {
        notificationTrigger( 'alert-clipboard-warning' );
      });
    }
    else {
      notificationTrigger( 'alert-clipboard-warning' );
    }
  }

  // Listener
  copyButtons.forEach( function( e ) {
    e.addEventListener( 'click', function() {
      copyText( e );
    }, false );
  });


  /*
  MENU ENTRIES

  Menu entries open a sub-menu. When they are clicked, all other sub-menus are closed.
  */

  // Variables
  const menuEntries = Array.prototype.slice.call( document.querySelectorAll( '.annobib-h-menu' ), 0);

  // Toggle sub-menu
  function menuToggle( e ) {

    // If the sub-menu is not yet shown
    if ( e.nextElementSibling.hidden === true ) {

      // Close all other menus
      menuClose();

      // Show sub-menu
      e.setAttribute( 'aria-expanded', 'true' );
      e.parentElement.classList.add( 'pf-m-expanded' );
      e.nextElementSibling.hidden = false;
    }

    // If the sub-menu is open already
    else {
      menuClose();
    }
  }

  // Close all sub-menus
  function menuClose() {
    menuEntries.forEach( function ( e ) {
      e.setAttribute( 'aria-expanded', 'false' );
      e.parentElement.classList.remove( 'pf-m-expanded' );
      e.nextElementSibling.hidden = true;
    });
  }

  // Listener
  menuEntries.forEach( function( e ) {
    e.addEventListener( 'click', function( event ) {
      event.preventDefault();
      menuToggle( e );
    }, false );
  });


  /*
  NOTIFICATIONS

  Notifications display and vanish for a brief amount of time.
  */

  // Show notification
  function notificationTrigger( alert ) {

    // Show the desired notification
    alert = document.getElementById( alert );
    if ( alert ) {
      alert.hidden = false;
      alert.setAttribute( 'aria-hidden', 'false' );

      // Only show it for four seconds
      setTimeout( function () {
        alert.hidden = true;
        alert.setAttribute( 'aria-hidden', 'true' );
      }, 4000);
    }
  }

  // Check for suggestion form notifications
  notificationTrigger( 'alert-suggestion-success' );
  notificationTrigger( 'alert-suggestion-warning' );


  /*
  FILTERS

  If a filter is clicked in the toolbar, it is added to a two-dimensional array, the clicked entry is disabled, the visible list of active filters is updated, and the card view is updated using the new set of filters. The reverse mechanism works for filter removals. The filter array has the following values:

  [0] Slug of the filter ('gymnasium-05-06')
  [1] Human-readable name of the filter ('Years 5-6 (Gymnasium)')
  [2] Slug of the filter group ('en-curriculum')
  [3] Human-readable name of the filter group ('School & year')
  [4] HTML ID of the filter button ('toolbar-filters-en-curriculum-gymnasium-05-06')
  [5] HTML ID of the clear label ('toolbar-filters-en-curriculum-gymnasium-05-06-clear-label')
  [6] HTML ID of the clear button ('toolbar-filters-en-curriculum-gymnasium-05-06-clear-button')
  */

  // Variables
  const filterLinks = Array.prototype.slice.call( document.querySelectorAll( '.annobib-h-filter' ), 0);
  const filterSection = document.getElementById( 'toolbar-chip' );
  const filterContainer = document.getElementById( 'toolbar-chipcontainer' );

  // Array (existing or new)
  var filters = [];
  const filtersExistingElement = document.getElementById( 'toolbar-filterform-active' );
  if ( filtersExistingElement ) {
    var filtersExisting = filtersExistingElement.innerHTML;
    if ( filtersExisting != '' ) {
      filtersExisting = filtersExisting.replace( /\\/g, '' );
      try {
        filters = JSON.parse( filtersExisting );
        filterView();
      }
      catch {
      }
    }
  }

  // Add filter
  function filterAdd( newFilterElement ) {
    const newFilter = [];

    // Collect data points
    const newFilterSlug = newFilterElement.dataset.target; // [0]
    var newFilterName = newFilterElement.textContent; // [1]
    const newFilterGroup = newFilterElement.closest( '.pf-c-select__menu' ).previousElementSibling; // Prerequisite for [2] and [3]
    const newFilterGroupSlug = newFilterGroup.dataset.target; // [2]
    const newFilterGroupName = newFilterGroup.getElementsByClassName( 'pf-c-select__toggle-text' )[0].textContent; // [3]
    const newFilterID = 'toolbar-filters-' + newFilterGroupSlug + '-' + newFilterSlug; // [4]
    const newFilterClearLabelID = newFilterID + '-clear-label'; // [5]
    const newFilterClearButtonID = newFilterID + '-clear-button'; // [6]

    // Optional addition to [1] if it has a parent
    const parentTest = newFilterElement.closest( 'ul li ul' );
    if ( parentTest ) {
      newFilterName = newFilterName + ' (' + parentTest.previousElementSibling.textContent + ')';
    }

    // Add data to the newFilter and then add it to the filters array
    newFilter.push( newFilterSlug, newFilterName, newFilterGroupSlug, newFilterGroupName, newFilterID, newFilterClearLabelID, newFilterClearButtonID );
    filters.push( newFilter );

    // Update the list of active filters and send the form
    filterView();
    filterForm();
  }

  // Re-enable filter entry
  function filterReenable( element ) {
    const reenabledElement = document.getElementById( element );

    // Remove checkmark icon
    const removedIcon = Array.prototype.slice.call( reenabledElement.querySelectorAll( '.annobib-h-filter-checkmark' ), 0);
    removedIcon.forEach( function( i ) {
      i.remove();
    });

    // Enable the filter button and remove the ID
    reenabledElement.setAttribute( 'aria-selected', 'false' );
    reenabledElement.setAttribute( 'aria-disabled', 'false' );
    reenabledElement.classList.remove( 'pf-m-selected' );
    reenabledElement.disabled = false;
    reenabledElement.id = '';
  }

  // Remove filters (one or all)
  function filterRemove( oldFilterElement ) {

    // Remove all filters if no specific filter is indicated
    if ( ! oldFilterElement ) {

      // Enable the original filter buttons
      filters.forEach( function( f ) {
        filterReenable( f[4] );
      });

      // Clear the array
      filters = [];
    }
    else {
      // Identify the array row in variable i
      var i = -1;
      for ( const filtersRow of filters ) {
        i++;
        if ( filtersRow.indexOf( oldFilterElement ) > -1 ) {
          break;
        }
      }

      // Remove the row
      let oldFilter = filters[i];
      filters.splice( i, 1 );

      // Enable the original filter button
      filterReenable( oldFilter[4] );
    }

    // Update the list of active filters and send the form
    filterView();
    filterForm();
  }

  // Update filter view
  function filterView() {

    // If there are filters
    if ( filters.length ) {

      // Deactivate the respective filter buttons
      filters.forEach( function( f ) {
        var filterButton = document.getElementById( f[4] );
        filterButton.disabled = true;
        filterButton.classList.add( 'pf-m-selected' );
        filterButton.setAttribute( 'aria-disabled', 'true' );
        filterButton.setAttribute( 'aria-selected', 'true' );

        // Add a checkmark icon to the disabled element
        const filterButtonCheckmark = document.createElement( 'span' );
        filterButtonCheckmark.classList.add( 'pf-c-select__menu-item-icon', 'annobib-h-filter-checkmark' );
        filterButton.appendChild( filterButtonCheckmark );
        const filterButtonCheckmarkElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
        filterButtonCheckmarkElement.classList.add( 'annobib-c-icon' );
        filterButtonCheckmarkElement.setAttribute( 'aria-hidden', 'true' );
        filterButtonCheckmark.appendChild( filterButtonCheckmarkElement );
        const filterButtonCheckmarkElementIcon = document.createElementNS('http://www.w3.org/2000/svg', 'use' );
        filterButtonCheckmarkElementIcon.setAttribute( 'href', 'https://home.uni-leipzig.de/lit4school/wp-content/themes/annobib-theme/assets/images/annobib-c-icon.svg#annobib-c-icon__small-checked' );
        filterButtonCheckmarkElement.appendChild( filterButtonCheckmarkElementIcon );
      });

      // Show and clear the filter container element
      filterSection.hidden = false;
      filterSection.setAttribute( 'aria-hidden', 'false' );
      filterSection.classList.remove( 'annobib-m-hidden' );
      filterContainer.innerHTML = '';

      // Add a wrapper for all chip groups
      const filterContainerGroups = document.createElement( 'div' );
      filterContainerGroups.classList.add( 'pf-c-toolbar__group' );
      filterContainer.appendChild( filterContainerGroups );

      // Cycle through all filters to form groups and display their information
      const filterGroups = [];
      filters.forEach( function( f ) {
        if ( ! filterGroups.includes( f[2] ) ) {
          filterGroups.push( f[2] );

          // Construct chip group
          const groupElement = document.createElement( 'div' );
          groupElement.classList.add( 'pf-c-toolbar__item', 'pf-m-chip-group' );
          filterContainerGroups.appendChild( groupElement );
          const groupElementChipgroup = document.createElement( 'div' );
          groupElementChipgroup.classList.add( 'pf-c-chip-group', 'pf-m-category' );
          groupElement.appendChild( groupElementChipgroup );

          // Add chip group title
          const groupElementChipgroupTitle = document.createElement( 'span' );
          groupElementChipgroupTitle.classList.add( 'pf-c-chip-group__label' );
          groupElementChipgroupTitle.setAttribute( 'aria-hidden', 'true' );
          groupElementChipgroupTitle.id = 'toolbar-chip-group-' + f[2];
          groupElementChipgroup.appendChild( groupElementChipgroupTitle );
          const groupElementChipgroupTitleText = document.createTextNode( f[3] );
          groupElementChipgroupTitle.appendChild( groupElementChipgroupTitleText );

          // Add chip list
          const groupElementChipgroupList = document.createElement( 'ul' );
          groupElementChipgroupList.classList.add( 'pf-c-chip-group__list' );
          groupElementChipgroupList.setAttribute( 'role', 'list' );
          groupElementChipgroupList.setAttribute( 'aria-labelledby', 'toolbar-chip-group-' + f[2] );
          groupElementChipgroup.appendChild( groupElementChipgroupList );

          // In each group, cycle through all filters again to display respective filters
          filters.forEach( function( ff ) {
            if ( ff[2] == f[2] ) {

              // Construct chip wrapper
              const groupElementChipgroupListItem = document.createElement( 'li' );
              groupElementChipgroupListItem.classList.add( 'pf-c-chip-group__list-item' );
              groupElementChipgroupList.appendChild( groupElementChipgroupListItem );
              const chipElement = document.createElement( 'div' );
              chipElement.classList.add( 'pf-c-chip' );
              groupElementChipgroupListItem.appendChild( chipElement );

              // Add chip label
              const chipElementLabel = document.createElement( 'span' );
              chipElementLabel.classList.add( 'pf-c-chip__text' );
              chipElementLabel.id = ff[5];
              chipElement.appendChild( chipElementLabel );
              const chipElementLabelText = document.createTextNode( ff[1] );
              chipElementLabel.appendChild( chipElementLabelText );

              // Add chip button
              const chipElementButton = document.createElement( 'button' );
              chipElementButton.classList.add( 'pf-c-button', 'pf-m-plain' );
              chipElementButton.id = ff[6];
              chipElementButton.setAttribute( 'type', 'button' );
              chipElementButton.setAttribute( 'aria-label', stringClear );
              chipElementButton.setAttribute( 'aria-labelledby', ff[6] + ' ' + ff[5] );
              chipElement.appendChild( chipElementButton );
              const chipElementButtonClose = document.createElementNS('http://www.w3.org/2000/svg', 'svg' );
              chipElementButtonClose.classList.add( 'annobib-c-icon' );
              chipElementButtonClose.setAttribute( 'aria-hidden', 'true' );
              chipElementButton.appendChild( chipElementButtonClose );
              const chipElementButtonCloseIcon = document.createElementNS('http://www.w3.org/2000/svg', 'use' );
              chipElementButtonCloseIcon.setAttribute( 'href', 'https://home.uni-leipzig.de/lit4school/wp-content/themes/annobib-theme/assets/images/annobib-c-icon.svg#annobib-c-icon__small-close' );
              chipElementButtonClose.appendChild( chipElementButtonCloseIcon );

              // Listener for the clear button
              chipElementButton.addEventListener( 'click', function() {
                filterRemove( ff[0] );
              }, false );
            }
          });
        }
      });

      // Add general clear button
      const clearElement = document.createElement( 'div' );
      clearElement.classList.add( 'pf-c-toolbar__item' );
      filterContainer.appendChild( clearElement );
      const clearElementButton = document.createElement( 'button' );
      clearElementButton.classList.add( 'pf-c-button', 'pf-m-link', 'pf-m-inline' );
      clearElement.appendChild( clearElementButton );
      const clearElementButtonText = document.createTextNode( stringClearAll );
      clearElementButton.appendChild( clearElementButtonText );

      // Listener for the clear button
      clearElementButton.addEventListener( 'click', function() {
        filterRemove();
      }, false );
    }

    // If there are no filters
    else {

      // Hide and clear the container element
      filterSection.hidden = true;
      filterSection.setAttribute( 'aria-hidden', 'true' );
      filterSection.classList.add( 'annobib-m-hidden' );
      filterContainer.innerHTML = '';
    }
  }

  // Stringify the filter array in the invisible filter form, submit for use in PHP
  function filterForm() {
    if ( filters.length ) {
      const filtersJSON = JSON.stringify( filters );
      document.getElementById( 'toolbar-filterform-filters' ).value = filtersJSON;
      document.getElementById( 'toolbar-filterform' ).submit();
    }
    else {
      window.location.href = window.location.href;
    }
  }

  // Listener
  filterLinks.forEach( function( e ) {
    e.addEventListener( 'click', function() {
      filterAdd( e );
    }, false );
  });


  /*
  SELECTION

  Simple dropdown with one selected option at a time.
  */

  // Variables
  const selectionLinks = Array.prototype.slice.call( document.querySelectorAll( '.annobib-h-selection' ), 0);

  // Switch the selected entry
  function selectionSwitch( selectionElement ) {

    // Enable all elements in the same component
    selectionLinks.forEach( function ( e ) {
      if ( e.parentElement.parentElement == selectionElement.parentElement.parentElement ) {
        e.setAttribute( 'aria-pressed', 'false' );
        e.setAttribute( 'aria-disabled', 'false' );
        e.disabled = false;

        // Remove checkmarks
        const selectionCheckmarks = Array.prototype.slice.call( e.querySelectorAll( '.annobib-h-selection-checkmark' ), 0);
        selectionCheckmarks.forEach( function( i ) {
          i.remove();
        });
      }
    });

    // Disable the new element
    selectionElement.disabled = true;
    selectionElement.setAttribute( 'aria-disabled', 'true' );
    selectionElement.setAttribute( 'aria-pressed', 'true' );

    // Add a new checkmark
    const selectionElementCheckmark = document.createElement( 'span' );
    selectionElementCheckmark.classList.add( 'pf-c-options-menu__menu-item-icon', 'annobib-h-selection-checkmark' );
    selectionElement.appendChild( selectionElementCheckmark );
    const selectionElementCheckmarkElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg' );
    selectionElementCheckmarkElement.classList.add( 'annobib-c-icon' );
    selectionElementCheckmarkElement.setAttribute( 'aria-hidden', 'true' );
    selectionElementCheckmark.appendChild( selectionElementCheckmarkElement );
    const selectionElementCheckmarkElementIcon = document.createElementNS('http://www.w3.org/2000/svg', 'use' );
    selectionElementCheckmarkElementIcon.setAttribute( 'href', 'https://home.uni-leipzig.de/lit4school/wp-content/themes/annobib-theme/assets/images/annobib-c-icon.svg#annobib-c-icon__small-checked' );
    selectionElementCheckmarkElement.appendChild( selectionElementCheckmarkElementIcon );

    // Retrieve the target and actionate it
    selectionAction( selectionElement.dataset.target );
  }

  // Start the desired action
  function selectionAction( action ) {
    if (action == 'sort-added') {
    }
    else if (action == 'sort-publication') {
    }
    else if (action == 'sort-author') {
    }
    else if (action == 'page-1') {
    }
    else if (action == 'page-2') {
    }
    else if (action == 'page-3') {
    }
    else if (action == 'page-4') {
    }
    else if (action == 'page-5') {
    }
    else if (action == 'page-6') {
    }
  }

  // Listener
  selectionLinks.forEach( function( e ) {
    e.addEventListener( 'click', function() {
      selectionSwitch( e );
    }, false );
  });


  /*
  BACK

  The back button returns users to the previous page.
  */

  // Variables
  const backButtons = Array.prototype.slice.call( document.querySelectorAll( '.annobib-h-back' ), 0);

  // Return to previous page
  function backGo() {
    window.history.back();
  }

  // Listener
  backButtons.forEach( function( e ) {
    e.addEventListener( 'click', function() {
      backGo();
    }, false );
  });


  /*
  NAVIGATION

  The sub-navigation button toggles the sidebar on smaller screens. An on-demand listener closes the sidebar when users subsequently click outside of it. On larger screens, the sidebar is always shown.
  */

  // Variables
  const navEntries = Array.prototype.slice.call( document.querySelectorAll( '.annobib-h-nav' ), 0);

  // Toggle sidebar
  function navToggle( navElement ) {

    // Retrieve sidebar ID
    const target = navElement.dataset.target;
    const navSidebar = Array.prototype.slice.call( document.querySelectorAll( target ), 0);

    // Open all sidebar elements
    navSidebar.forEach ( function( e ) {

      // Show the sidebar if it is closed
      if ( ! e.classList.contains( 'pf-m-expanded' ) ) {
        e.classList.add( 'pf-m-expanded' );

        // Change the toggle button to a 'close' symbol
        navElement.parentElement.classList.add( 'pf-m-expanded' );
        navElement.setAttribute( 'aria-expanded', 'true' );
        navElement.firstElementChild.firstElementChild.setAttribute( 'href', 'https://home.uni-leipzig.de/lit4school/wp-content/themes/annobib-theme/assets/images/annobib-c-icon.svg#annobib-c-icon__menu-close' );

        // Close the sidebar on any subsequent click
        navSubsequent( navElement, navSidebar, target );
      }

      // Hide the sidebar if is open
      else {
        navClose( navElement, navSidebar );
      }
    });
  }

  // On-demand listener for subsequent clicks
  function navSubsequent( navElement, navSidebar, target ) {
    setTimeout( function () {
      document.addEventListener( 'click', function () {
        if( ! event.target.closest( target ) ) {
          navClose( navElement, navSidebar );
        }
        else {
          navSubsequent( navElement, navSidebar, target );
        }
      }, { once: true } );
    }, 300);
  }

  // Close sidebar
  function navClose( navElement, navSidebar ) {

    // Close all sidebar elements
    navSidebar.forEach ( function( e ) {
      e.classList.remove( 'pf-m-expanded' );
    });

    // Reset the toggle button
    navElement.parentElement.classList.remove( 'pf-m-expanded' );
    navElement.setAttribute( 'aria-expanded', 'false' );
    if ( navElement.dataset.target == '#toolbar-filters' ) {
      navElement.firstElementChild.firstElementChild.setAttribute( 'href', 'https://home.uni-leipzig.de/lit4school/wp-content/themes/annobib-theme/assets/images/annobib-c-icon.svg#annobib-c-icon__menu-filter' );
    }
    else if ( navElement.dataset.target == '#toolbar-search' ) {
      navElement.firstElementChild.firstElementChild.setAttribute( 'href', 'https://home.uni-leipzig.de/lit4school/wp-content/themes/annobib-theme/assets/images/annobib-c-icon.svg#annobib-c-icon__navigation-search' );
    }
    else {
      navElement.firstElementChild.firstElementChild.setAttribute( 'href', 'https://home.uni-leipzig.de/lit4school/wp-content/themes/annobib-theme/assets/images/annobib-c-icon.svg#annobib-c-icon__menu-open' );
    }
  }

  // Listener
  navEntries.forEach( function( e ) {
    e.addEventListener( 'click', function() {
      navToggle( e );
    }, false );
  });


  /*
  TABS

  Clicking on a tab opens the desired panel and closes its siblings.
  */

  // Variables
  const tabButtons = Array.prototype.slice.call( document.querySelectorAll( '.annobib-h-tab' ), 0);

  // Open tab panel
  function tabOpen( tabButton ) {

    // Unhighlight all neighbouring tabs and highlight the selected one
    tabButtons.forEach( function( e ) {
      if ( e.parentElement.parentElement == tabButton.parentElement.parentElement ) {
        e.parentElement.classList.remove( 'pf-m-current' );
      }
    });
    tabButton.parentElement.classList.add( 'pf-m-current' );

    // Identify the desired panel
    const tabPanels = Array.prototype.slice.call( document.querySelectorAll( '.pf-c-tab-content' ), 0);
    const target = tabButton.dataset.target;
    const tabPanel = Array.prototype.slice.call( document.querySelectorAll( target ), 0);

    // Close all neighbouring panels
    tabPanels.forEach( function( e ) {
      if ( e.parentElement == tabPanel[0].parentElement ) {
        e.hidden = true;
      }
    });

    // Open the selected panel
    tabPanel.forEach( function( e ) {
      e.hidden = false;
    });
  }

  // Listener
  tabButtons.forEach( function( e ) {
    e.addEventListener( 'click', function() {
      tabOpen( e );
    }, false );
  });


  /*
  FORM FIELDS

  Clicking on a form field button opens the desired panel and closes its siblings.
  */

  // Variables
  const formfieldButtons = Array.prototype.slice.call( document.querySelectorAll( '.annobib-h-formfield' ), 0);

  // Open tab panel
  function formfieldOpen( formfieldButton ) {

    // Did the user click on an open form field?
    formfieldActive = false;
    if ( formfieldButton.parentElement.parentElement.parentElement.classList.contains( 'pf-m-expanded' ) ) {
      formfieldActive = true;
    }

    // Close all neighbouring form fields
    formfieldButtons.forEach( function( e ) {
      if ( e.parentElement.parentElement.parentElement.parentElement == formfieldButton.parentElement.parentElement.parentElement.parentElement ) {
        e.parentElement.parentElement.parentElement.classList.remove( 'pf-m-expanded' );
        e.setAttribute( 'aria-expanded', 'false' );
        e.parentElement.parentElement.nextElementSibling.nextElementSibling.setAttribute( 'hidden', 'true' );
      }
    });

    // Open the selected form field if it was closed
    if ( ! formfieldActive ) {
      formfieldButton.parentElement.parentElement.parentElement.classList.add( 'pf-m-expanded' );
      formfieldButton.setAttribute( 'aria-expanded', 'true' );
      formfieldButton.parentElement.parentElement.nextElementSibling.nextElementSibling.setAttribute( 'hidden', 'false' );
    }
  }

  // Listener
  formfieldButtons.forEach( function( e ) {
    e.addEventListener( 'click', function() {
      formfieldOpen( e );
    }, false );
  });


  /*
  ESCAPE KEY

  Overlays and sub-menus should be hidden when a user presses the escape key.
  */

  // Listen for escape key presses
  document.addEventListener( 'keydown', () => {
    const e = event || window.event;
    if ( e.keyCode === 27 ) {
      dropdownClose();
      modalClose();
    }
  });

});
