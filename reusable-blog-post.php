<?php
// Annotated Bibliography Theme
// Reusable Component: Blog Post

$annobib_section		= get_query_var( 'annobib_section' );
$annobib_language		= get_query_var( 'annobib_language' );
$annobib_current		= get_query_var( 'annobib_current' );
$annobib_date				= get_the_date();
$annobib_categories	= get_the_category_list( ', ' );
$annobib_tags				= get_the_terms( get_the_ID(), 'post_tag' );
?>


		<article>

			<!-- PAGE MAIN: SECTION -->
			<div class="pf-c-page__main-section pf-m-light">

				<?php if ( ! is_singular() ) : ?><!-- PAGE MAIN: SECTION: CONTENT -->
				<div class="pf-u-mt-sm pf-c-content">
					<?php the_title( '<h2 class="pf-c-title pf-m-3xl annobib-m-block-title"><a href="' .  get_permalink() . '">', '</a></h2>', true ); ?>
					<p class="annobib-m-block-meta">
						<?php if ( $annobib_categories ) {
							echo $annobib_categories . ' · ';
						}
						echo $annobib_date; ?>
					</p>
				</div>

				<?php else : ?><!-- PAGE MAIN: SECTION: SPLIT -->
				<div class="pf-l-split pf-m-gutter pf-u-mt-sm">

					<!-- PAGE MAIN: SECTION: SPLIT: ITEM -->
					<div class="pf-l-split__item pf-m-visible-on-xl">
						<?php set_query_var( 'annobib_modifier', 'norm' );
						get_template_part( 'reusable', 'back' ); ?>
					</div>

					<!-- PAGE MAIN: SECTION: SPLIT: FILL ITEM -->
					<div class="pf-l-split__item pf-m-fill pf-c-content">
						<?php the_title( '<h1 class="pf-c-title pf-m-3xl annobib-m-block-title" id="content-title">', '</h1>', true ); ?>
						<p class="annobib-m-block-meta">
							<?php if ( $annobib_categories ) {
								echo $annobib_categories . ' · ';
							}
							echo $annobib_date; ?>
						</p>
					</div>

					<!-- PAGE MAIN: SECTION: SPLIT: ITEM -->
					<div class="pf-l-split__item pf-m-visible-on-xl">
						<?php get_template_part( 'reusable', 'share' ); ?>
					</div>

				</div>


				<?php endif; ?>

				<!-- PAGE MAIN: SECTION: BULLSEYE -->
				<div class="pf-l-bullseye annobib-m-mono">

					<!-- PAGE MAIN: SECTION: BULLSEYE: CONTENT -->
					<div class="pf-l-bullseye__item">
						<div class="pf-c-content annobib-m-block-paragraph">
							<?php the_content(); ?>
						</div>
						<div class="pf-l-flex pf-m-nowrap pf-m-align-items-flex-start pf-u-mt-lg">

							<?php if ( ! is_singular() ) : ?><div class="pf-l-flex__item">
								<?php set_query_var( 'annobib_modifier', 'blog' );
								get_template_part( 'reusable', 'share' ); ?>
							</div><?php endif; ?>

							<?php if ( $annobib_tags ) : ?><div class="pf-l-flex__item pf-m-align-right">
								<div class="pf-c-label-group annobib-m-inline">
									<ul class="pf-c-label-group__list" role="list">
										<?php foreach( $annobib_tags as $annobib_tag ) : ?><li class="pf-c-label-group__list-item">
											<span class="pf-c-label pf-m-outline">
												<a class="pf-c-label__content" href="<?php echo get_tag_link( $annobib_tag->term_id ); ?>"><?php echo $annobib_tag->name ?></a>
											</span>
										</li><?php endforeach; ?>
									</ul>
								</div>
							</div><?php endif; ?>

						</div>
					</div>

				</div>

			</div>

		</article>

		<?php if ( ! is_singular() ) : ?><hr class="pf-c-divider" /><?php endif; ?>
