<?php /* Template Name: List Date Range (1989 to 1999, German) */

// Annotated Bibliography Theme
// Entry list: date 1989 to 1999 (German)

annobib_localise( 'de_DE' );
set_query_var( 'annobib_section', 'de' );
set_query_var( 'annobib_language', 'de' );
set_query_var( 'annobib_current', 'date-1989-de' );


// Header
get_header();

// Sidebar
get_sidebar( 'bibliography' );

// List before the main loop 1
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		get_template_part( 'reusable', 'list-before1' );
	}
}

// Reset loop, add filters for the next loop (if set)
wp_reset_postdata();
add_action( 'pre_get_posts', 'annobib_filter' );

// Set up the specialised loop
$paged = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
$loop = new WP_Query( array(
	'post_type'		=> 'bibliography-de',
	'meta_key'		=> 'annobib-meta-year',
	'meta_query'	=> array(
    array(
      'key'			=> 'annobib-meta-year',
      'value'		=> array( 1989, 1999 ),
      'compare'	=> 'BETWEEN',
      'type'		=> 'NUMERIC'
    )),
 	'paged'				=> $paged
));

// Set up pagination
$tempQuery = $wp_query;
$wp_query  = NULL;
$wp_query  = $loop;

// List before the main loop 2
get_template_part( 'reusable', 'pagination' );
get_template_part( 'reusable', 'list-before2' );

// Main
if ( $loop->have_posts() ) {
	while ( $loop->have_posts() ) {
		$loop->the_post();
		set_query_var( 'annobib_section', 'de' );
		set_query_var( 'annobib_language', 'de' );
		set_query_var( 'annobib_current', 'date-1989-de' );
		get_template_part( 'reusable', 'card' );
	}
}
else {
	get_template_part( 'reusable', 'empty' );
}

// List after the loop
get_template_part( 'reusable', 'list-after1' );
set_query_var( 'annobib_modifier', 'small-openup' );
get_template_part( 'reusable', 'pagination' );
get_template_part( 'reusable', 'list-after2' );

// Reset leep and pagination
$wp_query = NULL;
$wp_query = $tempQuery;
wp_reset_postdata();

// Footer
get_footer();

?>
