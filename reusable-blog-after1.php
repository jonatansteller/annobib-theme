<?php
// Annotated Bibliography Theme
// Reusable Component: Blog After the Loop 1

$annobib_section			= get_query_var( 'annobib_section' );
$annobib_language			= get_query_var( 'annobib_language' );
$annobib_current			= get_query_var( 'annobib_current' );
?>


		<!-- PAGE MAIN: BOTTOM PAGINATION -->
		<nav class="pf-c-pagination pf-m-bottom pf-m-static" aria-hidden="true">
			<h3 class="annobib-m-hidden" id="toolbar-pagination"><?php _e( 'Pagination', 'annobib-theme' ); ?></h3>
			<div class="pf-c-pagination__nav" aria-label="Pagination">
