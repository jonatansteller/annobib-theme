<?php
// Annotated Bibliography Theme
// Footer

$annobib_section	= get_query_var( 'annobib_section' );
$annobib_language	= get_query_var( 'annobib_language' );
$annobib_current	= get_query_var( 'annobib_current' );
$annobib_modifier	= get_query_var( 'annobib_modifier' );
if ( $annobib_modifier != 'plain' ) {
	$annobib_modifier = 'full';
}

// Routine for when the suggestion form has been submitted (English)
if ( isset( $_POST['suggestion-en-submitted'] ) && $_POST['suggestion-en-check'] == '' ) {

	// Get required strings
	$suggestionContentName	= sanitize_text_field( trim( $_POST['suggestion-en-name'] ) );
	$suggestionContentEmail	= sanitize_email( trim( $_POST['suggestion-en-email'] ) );
	$suggestionContentEntry	= sanitize_text_field( trim( $_POST['suggestion-en-entry'] ) );

	// Continue if all the required field were filled
	if ( $suggestionContentName != '' && $suggestionContentEmail != '' && $suggestionContentEntry != '' ) {

		// Collate email strings
		$suggestionTo				= 'Lit4School <lit4school@uni-leipzig.de>';
		$suggestionSubject	= __( 'New entry suggested (English)', 'annobib-theme' );
		$suggestionHeader		= array( sprintf ( 'From: %s <%s>', $suggestionContentName, $suggestionContentEmail ) );
		$suggestionContentA	= __( 'A user suggested a new entry for the English section of Lit4School. Please find the details below.', 'annobib-theme' ) . "\r\n\r\n--\r\n\r\n";
		$suggestionContentB	= '# ' . __( 'Basic information', 'annobib-theme' ) . "\r\n\r\n" . __( 'Your name', 'annobib-theme' ) . ': ' . $suggestionContentName . "\r\n" . __( 'Your email', 'annobib-theme' ) . ': ' . $suggestionContentEmail . "\r\n" . __( 'Entry title', 'annobib-theme' ) . ': ' . $suggestionContentEntry . "\r\n\r\n";
		$suggestionContentC	= '# ' . __( 'Metadata (optional)', 'annobib-theme' ) . "\r\n\r\n" . __( 'Author(s)', 'annobib-theme' ) . ': ' . sanitize_text_field( trim( $_POST['suggestion-en-writer'] ) ) . "\r\n" . __( 'Year of publication', 'annobib-theme' ) . ': ' . sanitize_text_field( trim( $_POST['suggestion-en-year'] ) ) . "\r\n" . __( 'Country of publication', 'annobib-theme' ) . ': ' . sanitize_text_field( trim( $_POST['suggestion-en-country'] ) ) . "\r\n" . __( 'Genre', 'annobib-theme' ) . ': ' . implode( '; ', $_POST['suggestion-en-genre'] ) . "\r\n\r\n";
		$suggestionContentD	= '# ' . __( 'Selection criteria (optional)', 'annobib-theme' ) . "\r\n\r\n" . __( 'Content summary', 'annobib-theme' ) . ': ' . sanitize_textarea_field( trim( $_POST['suggestion-en-summary'] ) ) . "\r\n" . __( 'Arguments in favour', 'annobib-theme' ) . ': ' . implode( '; ', $_POST['suggestion-en-criterion'] ) . "\r\n\r\n";
		$suggestionContentE	= '# ' . __( 'Curricular data (optional)', 'annobib-theme' ) . "\r\n\r\n" . __( 'School years', 'annobib-theme' ) . ': ' . implode( '; ', $_POST['suggestion-en-curriculum'] ) . "\r\n" . __( 'Topics', 'annobib-theme' ) . ': ' . implode( '; ', $_POST['suggestion-en-topic'] ) . "\r\n\r\n";
		$suggestionContentF	= '# ' . __( 'Advanced notes (optional)', 'annobib-theme' ) . "\r\n\r\n" . __( 'Classroom edition', 'annobib-theme' ) . ': ' . sanitize_text_field( trim( $_POST['suggestion-en-edition'] ) ) . "\r\n" . __( 'Useful links', 'annobib-theme' ) . ': ' . sanitize_text_field( trim( $_POST['suggestion-en-links'] ) ) . "\r\n" . __( 'Adaptation genres', 'annobib-theme' ) . ': ' . implode( '; ', $_POST['suggestion-en-adaptation'] ) . "\r\n" . __( 'Notes for the editors', 'annobib-theme' ) . ': ' . sanitize_textarea_field( trim( $_POST['suggestion-en-notes'] ) );
		$suggestionContent = $suggestionContentA . $suggestionContentB . $suggestionContentC . $suggestionContentD . $suggestionContentE . $suggestionContentF;

		// Send email
		$suggestion = wp_mail ( $suggestionTo, $suggestionSubject, $suggestionContent, $suggestionHeader );
		if ( $suggestion == true ) {
			$suggestionSuccess = true;
		}
		else {
			$suggestionSuccess = false;
		}
	}

	// Throw an error if required fields were left blank
	else {
		$suggestionSuccess = false;
	}
}

// Routine for when the suggestion form has been submitted (German)
elseif ( isset( $_POST['suggestion-de-submitted'] ) && $_POST['suggestion-de-check'] == '' ) {

	// Get required strings
	$suggestionContentName	= sanitize_text_field( trim( $_POST['suggestion-de-name'] ) );
	$suggestionContentEmail	= sanitize_email( trim( $_POST['suggestion-de-email'] ) );
	$suggestionContentEntry	= sanitize_text_field( trim( $_POST['suggestion-de-entry'] ) );

	// Continue if all the required field were filled
	if ( $suggestionContentName != '' && $suggestionContentEmail != '' && $suggestionContentEntry != '' ) {

		// Collate email strings
		$suggestionTo				= 'Lit4School <lit4school@uni-leipzig.de>';
		$suggestionSubject	= __( 'New entry suggested (German)', 'annobib-theme' );
		$suggestionHeader		= array( sprintf ( 'From: %s <%s>', $suggestionContentName, $suggestionContentEmail ) );
		$suggestionContentA	= __( 'A user suggested a new entry for the German section of Lit4School. Please find the details below.', 'annobib-theme' ) . "\r\n\r\n--\r\n\r\n";
		$suggestionContentB	= '# ' . __( 'Basic information', 'annobib-theme' ) . "\r\n\r\n" . __( 'Your name', 'annobib-theme' ) . ': ' . $suggestionContentName . "\r\n" . __( 'Your email', 'annobib-theme' ) . ': ' . $suggestionContentEmail . "\r\n" . __( 'Entry title', 'annobib-theme' ) . ': ' . $suggestionContentEntry . "\r\n\r\n";
		$suggestionContentC	= '# ' . __( 'Metadata (optional)', 'annobib-theme' ) . "\r\n\r\n" . __( 'Author(s)', 'annobib-theme' ) . ': ' . sanitize_text_field( trim( $_POST['suggestion-de-writer'] ) ) . "\r\n" . __( 'Year of publication', 'annobib-theme' ) . ': ' . sanitize_text_field( trim( $_POST['suggestion-de-year'] ) ) . "\r\n" . __( 'Genre', 'annobib-theme' ) . ': ' . implode( '; ', $_POST['suggestion-de-genre'] ) . "\r\n\r\n";
		$suggestionContentD	= '# ' . __( 'Selection criteria (optional)', 'annobib-theme' ) . "\r\n\r\n" . __( 'Content summary', 'annobib-theme' ) . ': ' . sanitize_textarea_field( trim( $_POST['suggestion-de-summary'] ) ) . "\r\n" . __( 'Arguments in favour', 'annobib-theme' ) . ': ' . implode( '; ', $_POST['suggestion-de-criterion'] ) . "\r\n\r\n";
		$suggestionContentE	= '# ' . __( 'Curricular data (optional)', 'annobib-theme' ) . "\r\n\r\n" . __( 'School years', 'annobib-theme' ) . ': ' . implode( '; ', $_POST['suggestion-de-curriculum'] ) . "\r\n" . __( 'Topics', 'annobib-theme' ) . ': ' . implode( '; ', $_POST['suggestion-de-topic[]'] ) . "\r\n" . __( 'Contexts', 'annobib-theme' ) . ': ' . implode( '; ', $_POST['suggestion-de-context'] ) . "\r\n\r\n";
		$suggestionContentF	= '# ' . __( 'Advanced notes (optional)', 'annobib-theme' ) . "\r\n\r\n" . __( 'Classroom edition', 'annobib-theme' ) . ': ' . sanitize_text_field( trim( $_POST['suggestion-de-edition'] ) ) . "\r\n" . __( 'Useful links', 'annobib-theme' ) . ': ' . sanitize_text_field( trim( $_POST['suggestion-de-links'] ) ) . "\r\n" . __( 'Adaptation genres', 'annobib-theme' ) . ': ' . implode( '; ', $_POST['suggestion-de-adaptation'] ) . "\r\n" . __( 'Notes for the editors', 'annobib-theme' ) . ': ' . sanitize_textarea_field( trim( $_POST['suggestion-de-notes'] ) );
		$suggestionContent = $suggestionContentA . $suggestionContentB . $suggestionContentC . $suggestionContentD . $suggestionContentE . $suggestionContentF;

		// Send email
		$suggestion = wp_mail ( $suggestionTo, $suggestionSubject, $suggestionContent, $suggestionHeader );
		if ( $suggestion == true ) {
			$suggestionSuccess = true;
		}
		else {
			$suggestionSuccess = false;
		}
	}

	// Throw an error if required fields were left blank
	else {
		$suggestionSuccess = false;
	}
}?>


	<?php if ( $annobib_modifier == 'full' ) : ?><!-- MODAL (Suggestion) -->
	<aside class="pf-c-backdrop" id="suggestion" aria-hidden="true" hidden role="dialog" aria-modal="true" aria-labelledby="suggestion-title" aria-describedby="suggestion-description">
	  <div class="pf-l-bullseye">
	    <div class="pf-c-modal-box pf-m-sm">
	      <button class="pf-c-button pf-m-plain annobib-h-close" type="button" aria-label="<?php _e( 'Close dialog', 'annobib-theme' ); ?>">
	        <svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__menu-close" /></svg>
	      </button>
			  <header class="pf-c-modal-box__header">
			    <h1 class="pf-c-modal-box__title" id="suggestion-title" aria-hidden="true"><?php _e( 'Suggest a new entry', 'annobib-theme' ); ?></h1>
			  	<div class="pf-c-modal-box__description" id="suggestion-description"><?php _e( 'We will get back to you when we have checked your suggestion.', 'annobib-theme' ); ?></div>
			  </header>
	      <div class="pf-c-modal-box__body">

					<!-- MODAL: TABS -->
					<nav class="pf-c-tabs pf-m-fill" aria-label="Select tab">
						<ul class="pf-c-tabs__list">
							<li class="pf-c-tabs__item pf-m-current">
								<button class="pf-c-tabs__link annobib-h-tab" id="suggestion-tabs-en" aria-controls="suggestion-en" data-target="#suggestion-en">
									<span class="pf-c-tabs__item-text"><?php _e( 'English Lessons', 'annobib-theme' ); ?></span>
								</button>
							</li>
							<li class="pf-c-tabs__item">
								<button class="pf-c-tabs__link annobib-h-tab" id="suggestion-tabs-de" aria-controls="suggestion-de" data-target="#suggestion-de">
									<span class="pf-c-tabs__item-text"><?php _e( 'German Lessons', 'annobib-theme' ); ?></span>
								</button>
							</li>
						</ul>
					</nav>

					<!-- MODAL: TAB PANEL -->
					<section class="pf-c-tab-content annobib-m-dense" id="suggestion-en" role="tabpanel" tabindex="0" aria-labelledby="suggestion-tabs-en">
						<h2 class="annobib-m-hidden"><?php _e( 'English entry form', 'annobib-theme' ); ?></h2>
						<form action="<?php global $wp; echo home_url( $wp->request ); ?>" method="post" class="pf-c-form pf-m-horizontal annobib-h-form" id="suggestion-en-form">

							<!-- MODAL: TAB PANEL: FIELD GROUP -->
							<div class="pf-c-form__field-group pf-m-expanded">
								<div class="pf-c-form__field-group-toggle">
									<div class="pf-c-form__field-group-toggle-button">
										<button class="pf-c-button pf-m-plain annobib-h-formfield" type="button" aria-expanded="true" aria-label="<?php _e( 'Details', 'annobib-theme' ); ?>" id="suggestion-en1-toggle" aria-labelledby="suggestion-en1-title suggestion-en1-toggle">
											<span class="pf-c-form__field-group-toggle-icon">
												<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__small-openright" /></svg>
											</span>
										</button>
									</div>
								</div>
								<div class="pf-c-form__field-group-header">
									<div class="pf-c-form__field-group-header-main">
										<div class="pf-c-form__field-group-header-title">
											<div class="pf-c-form__field-group-header-title-text" id="suggestion-en1-title"><?php _e( 'Basic information', 'annobib-theme' ); ?></div>
										</div>
									</div>
								</div>
								<div class="pf-c-form__field-group-body">
									<div class="pf-c-form__group">
										<div class="pf-c-form__group-label">
								    	<label class="pf-c-form__label" for="suggestion-en-name">
								      	<span class="pf-c-form__label-text"><?php _e( 'Your name', 'annobib-theme' ); ?></span>
								      	<span class="pf-c-form__label-required" aria-hidden="true">&#42;</span>
								    	</label>
										</div>
								    <div class="pf-c-form__group-control">
								      <input class="pf-c-form-control" required type="text" id="suggestion-en-name" name="suggestion-en-name" />
								    </div>
								  </div>
								  <div class="pf-c-form__group">
										<div class="pf-c-form__group-label">
								    	<label class="pf-c-form__label" for="suggestion-en-email">
								      	<span class="pf-c-form__label-text"><?php _e( 'Your email', 'annobib-theme' ); ?></span>
								      	<span class="pf-c-form__label-required" aria-hidden="true">&#42;</span>
								    	</label>
										</div>
								    <div class="pf-c-form__group-control">
								      <input class="pf-c-form-control" required type="email" id="suggestion-en-email" name="suggestion-en-email" />
								    </div>
								  </div>
								  <div class="pf-c-form__group">
										<div class="pf-c-form__group-label">
								    	<label class="pf-c-form__label" for="suggestion-en-entry">
								      	<span class="pf-c-form__label-text"><?php _e( 'Entry title', 'annobib-theme' ); ?></span>
								      	<span class="pf-c-form__label-required" aria-hidden="true">&#42;</span>
								    	</label>
										</div>
								    <div class="pf-c-form__group-control">
								      <input class="pf-c-form-control" required type="text" id="suggestion-en-entry" name="suggestion-en-entry" />
								    </div>
								  </div>
								</div>
							</div>

							<!-- MODAL: TAB PANEL: FIELD GROUP -->
							<div class="pf-c-form__field-group">
								<div class="pf-c-form__field-group-toggle">
									<div class="pf-c-form__field-group-toggle-button">
										<button class="pf-c-button pf-m-plain annobib-h-formfield" type="button" aria-expanded="false" aria-label="<?php _e( 'Details', 'annobib-theme' ); ?>" id="suggestion-en2-toggle" aria-labelledby="suggestion-en2-title suggestion-en2-toggle">
											<span class="pf-c-form__field-group-toggle-icon">
												<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__small-openright" /></svg>
											</span>
										</button>
									</div>
								</div>
								<div class="pf-c-form__field-group-header">
									<div class="pf-c-form__field-group-header-main">
										<div class="pf-c-form__field-group-header-title">
											<div class="pf-c-form__field-group-header-title-text" id="suggestion-en2-title"><?php _e( 'Metadata (optional)', 'annobib-theme' ); ?></div>
										</div>
									</div>
								</div>
								<div class="pf-c-form__field-group-body" hidden>
									<div class="pf-c-form__group">
										<div class="pf-c-form__group-label">
								    	<label class="pf-c-form__label" for="suggestion-en-writer">
								      	<span class="pf-c-form__label-text"><?php _e( 'Author(s)', 'annobib-theme' ); ?></span>
								    	</label>
										</div>
								    <div class="pf-c-form__group-control">
								      <input class="pf-c-form-control" type="text" id="suggestion-en-writer" name="suggestion-en-writer" />
								    </div>
								  </div>
									<div class="pf-c-form__group">
										<div class="pf-c-form__group-label">
								    	<label class="pf-c-form__label" for="suggestion-en-year">
								      	<span class="pf-c-form__label-text"><?php _e( 'Year of publication', 'annobib-theme' ); ?></span>
								    	</label>
										</div>
								    <div class="pf-c-form__group-control">
								      <input class="pf-c-form-control" type="text" id="suggestion-en-year" name="suggestion-en-year" />
								    </div>
								  </div>
									<div class="pf-c-form__group">
										<div class="pf-c-form__group-label">
								    	<label class="pf-c-form__label" for="suggestion-en-country">
								      	<span class="pf-c-form__label-text"><?php _e( 'Country of publication', 'annobib-theme' ); ?></span>
								    	</label>
										</div>
								    <div class="pf-c-form__group-control">
								      <input class="pf-c-form-control" type="text" id="suggestion-en-country" name="suggestion-en-country" />
								    </div>
								  </div>
									<div class="pf-c-form__group">
										<div class="pf-c-form__group-label">
								    	<label class="pf-c-form__label" for="suggestion-en-genre[]">
								      	<span class="pf-c-form__label-text"><?php _e( 'Genre', 'annobib-theme' ); ?></span>
								    	</label>
										</div>
								    <div class="pf-c-form__group-control">
								      <select class="pf-c-form-control annobib-m-inputbox" id="suggestion-en-genre[]" name="suggestion-en-genre[]" multiple>
												<?php annobib_select( 'en-genre' ); ?>
								      </select>
								    </div>
								  </div>
								</div>
							</div>

							<!-- MODAL: TAB PANEL: FIELD GROUP -->
							<div class="pf-c-form__field-group">
								<div class="pf-c-form__field-group-toggle">
									<div class="pf-c-form__field-group-toggle-button">
										<button class="pf-c-button pf-m-plain annobib-h-formfield" type="button" aria-expanded="false" aria-label="<?php _e( 'Details', 'annobib-theme' ); ?>" id="suggestion-en3-toggle" aria-labelledby="suggestion-en3-title suggestion-en3-toggle">
											<span class="pf-c-form__field-group-toggle-icon">
												<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__small-openright" /></svg>
											</span>
										</button>
									</div>
								</div>
								<div class="pf-c-form__field-group-header">
									<div class="pf-c-form__field-group-header-main">
										<div class="pf-c-form__field-group-header-title">
											<div class="pf-c-form__field-group-header-title-text" id="suggestion-en3-title"><?php _e( 'Selection criteria (optional)', 'annobib-theme' ); ?></div>
										</div>
									</div>
								</div>
								<div class="pf-c-form__field-group-body" hidden>
									<div class="pf-c-form__group">
										<div class="pf-c-form__group-label">
											<label class="pf-c-form__label" for="suggestion-en-summary">
												<span class="pf-c-form__label-text"><?php _e( 'Content summary', 'annobib-theme' ); ?></span>
											</label>
										</div>
										<div class="pf-c-form__group-control">
											<textarea class="pf-c-form-control annobib-m-inputbox" name="suggestion-en-summary" id="suggestion-en-summary"></textarea>
										</div>
									</div>
									<div class="pf-c-form__group">
										<div class="pf-c-form__group-label">
								    	<label class="pf-c-form__label" for="suggestion-en-criterion[]">
								      	<span class="pf-c-form__label-text"><?php _e( 'Arguments in favour', 'annobib-theme' ); ?></span>
								    	</label>
										</div>
								    <div class="pf-c-form__group-control">
								      <select class="pf-c-form-control annobib-m-inputbox" id="suggestion-en-criterion[]" name="suggestion-en-criterion[]" multiple>
												<?php annobib_select( 'en-criterion' ); ?>
								      </select>
								    </div>
								  </div>
								</div>
							</div>

							<!-- MODAL: TAB PANEL: FIELD GROUP -->
							<div class="pf-c-form__field-group">
								<div class="pf-c-form__field-group-toggle">
									<div class="pf-c-form__field-group-toggle-button">
										<button class="pf-c-button pf-m-plain annobib-h-formfield" type="button" aria-expanded="false" aria-label="<?php _e( 'Details', 'annobib-theme' ); ?>" id="suggestion-en4-toggle" aria-labelledby="suggestion-en4-title suggestion-en4-toggle">
											<span class="pf-c-form__field-group-toggle-icon">
												<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__small-openright" /></svg>
											</span>
										</button>
									</div>
								</div>
								<div class="pf-c-form__field-group-header">
									<div class="pf-c-form__field-group-header-main">
										<div class="pf-c-form__field-group-header-title">
											<div class="pf-c-form__field-group-header-title-text" id="suggestion-en4-title"><?php _e( 'Curricular data (optional)', 'annobib-theme' ); ?></div>
										</div>
									</div>
								</div>
								<div class="pf-c-form__field-group-body" hidden>
									<div class="pf-c-form__group">
										<div class="pf-c-form__group-label">
								    	<label class="pf-c-form__label" for="suggestion-en-curriculum[]">
								      	<span class="pf-c-form__label-text"><?php _e( 'School years', 'annobib-theme' ); ?></span>
								    	</label>
										</div>
								    <div class="pf-c-form__group-control">
								      <select class="pf-c-form-control annobib-m-inputbox" id="suggestion-en-curriculum[]" name="suggestion-en-curriculum[]" multiple>
												<?php annobib_select( 'en-curriculum' ); ?>
								      </select>
								    </div>
								  </div>
									<div class="pf-c-form__group">
										<div class="pf-c-form__group-label">
								    	<label class="pf-c-form__label" for="suggestion-en-topic[]">
								      	<span class="pf-c-form__label-text"><?php _e( 'Topics', 'annobib-theme' ); ?></span>
								    	</label>
										</div>
								    <div class="pf-c-form__group-control">
								      <select class="pf-c-form-control annobib-m-inputbox" id="suggestion-en-topic[]" name="suggestion-en-topic[]" multiple>
												<?php annobib_select( 'en-topic' ); ?>
								      </select>
								    </div>
								  </div>
								</div>
							</div>

							<!-- MODAL: TAB PANEL: FIELD GROUP -->
							<div class="pf-c-form__field-group">
								<div class="pf-c-form__field-group-toggle">
									<div class="pf-c-form__field-group-toggle-button">
										<button class="pf-c-button pf-m-plain annobib-h-formfield" type="button" aria-expanded="false" aria-label="<?php _e( 'Details', 'annobib-theme' ); ?>" id="suggestion-en5-toggle" aria-labelledby="suggestion-en5-title suggestion-en5-toggle">
											<span class="pf-c-form__field-group-toggle-icon">
												<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__small-openright" /></svg>
											</span>
										</button>
									</div>
								</div>
								<div class="pf-c-form__field-group-header">
									<div class="pf-c-form__field-group-header-main">
										<div class="pf-c-form__field-group-header-title">
											<div class="pf-c-form__field-group-header-title-text" id="suggestion-en5-title"><?php _e( 'Advanced notes (optional)', 'annobib-theme' ); ?></div>
										</div>
									</div>
								</div>
								<div class="pf-c-form__field-group-body" hidden>
									<div class="pf-c-form__group">
										<div class="pf-c-form__group-label">
											<label class="pf-c-form__label" for="suggestion-en-edition">
												<span class="pf-c-form__label-text"><?php _e( 'Classroom edition', 'annobib-theme' ); ?></span>
											</label>
										</div>
										<div class="pf-c-form__group-control">
											<input class="pf-c-form-control" type="text" id="suggestion-en-edition" name="suggestion-en-edition" />
										</div>
									</div>
									<div class="pf-c-form__group">
										<div class="pf-c-form__group-label">
											<label class="pf-c-form__label" for="suggestion-en-links">
												<span class="pf-c-form__label-text"><?php _e( 'Useful links', 'annobib-theme' ); ?></span>
											</label>
										</div>
										<div class="pf-c-form__group-control">
											<input class="pf-c-form-control" type="text" id="suggestion-en-links" name="suggestion-en-links" />
										</div>
									</div>
									<div class="pf-c-form__group">
										<div class="pf-c-form__group-label">
								    	<label class="pf-c-form__label" for="suggestion-en-adaptation[]">
								      	<span class="pf-c-form__label-text"><?php _e( 'Adaptation genres', 'annobib-theme' ); ?></span>
								    	</label>
										</div>
								    <div class="pf-c-form__group-control">
								      <select class="pf-c-form-control annobib-m-inputbox" id="suggestion-en-adaptation[]" name="suggestion-en-adaptation[]" multiple>
												<?php annobib_select( 'en-adaptation' ); ?>
								      </select>
								    </div>
								  </div>
									<div class="pf-c-form__group">
										<div class="pf-c-form__group-label">
											<label class="pf-c-form__label" for="suggestion-en-notes">
												<span class="pf-c-form__label-text"><?php _e( 'Notes for the editors', 'annobib-theme' ); ?></span>
											</label>
										</div>
										<div class="pf-c-form__group-control">
											<textarea class="pf-c-form-control annobib-m-inputbox" name="suggestion-en-notes" id="suggestion-en-notes"></textarea>
										</div>
									</div>
								</div>
							</div><!-- Not included: length, content rating, identifier -->

							<input class="annobib-m-hidden" type="text" id="suggestion-en-check" name="suggestion-en-check" aria-hidden="true" />
							<input type="hidden" name="suggestion-en-submitted" id="suggestion-en-submitted" value="true" />

							<div class="pf-c-form__group pf-m-action">
								<div class="pf-c-form__actions">
									<button class="pf-c-button pf-m-primary" type="submit" form="suggestion-en-form"><?php _e( 'Send entry', 'annobib-theme' ); ?></button>
						      <button class="pf-c-button pf-m-link annobib-h-close" type="button"><?php _e( 'Cancel', 'annobib-theme' ); ?></button>
								</div>
							</div>
						</form>
					</section>

					<!-- MODAL: TAB PANEL -->
					<section class="pf-c-tab-content annobib-m-dense" id="suggestion-de" role="tabpanel" tabindex="0" aria-labelledby="suggestion-tabs-de" hidden>
						<h2 class="annobib-m-hidden"><?php _e( 'German entry form', 'annobib-theme' ); ?></h2>
						<form action="<?php global $wp; echo home_url( $wp->request ); ?>" method="post" class="pf-c-form pf-m-horizontal annobib-h-form" id="suggestion-de-form">

							<!-- MODAL: TAB PANEL: FIELD GROUP -->
							<div class="pf-c-form__field-group pf-m-expanded">
								<div class="pf-c-form__field-group-toggle">
									<div class="pf-c-form__field-group-toggle-button">
										<button class="pf-c-button pf-m-plain annobib-h-formfield" type="button" aria-expanded="true" aria-label="<?php _e( 'Details', 'annobib-theme' ); ?>" id="suggestion-de1-toggle" aria-labelledby="suggestion-de1-title suggestion-de1-toggle">
											<span class="pf-c-form__field-group-toggle-icon">
												<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__small-openright" /></svg>
											</span>
										</button>
									</div>
								</div>
								<div class="pf-c-form__field-group-header">
									<div class="pf-c-form__field-group-header-main">
										<div class="pf-c-form__field-group-header-title">
											<div class="pf-c-form__field-group-header-title-text" id="suggestion-de1-title"><?php _e( 'Basic information', 'annobib-theme' ); ?></div>
										</div>
									</div>
								</div>
								<div class="pf-c-form__field-group-body">
									<div class="pf-c-form__group">
										<div class="pf-c-form__group-label">
								    	<label class="pf-c-form__label" for="suggestion-de-name">
								      	<span class="pf-c-form__label-text"><?php _e( 'Your name', 'annobib-theme' ); ?></span>
								      	<span class="pf-c-form__label-required" aria-hidden="true">&#42;</span>
								    	</label>
										</div>
								    <div class="pf-c-form__group-control">
								      <input class="pf-c-form-control" required type="text" id="suggestion-de-name" name="suggestion-de-name" />
								    </div>
								  </div>
								  <div class="pf-c-form__group">
										<div class="pf-c-form__group-label">
								    	<label class="pf-c-form__label" for="suggestion-de-email">
								      	<span class="pf-c-form__label-text"><?php _e( 'Your email', 'annobib-theme' ); ?></span>
								      	<span class="pf-c-form__label-required" aria-hidden="true">&#42;</span>
								    	</label>
										</div>
								    <div class="pf-c-form__group-control">
								      <input class="pf-c-form-control" required type="email" id="suggestion-de-email" name="suggestion-de-email" />
								    </div>
								  </div>
								  <div class="pf-c-form__group">
										<div class="pf-c-form__group-label">
								    	<label class="pf-c-form__label" for="suggestion-de-entry">
								      	<span class="pf-c-form__label-text"><?php _e( 'Entry title', 'annobib-theme' ); ?></span>
								      	<span class="pf-c-form__label-required" aria-hidden="true">&#42;</span>
								    	</label>
										</div>
								    <div class="pf-c-form__group-control">
								      <input class="pf-c-form-control" required type="text" id="suggestion-de-entry" name="suggestion-de-entry" />
								    </div>
								  </div>
								</div>
							</div>

							<!-- MODAL: TAB PANEL: FIELD GROUP -->
							<div class="pf-c-form__field-group">
								<div class="pf-c-form__field-group-toggle">
									<div class="pf-c-form__field-group-toggle-button">
										<button class="pf-c-button pf-m-plain annobib-h-formfield" type="button" aria-expanded="false" aria-label="<?php _e( 'Details', 'annobib-theme' ); ?>" id="suggestion-de2-toggle" aria-labelledby="suggestion-de2-title suggestion-de2-toggle">
											<span class="pf-c-form__field-group-toggle-icon">
												<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__small-openright" /></svg>
											</span>
										</button>
									</div>
								</div>
								<div class="pf-c-form__field-group-header">
									<div class="pf-c-form__field-group-header-main">
										<div class="pf-c-form__field-group-header-title">
											<div class="pf-c-form__field-group-header-title-text" id="suggestion-de2-title"><?php _e( 'Metadata (optional)', 'annobib-theme' ); ?></div>
										</div>
									</div>
								</div>
								<div class="pf-c-form__field-group-body" hidden>
									<div class="pf-c-form__group">
										<div class="pf-c-form__group-label">
								    	<label class="pf-c-form__label" for="suggestion-de-writer">
								      	<span class="pf-c-form__label-text"><?php _e( 'Author(s)', 'annobib-theme' ); ?></span>
								    	</label>
										</div>
								    <div class="pf-c-form__group-control">
								      <input class="pf-c-form-control" type="text" id="suggestion-de-writer" name="suggestion-de-writer" />
								    </div>
								  </div>
									<div class="pf-c-form__group">
										<div class="pf-c-form__group-label">
								    	<label class="pf-c-form__label" for="suggestion-de-year">
								      	<span class="pf-c-form__label-text"><?php _e( 'Year of publication', 'annobib-theme' ); ?></span>
								    	</label>
										</div>
								    <div class="pf-c-form__group-control">
								      <input class="pf-c-form-control" type="text" id="suggestion-de-year" name="suggestion-de-year" />
								    </div>
								  </div>
									<div class="pf-c-form__group">
										<div class="pf-c-form__group-label">
								    	<label class="pf-c-form__label" for="suggestion-de-genre[]">
								      	<span class="pf-c-form__label-text"><?php _e( 'Genre', 'annobib-theme' ); ?></span>
								    	</label>
										</div>
								    <div class="pf-c-form__group-control">
								      <select class="pf-c-form-control annobib-m-inputbox" id="suggestion-de-genre[]" name="suggestion-de-genre[]" multiple>
												<?php annobib_select( 'de-genre' ); ?>
								      </select>
								    </div>
								  </div>
								</div>
							</div>

							<!-- MODAL: TAB PANEL: FIELD GROUP -->
							<div class="pf-c-form__field-group">
								<div class="pf-c-form__field-group-toggle">
									<div class="pf-c-form__field-group-toggle-button">
										<button class="pf-c-button pf-m-plain annobib-h-formfield" type="button" aria-expanded="false" aria-label="<?php _e( 'Details', 'annobib-theme' ); ?>" id="suggestion-de3-toggle" aria-labelledby="suggestion-de3-title suggestion-de3-toggle">
											<span class="pf-c-form__field-group-toggle-icon">
												<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__small-openright" /></svg>
											</span>
										</button>
									</div>
								</div>
								<div class="pf-c-form__field-group-header">
									<div class="pf-c-form__field-group-header-main">
										<div class="pf-c-form__field-group-header-title">
											<div class="pf-c-form__field-group-header-title-text" id="suggestion-de3-title"><?php _e( 'Selection criteria (optional)', 'annobib-theme' ); ?></div>
										</div>
									</div>
								</div>
								<div class="pf-c-form__field-group-body" hidden>
									<div class="pf-c-form__group">
										<div class="pf-c-form__group-label">
											<label class="pf-c-form__label" for="suggestion-de-summary">
												<span class="pf-c-form__label-text"><?php _e( 'Content summary', 'annobib-theme' ); ?></span>
											</label>
										</div>
										<div class="pf-c-form__group-control">
											<textarea class="pf-c-form-control annobib-m-inputbox" name="suggestion-de-summary" id="suggestion-de-summary"></textarea>
										</div>
									</div>
									<div class="pf-c-form__group">
										<div class="pf-c-form__group-label">
								    	<label class="pf-c-form__label" for="suggestion-de-criterion[]">
								      	<span class="pf-c-form__label-text"><?php _e( 'Arguments in favour', 'annobib-theme' ); ?></span>
								    	</label>
										</div>
								    <div class="pf-c-form__group-control">
								      <select class="pf-c-form-control annobib-m-inputbox" id="suggestion-de-criterion[]" name="suggestion-de-criterion[]" multiple>
												<?php annobib_select( 'de-criterion' ); ?>
								      </select>
								    </div>
								  </div>
								</div>
							</div>

							<!-- MODAL: TAB PANEL: FIELD GROUP -->
							<div class="pf-c-form__field-group">
								<div class="pf-c-form__field-group-toggle">
									<div class="pf-c-form__field-group-toggle-button">
										<button class="pf-c-button pf-m-plain annobib-h-formfield" type="button" aria-expanded="false" aria-label="<?php _e( 'Details', 'annobib-theme' ); ?>" id="suggestion-de4-toggle" aria-labelledby="suggestion-de4-title suggestion-de4-toggle">
											<span class="pf-c-form__field-group-toggle-icon">
												<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__small-openright" /></svg>
											</span>
										</button>
									</div>
								</div>
								<div class="pf-c-form__field-group-header">
									<div class="pf-c-form__field-group-header-main">
										<div class="pf-c-form__field-group-header-title">
											<div class="pf-c-form__field-group-header-title-text" id="suggestion-de4-title"><?php _e( 'Curricular data (optional)', 'annobib-theme' ); ?></div>
										</div>
									</div>
								</div>
								<div class="pf-c-form__field-group-body" hidden>
									<div class="pf-c-form__group">
										<div class="pf-c-form__group-label">
								    	<label class="pf-c-form__label" for="suggestion-de-curriculum[]">
								      	<span class="pf-c-form__label-text"><?php _e( 'School years', 'annobib-theme' ); ?></span>
								    	</label>
										</div>
								    <div class="pf-c-form__group-control">
								      <select class="pf-c-form-control annobib-m-inputbox" id="suggestion-de-curriculum[]" name="suggestion-de-curriculum[]" multiple>
												<?php annobib_select( 'de-curriculum' ); ?>
								      </select>
								    </div>
								  </div>
									<div class="pf-c-form__group">
										<div class="pf-c-form__group-label">
								    	<label class="pf-c-form__label" for="suggestion-de-topic[]">
								      	<span class="pf-c-form__label-text"><?php _e( 'Topics', 'annobib-theme' ); ?></span>
								    	</label>
										</div>
								    <div class="pf-c-form__group-control">
								      <select class="pf-c-form-control annobib-m-inputbox" id="suggestion-de-topic[]" name="suggestion-de-topic[]" multiple>
												<?php annobib_select( 'de-topic' ); ?>
								      </select>
								    </div>
								  </div>
									<div class="pf-c-form__group">
										<div class="pf-c-form__group-label">
								    	<label class="pf-c-form__label" for="suggestion-de-context[]">
								      	<span class="pf-c-form__label-text"><?php _e( 'Contexts', 'annobib-theme' ); ?></span>
								    	</label>
										</div>
								    <div class="pf-c-form__group-control">
								      <select class="pf-c-form-control annobib-m-inputbox" id="suggestion-de-context[]" name="suggestion-de-context[]" multiple>
												<?php annobib_select( 'de-context' ); ?>
								      </select>
								    </div>
								  </div>
								</div>
							</div>

							<!-- MODAL: TAB PANEL: FIELD GROUP -->
							<div class="pf-c-form__field-group">
								<div class="pf-c-form__field-group-toggle">
									<div class="pf-c-form__field-group-toggle-button">
										<button class="pf-c-button pf-m-plain annobib-h-formfield" type="button" aria-expanded="false" aria-label="<?php _e( 'Details', 'annobib-theme' ); ?>" id="suggestion-de5-toggle" aria-labelledby="suggestion-de5-title suggestion-de5-toggle">
											<span class="pf-c-form__field-group-toggle-icon">
												<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__small-openright" /></svg>
											</span>
										</button>
									</div>
								</div>
								<div class="pf-c-form__field-group-header">
									<div class="pf-c-form__field-group-header-main">
										<div class="pf-c-form__field-group-header-title">
											<div class="pf-c-form__field-group-header-title-text" id="suggestion-de5-title"><?php _e( 'Advanced notes (optional)', 'annobib-theme' ); ?></div>
										</div>
									</div>
								</div>
								<div class="pf-c-form__field-group-body" hidden>
									<div class="pf-c-form__group">
										<div class="pf-c-form__group-label">
											<label class="pf-c-form__label" for="suggestion-de-edition">
												<span class="pf-c-form__label-text"><?php _e( 'Classroom edition', 'annobib-theme' ); ?></span>
											</label>
										</div>
										<div class="pf-c-form__group-control">
											<input class="pf-c-form-control" type="text" id="suggestion-de-edition" name="suggestion-de-edition" />
										</div>
									</div>
									<div class="pf-c-form__group">
										<div class="pf-c-form__group-label">
											<label class="pf-c-form__label" for="suggestion-de-links">
												<span class="pf-c-form__label-text"><?php _e( 'Useful links', 'annobib-theme' ); ?></span>
											</label>
										</div>
										<div class="pf-c-form__group-control">
											<input class="pf-c-form-control" type="text" id="suggestion-de-links" name="suggestion-de-links" />
										</div>
									</div>
									<div class="pf-c-form__group">
										<div class="pf-c-form__group-label">
								    	<label class="pf-c-form__label" for="suggestion-de-adaptation[]">
								      	<span class="pf-c-form__label-text"><?php _e( 'Adaptation genres', 'annobib-theme' ); ?></span>
								    	</label>
										</div>
								    <div class="pf-c-form__group-control">
								      <select class="pf-c-form-control annobib-m-inputbox" id="suggestion-de-adaptation[]" name="suggestion-de-adaptation[]" multiple>
												<?php annobib_select( 'de-adaptation' ); ?>
								      </select>
								    </div>
								  </div>
									<div class="pf-c-form__group">
										<div class="pf-c-form__group-label">
											<label class="pf-c-form__label" for="suggestion-de-notes">
												<span class="pf-c-form__label-text"><?php _e( 'Notes for the editors', 'annobib-theme' ); ?></span>
											</label>
										</div>
										<div class="pf-c-form__group-control">
											<textarea class="pf-c-form-control annobib-m-inputbox" name="suggestion-de-notes" id="suggestion-de-notes"></textarea>
										</div>
									</div>
								</div>
							</div><!-- Not included: length, content rating, identifier -->

							<input class="annobib-m-hidden" type="text" id="suggestion-de-check" name="suggestion-de-check" aria-hidden="true" />
							<input type="hidden" name="suggestion-de-submitted" id="suggestion-de-submitted" value="true" />

							<div class="pf-c-form__group pf-m-action">
								<div class="pf-c-form__actions">
									<button class="pf-c-button pf-m-primary" type="submit" form="suggestion-de-form"><?php _e( 'Send entry', 'annobib-theme' ); ?></button>
						       <button class="pf-c-button pf-m-link annobib-h-close" type="button"><?php _e( 'Cancel', 'annobib-theme' ); ?></button>
								</div>
							</div>
						</form>
					</section>

	      </div>
	    </div>
	  </div>
	</aside>


	<!-- MODAL (Tutorial) -->
	<aside class="pf-c-backdrop" id="tutorial" aria-hidden="true" hidden role="dialog" aria-modal="true" aria-labelledby="tutorial-title" aria-describedby="tutorial-description">
		<div class="pf-l-bullseye">
			<div class="pf-c-modal-box pf-m-sm">
				<button class="pf-c-button pf-m-plain annobib-h-close" type="button" aria-label="<?php _e( 'Close dialog', 'annobib-theme' ); ?>">
					<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__menu-close" /></svg>
				</button>
			  <header class="pf-c-modal-box__header">
			    <h1 class="pf-c-modal-box__title" id="tutorial-title" aria-hidden="true"><?php _e( 'Watch the tutorial', 'annobib-theme' ); ?></h1>
					<div class="pf-c-modal-box__description" id="tutorial-description"><?php _e( 'Get an overview of what Lit4School can do for you.', 'annobib-theme' ); ?></div>
			  </header>
				<div class="pf-c-modal-box__body">
					<video controls class="annobib-h-video" src="<?php echo get_template_directory_uri(); ?>/assets/videos/tutorial-<?php echo $annobib_language; ?>.mp4" type="video/mp4">
						<p><?php _e( 'Your browser does not support playing this video.', 'annobib-theme' ); ?></p>
					</video>
				</div>
				<footer class="pf-c-modal-box__footer">
					<button class="pf-c-button pf-m-primary annobib-h-close" type="button"><?php _e( 'Close', 'annobib-theme' ); ?></button>
				</footer>
			</div>
		</div>
	</aside>


	<!-- MODAL (About Lit4School) -->
	<aside class="pf-c-backdrop" id="about" aria-hidden="true" hidden role="dialog" aria-modal="true" aria-labelledby="about-title">
	  <div class="pf-l-bullseye">
	    <div class="pf-c-about-modal-box">
	      <div class="pf-c-about-modal-box__brand">
	        <img class="pf-c-about-modal-box__brand-image" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.svg" alt="<?php _e( 'Lit4School logo', 'annobib-theme' ); ?>" />
	      </div>
	      <div class="pf-c-about-modal-box__close">
	        <button class="pf-c-button pf-m-plain annobib-h-close" type="button" aria-label="<?php _e( 'Close dialog', 'annobib-theme' ); ?>">
	          <svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__menu-close" /></svg>
	        </button>
	      </div>
	      <div class="pf-c-about-modal-box__header">
	        <h1 class="pf-c-title pf-m-4xl" id="about-title" aria-hidden="true"><?php bloginfo( 'name' ); ?></h1>
		      <h2 class="pf-c-title pf-m-2xl pf-u-mt-sm" id="about-title"><?php _e( 'Authentic&nbsp;classroom literature&nbsp;&&nbsp;media', 'annobib-theme' ); ?></h2>
	      </div>
	      <div class="pf-c-about-modal-box__hero"></div>
	      <div class="pf-c-about-modal-box__content">
	        <div class="pf-c-content">
	          <dl>
		          <dt><?php _e( 'Last updated', 'annobib-theme' ); ?></dt>
		          <dd><?php wp_reset_postdata();
							$recent = new WP_Query( array(
								'post_type'				=> 'any',
								'post_status'			=> 'publish',
								'orderby'					=> 'post_date',
                'posts_per_page'	=> 1
							));
							if ( $recent->have_posts() ) {
								while ( $recent->have_posts() ) {
									$recent->the_post();
									$modified = get_the_modified_date();
									if ( $modified ) {
										echo $modified;
									}
									else {
		                echo get_the_date();
									}
								}
							}
              wp_reset_postdata(); ?> (<?php $count = wp_count_posts( 'bibliography-en' )->publish + wp_count_posts( 'bibliography-de' )->publish; echo $count . ' ' . __( 'entries', 'annobib-theme' ); ?>)</dd>
	            <dt><?php _e( 'Project lead', 'annobib-theme' ); ?></dt>
	            <dd>Simon&nbsp;Weise, Jonatan&nbsp;Jalle&nbsp;Steller (<?php _e( 'web development', 'annobib-theme' ); ?>)</dd>
	            <dt><?php _e( 'English entries', 'annobib-theme' ); ?></dt>
	            <dd>Simon&nbsp;Weise, Jonatan&nbsp;Jalle&nbsp;Steller, Sarah&nbsp;Clart, Sarah-Sophia&nbsp;Gärtner, Melanie&nbsp;Schoenhoff, Rico&nbsp;Bartsch</dd>
	            <dt><?php _e( 'German entries', 'annobib-theme' ); ?></dt>
	            <dd>Silke&nbsp;Horstkotte, Karolin&nbsp;Freund, Nils&nbsp;Rosenkranz, Katharina&nbsp;Kraus, Joachim&nbsp;Kern, Frieder&nbsp;Stange, Maren&nbsp;Petrich, Anne&nbsp;Seeger</dd>
	            <dt><?php _e( 'Former contributors', 'annobib-theme' ); ?></dt>
	            <dd>Luise&nbsp;Czajkowski</dd>
	            <dt><?php _e( 'University affiliation', 'annobib-theme' ); ?></dt>
							<dd><?php printf( '<a href="%s" target="_blank" rel="noreferrer noopener"><abbr title="' . __( 'Zentrum für Lehrerbildung und Schulforschung', 'annobib-theme' ) . '">' . __( 'ZLS', 'annobib-theme' ) . '</abbr> ' . __( 'at Leipzig University', 'annobib-theme' ) . '</a>', 'https://www.zls.uni-leipzig.de/' ); ?></dd>
							<dt><?php _e( 'Title illustration', 'annobib-theme' ); ?></dt>
							<dd>Susanne&nbsp;Haase</dd>
	            <!--<dt><?php _e( 'Version', 'annobib-theme' ); ?></dt>
	            <dd><?php $theme = wp_get_theme(); echo esc_html( $theme->get( 'Version' ) ); ?></dd>-->
	          </dl>
	        </div>
	        <p class="pf-c-about-modal-box__strapline"><?php printf( __( 'Built using', 'annobib-theme' ) . ' <a href="%s" target="_blank" rel="noreferrer noopener">' . __( 'WordPress', 'annobib-theme' ) . '</a> ' . __( 'and', 'annobib-theme' ) . ' <a href="%s" target="_blank" rel="noreferrer noopener">' . __( 'PatternFly', 'annobib-theme' ) . '</a>.', 'https://wordpress.org/', 'https://www.patternfly.org/' ); ?> <?php printf( '<a href="%s" target="_blank" rel="noreferrer noopener">' . __( 'Plugin', 'annobib-theme' ) . '</a> ' . __( 'and', 'annobib-theme' ) . ' <a href="%s" target="_blank" rel="noreferrer noopener">' . __( 'theme', 'annobib-theme' ) . '</a> ' . __( 'are open-source.', 'annobib-theme' ), 'https://gitlab.com/jonatansteller/annobib-plugin', 'https://gitlab.com/jonatansteller/annobib-theme' ); ?></p>
	      </div>
	    </div>
	  </div>
	</aside>


	<!-- ALERT GROUP -->
	<aside aria-labelledby="alerts-title">
		<h1 class="annobib-m-hidden" id="alerts-title" aria-hidden="true"><?php _e( 'Alerts', 'annobib-theme' ); ?></h1>
		<ul class="pf-c-alert-group pf-m-toast">
		 	<li class="pf-c-alert-group__item" id="alert-clipboard-success" aria-hidden="true" hidden>
				<div class="pf-c-alert pf-m-success" aria-label="<?php _e( 'Success alert', 'annobib-theme' ); ?>">
			  	<div class="pf-c-alert__icon">
			   		<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__navigation-success" /></svg>
			  	</div>
			  	<h2 class="pf-c-alert__title"><span class="pf-screen-reader"><?php _e( 'Success alert:', 'annobib-theme' ); ?> </span><?php _e( 'Address copied to clipboard', 'annobib-theme' ); ?></h2>
				</div>
			</li>
			<li class="pf-c-alert-group__item" id="alert-clipboard-warning" aria-hidden="true" hidden>
				<div class="pf-c-alert pf-m-warning" aria-label="<?php _e( 'Warning alert', 'annobib-theme' ); ?>">
					<div class="pf-c-alert__icon">
						<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__navigation-warning" /></svg>
					</div>
					<h2 class="pf-c-alert__title"><span class="pf-screen-reader"><?php _e( 'Warning alert:', 'annobib-theme' ); ?> </span><?php _e( 'Address could not be copied to clipboard', 'annobib-theme' ); ?></h2>
				</div>
			</li>
			<?php if ( isset( $suggestionSuccess ) && $suggestionSuccess == true ) : ?><li class="pf-c-alert-group__item" id="alert-suggestion-success" aria-hidden="true" hidden>
				<div class="pf-c-alert pf-m-success" aria-label="<?php _e( 'Success alert', 'annobib-theme' ); ?>">
				  <div class="pf-c-alert__icon">
				   	<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__navigation-success" /></svg>
				  </div>
				  <h2 class="pf-c-alert__title"><span class="pf-screen-reader"><?php _e( 'Success alert:', 'annobib-theme' ); ?> </span><?php _e( 'You suggestion was sent successfully', 'annobib-theme' ); ?></h2>
				</div>
			</li>
		<?php elseif ( isset( $suggestionSuccess ) && $suggestionSuccess == false ) : ?><li class="pf-c-alert-group__item" id="alert-suggestion-warning" aria-hidden="true" hidden>
				<div class="pf-c-alert pf-m-warning" aria-label="<?php _e( 'Warning alert', 'annobib-theme' ); ?>">
					<div class="pf-c-alert__icon">
						<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__navigation-warning" /></svg>
					</div>
					<h2 class="pf-c-alert__title"><span class="pf-screen-reader"><?php _e( 'Warning alert:', 'annobib-theme' ); ?> </span><?php _e( 'Your suggestion could not be sent', 'annobib-theme' ); ?></h2>
				</div>
			</li>
			<?php endif; ?>
		</ul>
	</aside><?php endif; ?>


  <!-- WORDPRESS: FOOTER -->
  <?php wp_footer(); ?>


</div></body>


</html>
