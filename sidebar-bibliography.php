<?php
// Annotated Bibliography Theme
// Sidebar: Bibliography

$annobib_section	= get_query_var( 'annobib_section' );
$annobib_language	= get_query_var( 'annobib_language' );
$annobib_current	= get_query_var( 'annobib_current' );
?>


	<!-- PAGE SIDEBAR -->
	<nav class="pf-c-page__sidebar pf-m-light" id="sidebar" aria-labelledby="sidebar-title">
		<div class="pf-c-page__sidebar-body">
			<div class="pf-c-nav pf-m-light">
				<h2 class="annobib-m-hidden" id="sidebar-title" aria-hidden="true"><?php _e( 'Find entries', 'annobib-theme' ); ?></h2>

				<!-- PAGE SIDEBAR: SECTION -->
				<section class="pf-c-nav__section" aria-labelledby="sidebar-scope">
					<h3 class="pf-c-nav__section-title annobib-m-hidden" id="sidebar-scope"><?php _e( 'By scope', 'annobib-theme' ); ?></h3>
					<ul class="pf-c-nav__list">
						<li class="pf-c-nav__item">
							<a href="<?php echo home_url( '/' . $annobib_section ); ?>" class="pf-c-nav__link<?php if ( $annobib_current == 'home-' . $annobib_section ) { echo ' pf-m-current'; } ?>" id="sidebar-scope-overview"<?php if ( $annobib_current == 'home-' . $annobib_section ) { echo ' aria-current="page"'; } ?>><?php _e( 'Overview', 'annobib-theme' ); ?>
								<span class="pf-c-nav__toggle">
									<span class="pf-c-nav__toggle-icon annobib-m-steady">
										<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__entry-home" /></svg>
									</span>
								</span>
							</a>
						</li>
						<li class="pf-c-nav__item">
							<a href="<?php echo home_url( '/' . $annobib_section . '-recent' ); ?>" class="pf-c-nav__link<?php if ( $annobib_current == 'recent-' . $annobib_section ) { echo ' pf-m-current'; } ?>" id="sidebar-scope-all"<?php if ( $annobib_current == 'recent-' . $annobib_section ) { echo ' aria-current="page"'; } ?>><?php _e( 'All entries', 'annobib-theme' ); ?>
								<span class="pf-c-nav__toggle">
									<span class="pf-c-nav__toggle-icon annobib-m-steady">
										<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__entry-all" /></svg>
									</span>
								</span>
							</a>
						</li>
						<li class="pf-c-nav__item">
							<a href="<?php echo home_url( '/' . $annobib_section . '-highlights' ); ?>" class="pf-c-nav__link<?php if ( $annobib_current == 'highlights-' . $annobib_section ) { echo ' pf-m-current'; } ?>" id="sidebar-scope-highlights"<?php if ( $annobib_current == 'highlights-' . $annobib_section ) { echo ' aria-current="page"'; } ?>><?php _e( 'Highlights', 'annobib-theme' ); ?>
								<span class="pf-c-nav__toggle">
									<span class="pf-c-nav__toggle-icon annobib-m-steady">
										<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__entry-highlight" /></svg>
									</span>
								</span>
							</a>
						</li>
					</ul>
				</section>

				<!-- PAGE SIDEBAR: SECTION -->
				<section class="pf-c-nav__section" aria-labelledby="sidebar-schoolyear">
					<h3 class="pf-c-nav__section-title" id="sidebar-schoolyear"><?php _e( 'School & year', 'annobib-theme' ); ?></h3>
					<ul class="pf-c-nav__list">
						<?php annobib_navigation( $annobib_section . '-curriculum', 'super', 'schoolyear' ); ?>
					</ul>
				</section>

				<!-- PAGE SIDEBAR: SECTION -->
				<section class="pf-c-nav__section" aria-labelledby="sidebar-metadata">
					<h3 class="pf-c-nav__section-title" id="sidebar-metadata"><?php _e( 'Metadata', 'annobib-theme' ); ?></h3>
					<ul class="pf-c-nav__list">
						<li class="pf-c-nav__item pf-m-expandable">
							<button type="button" class="pf-c-nav__link annobib-h-menu" id="sidebar-metadata-topic" aria-expanded="false"><?php _e( 'Topic', 'annobib-theme' ); ?>
								<span class="pf-c-nav__toggle">
									<span class="pf-c-nav__toggle-icon annobib-m-steady">
										<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__entry-topic" /></svg>
									</span>
								</span>
							</button>
							<section class="pf-c-nav__subnav" aria-labelledby="sidebar-metadata-topic" hidden>
								<ul class="pf-c-nav__list">
									<?php annobib_navigation( $annobib_section . '-topic', 'sub', false ); ?>
								</ul>
							</section>
						</li>
						<?php if ( $annobib_section == 'de' ) : ?><li class="pf-c-nav__item pf-m-expandable">
							<button type="button" class="pf-c-nav__link annobib-h-menu" id="sidebar-metadata-context" aria-expanded="false"><?php _e( 'Context', 'annobib-theme' ); ?>
								<span class="pf-c-nav__toggle">
									<span class="pf-c-nav__toggle-icon annobib-m-steady">
										<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__entry-context" /></svg>
									</span>
								</span>
							</button>
							<section class="pf-c-nav__subnav" aria-labelledby="sidebar-metadata-context" hidden>
								<ul class="pf-c-nav__list">
									<?php annobib_navigation( $annobib_section . '-context', 'sub', false ); ?>
								</ul>
							</section>
						</li><?php endif; ?>
						<li class="pf-c-nav__item pf-m-expandable">
							<button type="button" class="pf-c-nav__link annobib-h-menu" id="sidebar-metadata-genre" aria-expanded="false"><?php _e( 'Genre', 'annobib-theme' ); ?>
								<span class="pf-c-nav__toggle">
									<span class="pf-c-nav__toggle-icon annobib-m-steady">
										<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__entry-genre" /></svg>
									</span>
								</span>
							</button>
							<section class="pf-c-nav__subnav" aria-labelledby="sidebar-metadata-genre" hidden>
								<ul class="pf-c-nav__list">
									<?php annobib_navigation( $annobib_section . '-genre', 'sub', false ); ?>
								</ul>
							</section>
						</li>
						<li class="pf-c-nav__item pf-m-expandable">
							<button type="button" class="pf-c-nav__link annobib-h-menu" id="sidebar-metadata-author" aria-expanded="false"><?php _e( 'Author', 'annobib-theme' ); ?>
								<span class="pf-c-nav__toggle">
									<span class="pf-c-nav__toggle-icon annobib-m-steady">
										<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__entry-author" /></svg>
									</span>
								</span>
							</button>
							<section class="pf-c-nav__subnav" aria-labelledby="sidebar-metadata-author" hidden>
								<ul class="pf-c-nav__list">
									<?php annobib_navigation( $annobib_section . '-writer', 'sub', false ); ?>
								</ul>
							</section>
						</li>
						<?php if ( $annobib_section == 'en' ) : ?><li class="pf-c-nav__item pf-m-expandable">
							<button type="button" class="pf-c-nav__link annobib-h-menu" id="sidebar-metadata-country" aria-expanded="false"><?php _e( 'Country', 'annobib-theme' ); ?>
								<span class="pf-c-nav__toggle">
									<span class="pf-c-nav__toggle-icon annobib-m-steady">
										<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__entry-country" /></svg>
									</span>
								</span>
							</button>
							<section class="pf-c-nav__subnav" aria-labelledby="sidebar-metadata-country" hidden>
								<ul class="pf-c-nav__list">
									<?php annobib_navigation( $annobib_section . '-country', 'sub', false ); ?>
								</ul>
							</section>
						</li><?php endif; ?>
						<li class="pf-c-nav__item pf-m-expandable">
							<button type="button" class="pf-c-nav__link annobib-h-menu" id="sidebar-metadata-date" aria-expanded="false"><?php _e( 'Publication date', 'annobib-theme' ); ?>
								<span class="pf-c-nav__toggle">
									<span class="pf-c-nav__toggle-icon annobib-m-steady">
										<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__entry-date" /></svg>
									</span>
								</span>
							</button>
							<section class="pf-c-nav__subnav" aria-labelledby="sidebar-metadata-date" hidden>
								<ul class="pf-c-nav__list">
									<li class="pf-c-nav__item">
										<a href="<?php echo home_url( '/' . $annobib_section . '-date-older' ); ?>" class="pf-c-nav__link"><?php _e( 'Before 1945', 'annobib-theme' ); ?></a>
										<a href="<?php echo home_url( '/' . $annobib_section . '-date-1945' ); ?>" class="pf-c-nav__link"><?php _e( '1945 to 1988', 'annobib-theme' ); ?></a>
										<a href="<?php echo home_url( '/' . $annobib_section . '-date-1989' ); ?>" class="pf-c-nav__link"><?php _e( '1989 to 1999', 'annobib-theme' ); ?></a>
										<a href="<?php echo home_url( '/' . $annobib_section . '-date-2000' ); ?>" class="pf-c-nav__link"><?php _e( '2000 to 2009', 'annobib-theme' ); ?></a>
										<a href="<?php echo home_url( '/' . $annobib_section . '-date-2010' ); ?>" class="pf-c-nav__link"><?php _e( 'Since 2010', 'annobib-theme' ); ?></a>
									</li>
								</ul>
							</section>
						</li>
					</ul>
				</section>

			</div>
		</div>
	</nav>
