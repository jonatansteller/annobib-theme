<?php
// Annotated Bibliography Theme
// Reusable Component: Empty Section

$annobib_section	= get_query_var( 'annobib_section' );
$annobib_language	= get_query_var( 'annobib_language' );
$annobib_current	= get_query_var( 'annobib_current' );
?>


				<!-- PAGE MAIN: CARDS: EMPTY -->
				<article class="pf-c-empty-state">
					<div class="pf-c-empty-state__content">
						<svg class="annobib-c-icon pf-c-empty-state__icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__navigation-empty" /></svg>

						<!-- PAGE MAIN: CARDS: EMPTY: TEXT -->
						<h2 class="pf-c-title pf-m-lg"><?php _e( 'Coming soon', 'annobib-theme' ); ?></h2>
						<div class="pf-c-empty-state__body"><?php _e( 'We are currently getting everything ready for launch.', 'annobib-theme' ); ?></div>

					</div>
				</article>
