<?php
// Annotated Bibliography Theme
// Reusable Component: Empty List

$annobib_section	= get_query_var( 'annobib_section' );
$annobib_language	= get_query_var( 'annobib_language' );
$annobib_current	= get_query_var( 'annobib_current' );
?>


				<!-- PAGE MAIN: CARDS: EMPTY -->
				<article class="pf-c-empty-state">
					<div class="pf-c-empty-state__content">
						<svg class="annobib-c-icon pf-c-empty-state__icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__navigation-empty" /></svg>

						<!-- PAGE MAIN: CARDS: EMPTY: TEXT -->
						<h2 class="pf-c-title pf-m-lg"><?php _e( 'Whoopsies', 'annobib-theme' ); ?></h2>
						<div class="pf-c-empty-state__body"><?php _e( 'We could not find any entries that match these criteria.', 'annobib-theme' ); ?></div>

						<!-- PAGE MAIN: CARDS: EMPTY: PRIMARY -->
						<div class="pf-c-empty-state__primary">
							<button type="button" class="pf-c-button pf-m-primary annobib-h-back"><?php _e( 'Return to previous page', 'annobib-theme' ); ?></button>
						</div>

						<!-- PAGE MAIN: CARDS: EMPTY: SECONDARY -->
						<div class="pf-c-empty-state__secondary">
							<a href="<?php echo get_home_url(); ?>" class="pf-c-button pf-m-plain"><?php _e( 'Try the front page', 'annobib-theme' ); ?></a>
							<button class="pf-c-button pf-m-plain annobib-h-open" type="button" data-target="#suggestion"><?php _e( 'Suggest an entry', 'annobib-theme' ); ?></button>
						</div>

					</div>
				</article>
