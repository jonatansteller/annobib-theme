<?php
// Annotated Bibliography Theme
// Homepage: bibliography (German)

annobib_localise( 'de_DE' );
set_query_var( 'annobib_section', 'de' );
set_query_var( 'annobib_language', 'de' );
set_query_var( 'annobib_current', 'home-de' );


// Header
get_header();

// Sidebar
get_sidebar( 'bibliography' );

// Main
get_template_part( 'reusable', 'home' );

// Footer
get_footer();

?>
