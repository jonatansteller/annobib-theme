<?php
// Annotated Bibliography Theme
// Reusable Component: List Before the Loop 2

$annobib_section	= get_query_var( 'annobib_section' );
$annobib_language	= get_query_var( 'annobib_language' );
$annobib_current	= get_query_var( 'annobib_current' );
?>


								</div>
							</div>
						</nav>

					</div>
				</div>

				<!-- TOOLBAR (Invisible Filter Form) -->
				<form method="post" action="<?php global $wp; echo home_url( $wp->request ); ?>" aria-aria-hidden="true" id="toolbar-filterform" class="annobib-m-hidden">
					<div id="toolbar-filterform-active"><?php if ( isset( $_POST['toolbar-filterform-filters'] ) ) {
						echo sanitize_text_field( trim( $_POST['toolbar-filterform-filters'] ) );
					} ?></div>
					<input type="hidden" id="toolbar-filterform-filters" name="toolbar-filterform-filters" value="">
				</form>

				<!-- TOOLBAR (Second row) -->
				<div class="annobib-m-hidden annobib-m-chips" id="toolbar-chip" aria-hidden="true" hidden>
					<h3 class="annobib-m-hidden"><?php _e( 'Active filters', 'annobib-theme' ); ?></h3>
					<div class="pf-c-toolbar__content pf-m-chip-container" id="toolbar-chipcontainer">
					</div>
				</div>

			</div>

		</section>

		<!-- PAGE MAIN: CARDS -->
		<section class="pf-c-page__main-section pf-m-fill" aria-labelledby="cards-title">
			<h2 class="annobib-m-hidden" aria-hidden="true" id="cards-title"><?php _e( 'List of entries', 'annobib-theme' ); ?></h2>
			<div class="pf-l-gallery pf-m-gutter">
