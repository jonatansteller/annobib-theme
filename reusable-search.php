<?php
// Annotated Bibliography Theme
// Reusable Component: Search Bar

$annobib_section  = get_query_var( 'annobib_section' );
$annobib_language = get_query_var( 'annobib_language' );
$annobib_current	= get_query_var( 'annobib_current' );
?>


            <!-- TOOLBAR: GROUP -->
            <div class="pf-c-toolbar__group pf-m-hidden pf-m-visible-on-md" id="toolbar-search">
              <h3 class="pf-c-toolbar__item pf-m-label"><?php _e( 'Search', 'annobib-theme' ); ?></h3>

              <!-- TOOLBAR: GROUP: SEARCH -->
              <div class="pf-c-toolbar__item">
                <form role="search" method="get" action="<?php esc_url( home_url( '/' ) ); ?>">
                  <div class="pf-c-input-group">
                    <input class="pf-c-form-control" type="search" id="toolbar-search-input" name="s" aria-label="<?php _e( 'Search text', 'annobib-theme' ); ?>" />
                    <button class="pf-c-button pf-m-control" type="submit" aria-label="Search button">
                      <svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__navigation-search" /></svg>
                    </button>
                  </div>
                </form>
              </div>

            </div>

            <!-- TOOLBAR: BUTTON GROUP -->
						<div class="pf-c-toolbar__group pf-m-icon-button-group">

							<!-- TOOLBAR: BUTTON GROUP: BUTTON -->
							<div class="pf-c-toolbar__toggle pf-m-hidden-on-md">
								<button class="pf-c-button pf-m-plain annobib-h-nav" type="button" aria-label="<?php _e( 'Search field toggle', 'annobib-theme' ); ?>" aria-expanded="false" aria-controls="toolbar-search" data-target="#toolbar-search">
									<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__navigation-search" /></svg>
								</button>
							</div>

            </div>
