<?php
// Annotated Bibliography Theme
// Homepage: home

// Set language according to browser setting
if ( substr( $_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2 ) == 'de' ) {
  annobib_localise( 'de_DE' );
  set_query_var( 'annobib_language', 'de' );
}
else {
  //annobib_localise( 'en_GB' );
  set_query_var( 'annobib_language', 'en' );
}
set_query_var( 'annobib_section', 'general' );
set_query_var( 'annobib_current', 'home' );


// Header
set_query_var( 'annobib_modifier', 'plain' );
get_header(); ?>

  <!-- PAGE MAIN -->
  <main role="main" class="pf-c-page__main annobib-l-front" tabindex="-1" id="content" aria-labelledby="content-title">

    <!-- PAGE MAIN: SECTION -->
    <div class="pf-c-page__main-section pf-l-bullseye annobib-l-front-decoration">
      <div class="pf-l-bullseye__item">
        <figure class="annobib-c-decoration"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/background.jpg" class="annobib-c-decoration__image annobib-h-open" data-target="#decoration"><figcaption class="annobib-c-decoration__caption"><?php _e( 'Illustration:', 'annobib-theme' ); ?> Susanne&nbsp;Haase</figcaption></figure>
      </div>
    </div>

    <!-- PAGE MAIN: SECTION -->
    <div class="pf-c-page__main-section pf-m-dark-100 pf-l-bullseye annobib-l-front-controls">
      <div class="pf-l-bullseye__item">

        <div class="annobib-m-block-title">
          <h1 class="annobib-m-hidden" id="content-title" aria-hidden="true"><?php bloginfo( 'name' ); ?></h1>
          <img class="annobib-c-wordmark" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-wordmark.svg" alt="<?php _e( 'Lit4School logo', 'annobib-theme' ); ?>">
        </div>

        <div class="annobib-m-block-title"><a href="<?php echo home_url( '/en' ); ?>" class="pf-c-button pf-m-tertiary pf-m-display-lg annobib-m-display-xl"><?php _e( 'English', 'annobib-theme' ); ?></a></div>

        <?php // Set up loop
        $loop = new WP_Query( array(
          'post_type'				=> 'post',
          'category_name'		=> 'english',
          'posts_per_page'	=> 1
        ));

        // Loop
        if ( $loop->have_posts() ) {
          while ( $loop->have_posts() ) {
            $loop->the_post();
            echo '<div class="annobib-m-block-title"><a href="' . home_url( '/en-blog' ) . '" class="pf-c-button pf-m-link pf-m-inline annobib-m-display-xs">' . __( 'From our blog', 'annobib-theme' ) . ': ' . the_title( '', '', false ) . '</a></div>';
          }
        }

        // Prepare next loop
        wp_reset_postdata(); ?>

        <div class="annobib-m-block-title"><a href="<?php echo home_url( '/de' ); ?>" class="pf-c-button pf-m-tertiary pf-m-display-lg annobib-m-display-xl"><?php _e( 'German', 'annobib-theme' ); ?></a></div>

        <?php // Set up loop
        $loop = new WP_Query( array(
          'post_type'				=> 'post',
          'category_name'		=> 'german',
          'posts_per_page'	=> 1
        ));

        // Loop
        if ( $loop->have_posts() ) {
          while ( $loop->have_posts() ) {
            $loop->the_post();
            echo '<div class="annobib-m-block-title"><a href="' . home_url( '/de-blog' ) . '" class="pf-c-button pf-m-link pf-m-inline annobib-m-display-xs">' . __( 'From our blog', 'annobib-theme' ) . ': ' . the_title( '', '', false ) . '</a></div>';
          }
        }

        // Prepare next loop
        wp_reset_postdata(); ?>

        <p class="annobib-m-block-info annobib-m-hidden-on-xs"><?php _e( 'Lit4School is a non-profit database of literature and media for the classroom. This OER is developed by the English group and edited by the ZLS\'s English and German groups at Leipzig University.', 'annobib-theme' ); ?></p>

        <div class="annobib-m-block-image annobib-m-small annobib-m-hidden-on-xs">
          <a href="https://www.zls.uni-leipzig.de/" target="_blank" rel="noreferrer noopener"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-zls-white.svg" alt="<?php _e( 'Zentrum für Lehrerbildung und Schulforschung at Leipzig University', 'annobib-theme' ); ?>"></a>
        </div>

      </div>
    </div>

  </main>


  <!-- MODAL (Decoration) -->
  <aside class="pf-c-backdrop" id="decoration" aria-hidden="true" hidden role="dialog" aria-modal="true" aria-labelledby="decoration-title">
    <h1 class="annobib-m-hidden" id="decoration-title" aria-hidden="true"><?php _e( 'Illustration', 'annobib-theme' ); ?></h1>
    <figure class="annobib-c-lightbox">
      <img src="<?php echo get_template_directory_uri(); ?>/assets/images/background.jpg" class="annobib-c-lightbox__image">
    </figure>
  </aside>

<?php // Footer
set_query_var( 'annobib_modifier', 'plain' );
get_footer();

?>
