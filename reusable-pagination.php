<?php
// Annotated Bibliography Theme
// Reusable Component: Pagination

$annobib_section				= get_query_var( 'annobib_section' );
$annobib_language				= get_query_var( 'annobib_language' );
$annobib_current				= get_query_var( 'annobib_current' );
$annobib_modifier				= get_query_var( 'annobib_modifier' );
if ( ! $annobib_modifier ) {
	$annobib_modifier			= 'small-opendown';
}
$annobib_searchcount		= get_query_var( 'annobib_searchcount' );
if ( ! $annobib_searchcount ) {
	$annobib_searchcount	= 0;
}

// Current page
global $paged;
if( empty( $paged ) ) {
	$paged = 1;
}

// Number of pages
global $wp_query;
$pages = $wp_query->max_num_pages;
if( ! $pages ) {
	$pages = 1;
} ?>


				<!-- PAGINATION: NAV LEFT -->
				<div class="pf-c-pagination__nav-control pf-m-first">
					<?php if ( $pages != 1 && $paged > 2 && ! isset( $_POST['toolbar-filterform-filters'] ) ) : ?><a href="<?php echo get_pagenum_link( 1 ); ?>" class="pf-c-button pf-m-plain" aria-label="<?php _e( 'First page', 'annobib-theme' ); ?>">
						<svg class="annobib-c-icon"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__navigation-leftforward" /></svg>
					</a>
					<?php else : ?><button class="pf-c-button pf-m-plain" type="button" aria-label="<?php _e( 'First page', 'annobib-theme' ); ?>" aria-disabled="true" disabled>
						<svg class="annobib-c-icon"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__navigation-leftforward" /></svg>
					</button><?php endif; ?>
				</div>
				<div class="pf-c-pagination__nav-control pf-m-prev">
					<?php if ( $pages != 1 && $paged > 1 && ! isset( $_POST['toolbar-filterform-filters'] ) ) : ?><a href="<?php echo get_pagenum_link( $paged - 1 ); ?>" class="pf-c-button pf-m-plain" aria-label="<?php _e( 'Previous page', 'annobib-theme' ); ?>">
						<svg class="annobib-c-icon"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__navigation-left" /></svg>
					</a>
					<?php else : ?><button class="pf-c-button pf-m-plain" type="button" aria-label="<?php _e( 'Previous page', 'annobib-theme' ); ?>" aria-disabled="true" disabled>
						<svg class="annobib-c-icon"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__navigation-left" /></svg>
					</button><?php endif; ?>
				</div>

				<!-- PAGINATION: PAGES -->
				<?php if ( $pages != 1 && ! isset( $_POST['toolbar-filterform-filters'] ) ) : ?><div class="pf-c-options-menu pf-m-hidden-on-flex<?php if ( $annobib_modifier == 'small-openup' ) { echo ' pf-m-top'; } ?>">
					<button class="pf-c-options-menu__toggle pf-m-text pf-m-plain" id="<?php if ( $annobib_modifier == 'small-openup' ) { echo 'bottom-pagination-pages'; } else { echo 'toolbar-pagination-pages'; } ?>" aria-haspopup="listbox" aria-expanded="false">
						<span class="pf-c-options-menu__toggle-text"><?php echo ( $wp_query->found_posts + $annobib_searchcount ) . ' '; _e( 'entries', 'annobib-theme' ); ?></span>
						<span class="pf-c-options-menu__toggle-icon">
							<svg class="annobib-c-icon"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__<?php echo $annobib_modifier; ?>" /></svg>
						</span>
					</button>
					<ul class="pf-c-options-menu__menu pf-m-align-right" aria-labelledby="<?php if ( $annobib_modifier == 'small-openup' ) { echo 'bottom-pagination-pages'; } else { echo 'toolbar-pagination-pages'; } ?>" hidden>
						<?php for ( $i=1; $i <= $pages; $i++ ) : ?><li>
							<?php if ( $paged == $i ) : ?>
								<button class="pf-c-options-menu__menu-item annobib-h-selection" type="button" aria-disabled="true" aria-pressed="true" disabled><?php _e( 'Page', 'annobib-theme' ); echo ' ' . $i; ?>
								<span class="pf-c-options-menu__menu-item-icon annobib-h-selection-checkmark">
									<svg class="annobib-c-icon" aria-hidden="true"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__small-checked" /></svg>
								</span>
							</button>
							<?php else : ?>
								<a href="<?php echo get_pagenum_link( $i ); ?>" class="pf-c-options-menu__menu-item"><?php _e( 'Page', 'annobib-theme' ); echo ' ' . $i; ?></a>
							<?php endif; ?>
						</li><?php endfor; ?>
					</ul>
				</div>
				<?php elseif ( isset( $_POST['toolbar-filterform-filters'] ) ) : ?><div class="annobib-c-info">
					<span class="annobib-c-info__text"><?php _e( 'Filtered', 'annobib-theme' ); ?></span>
				</div>
				<?php else : ?><div class="annobib-c-info">
					<span class="annobib-c-info__text"><?php echo ( $wp_query->found_posts + $annobib_searchcount ) . ' '; _e( 'entries', 'annobib-theme' ); ?></span>
				</div><?php endif; ?>

				<!-- PAGINATION: NAV RIGHT -->
				<div class="pf-c-pagination__nav-control pf-m-next">
					<?php if ( $pages != 1 && $paged < $pages && ! isset( $_POST['toolbar-filterform-filters'] ) ) : ?><a href="<?php echo get_pagenum_link( $paged + 1 ); ?>" class="pf-c-button pf-m-plain" aria-label="<?php _e( 'Next page', 'annobib-theme' ); ?>">
						<svg class="annobib-c-icon"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__navigation-right" /></svg>
					</a>
					<?php else : ?><button class="pf-c-button pf-m-plain" type="button" aria-label="<?php _e( 'Next page', 'annobib-theme' ); ?>" aria-disabled="true" disabled>
						<svg class="annobib-c-icon"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__navigation-right" /></svg>
					</button><?php endif; ?>
				</div>
				<div class="pf-c-pagination__nav-control pf-m-last">
					<?php if ( $pages != 1 && $paged < $pages-1 && ! isset( $_POST['toolbar-filterform-filters'] ) ) : ?><a href="<?php echo get_pagenum_link( $pages ); ?>" class="pf-c-button pf-m-plain" aria-label="<?php _e( 'Last page', 'annobib-theme' ); ?>">
						<svg class="annobib-c-icon"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__navigation-rightforward" /></svg>
					</a>
					<?php else : ?><button class="pf-c-button pf-m-plain" type="button" aria-label="<?php _e( 'Last page', 'annobib-theme' ); ?>" aria-disabled="true" disabled>
						<svg class="annobib-c-icon"><use href="<?php echo get_template_directory_uri(); ?>/assets/images/annobib-c-icon.svg#annobib-c-icon__navigation-rightforward" /></svg>
					</button><?php endif; ?>
				</div>
