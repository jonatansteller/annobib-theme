<?php
// Annotated Bibliography Theme
// Post or Page

// Page: set language and section according to URL
if ( is_page() ) {
	if ( strpos( $_SERVER['REQUEST_URI'], '/de-' ) != false ) {
		annobib_localise( 'de_DE' );
		set_query_var( 'annobib_section', 'de' );
		set_query_var( 'annobib_language', 'de' );
	}
	else {
		//annobib_localise( 'en_GB' );
		set_query_var( 'annobib_section', 'en' );
		set_query_var( 'annobib_language', 'en' );
	}
}

// Post: set section to blog and language according to category
else {
	set_query_var( 'annobib_section', 'blog' );
	if ( in_category( 'german' ) ) {
		annobib_localise( 'de_DE' );
		set_query_var( 'annobib_language', 'de' );
	}
	else {
		//annobib_localise( 'en_GB' );
		set_query_var( 'annobib_language', 'en' );
	}
}

// Pages and posts: set current template type
set_query_var( 'annobib_current', 'post' );


// Header
get_header(); ?>

	<!-- PAGE MAIN -->
	<main role="main" class="pf-c-page__main" tabindex="-1" id="content" aria-labelledby="content-title">

<?php // Main
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		get_template_part( 'reusable', 'blog-post' );
	}
}
else {
	get_template_part( 'reusable', 'error' );
} ?>

	</main>

<?php // Footer
get_footer();

?>
